/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingMeanPhaseAndAzimutShiftImageFilter_h
#define otbSARStreamingMeanPhaseAndAzimutShiftImageFilter_h

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "otbPersistentFilterStreamingDecorator.h"

#include "otbSarSensorModel.h"
#include "otbImageMetadata.h"

#include <complex>
#include <cmath>

namespace otb
{

/** \class PersistentMeanPhaseAndAzimutShiftImageFilter
 * \brief Estimate mean values for an input interferogram or azimut shift image
 *
 * This filter persists its temporary data. It means that if you Update it n times on n different
 * requested regions, the output statistics will be the statitics of the whole set of n regions.
 *
 * To reset the temporary data, one should call the Reset() function.
 *
 * To get the wanted values once the regions have been processed via the pipeline, use the Synthetize() method.
 *
 * \ingroup DiapOTBModule
 */
template<class TInputImage>
class ITK_EXPORT PersistentMeanPhaseAndAzimutShiftImageFilter :
  public PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentMeanPhaseAndAzimutShiftImageFilter                 Self;
  typedef PersistentImageFilter<TInputImage, TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                         Pointer;
  typedef itk::SmartPointer<const Self>                   ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentMeanPhaseAndAzimutShiftImageFilter, PersistentImageFilter);

  /** Image related typedefs. For Grid Input (VectorImage) */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType   RegionType;
  typedef typename TInputImage::SizeType     SizeType;
  typedef typename TInputImage::IndexType    IndexType;
  typedef typename TInputImage::PixelType    PixelType;
  typedef double                             ValueType;
  typedef typename ImageType::IndexValueType IndexValueType;
  typedef typename ImageType::SizeValueType  SizeValueType;

  itkStaticConstMacro(InputImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Type to use for computations. */
  //  typedef typename itk::NumericTraits<PixelType>::RealType RealType;
  typedef typename itk::NumericTraits<double>::RealType RealType;
 
  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>      LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<ValueType> ValueObjectType;

    /** Return the number of valid points contained into the input grid. */
  long GetCount() const
  {
    return this->GetCountOutput()->Get();
  }
  LongObjectType* GetCountOutput();
  const LongObjectType* GetCountOutput() const;

  /** Return the computed Sum. */
  ValueType GetSum() const
  {
    return this->GetSumOutput()->Get();
  }
  ValueObjectType* GetSumOutput();
  const ValueObjectType* GetSumOutput() const;

  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) ITK_OVERRIDE;
  using Superclass::MakeOutput;

  /**
   * Synthetize and Reset function called by our PersistentFilterStreamingDecorator
   */
  void Synthetize(void) ITK_OVERRIDE;
  void Reset(void) ITK_OVERRIDE;

  itkSetMacro(IgnoreInfiniteValues, bool);
  itkGetMacro(IgnoreInfiniteValues, bool);

  itkSetMacro(IgnoreUserDefinedValue, bool);
  itkGetMacro(IgnoreUserDefinedValue, bool);

  itkSetMacro(Threshold, double);
  itkGetMacro(Threshold, double);

  itkSetMacro(PhaseMean, bool);
  itkGetMacro(PhaseMean, bool);

  itkSetMacro(UserIgnoredValue, RealType);
  itkGetMacro(UserIgnoredValue, RealType);

protected:
  PersistentMeanPhaseAndAzimutShiftImageFilter();
  ~PersistentMeanPhaseAndAzimutShiftImageFilter() ITK_OVERRIDE {}
  void PrintSelf(std::ostream& os, itk::Indent indent) const ITK_OVERRIDE;

  
  /** PersistentMeanPhaseAndAzimutShiftImageFilter needs a larger input requested region than the output
   * requested region.  As such, SARQuadraticAveragingImageFilter needs to provide an
   * implementation for GenerateInputRequestedRegion() in order to inform the
   * pipeline execution model.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input region 
   */
  RegionType OutputRegionToInputRegion(const RegionType& outputRegion) const;

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType&
                             outputRegionForThread,
                             itk::ThreadIdType threadId) ITK_OVERRIDE;

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() ITK_OVERRIDE;
  void GenerateOutputInformation() ITK_OVERRIDE;

private:
  PersistentMeanPhaseAndAzimutShiftImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  itk::Array<ValueType>       m_ThreadSum;
  itk::Array<long>            m_ThreadCount;

  /* Ignored values */
  bool                        m_IgnoreInfiniteValues;
  bool                        m_IgnoreUserDefinedValue;
  RealType                    m_UserIgnoredValue;
  std::vector<unsigned long>  m_IgnoredInfinitePixelCount;
  std::vector<unsigned int>   m_IgnoredUserPixelCount;
  double                      m_Threshold;

  int                        m_NbComponentForInput;
  
  // Boolean to indicate the mode : Phase or Azimut Shift
  bool                        m_PhaseMean;


}; // end of class PersistentMeanPhaseAndAzimutShiftImageFilter

/*===========================================================================*/

/** \class SARStreamingMeanPhaseAndAzimutShiftImageFilter
 * \brief This class streams the whole input image through the PersistentMeanPhaseAndAzimutShiftImageFilter.
 *
 * This way, it allows computing the first order global statistics of this image. It calls the
 * Reset() method of the PersistentMeanPhaseAndAzimutShiftImageFilter before streaming the image and the
 * Synthetize() method of the PersistentMeanPhaseAndAzimutShiftImageFilter after having streamed the image
 * to compute the statistics. The accessor on the results are wrapping the accessors of the
 * internal PersistentMeanPhaseAndAzimutShiftImageFilter.
 * By default infinite values are ignored, use IgnoreInfiniteValues accessor to consider
 * infinite values in the computation.
 *
 * This filter can be used as:
 * \code
 * typedef otb::StreamingMeanPhaseAndAzimutShiftImageFilter<ImageType> StatisticsType;
 * StatisticsType::Pointer statistics = StatisticsType::New();
 * statistics->SetInput(reader->GetOutput());
 * statistics->Update();
 * std::cout << statistics-> GetSumRange() << std::endl;
 * std::cout << statistics-> GetSumAzimut() << std::endl;
 * std::cout << statistics-> GetMeanGrid(meanRange, meanAzimut) << std::endl;
 * \endcode
 *
 * \sa PersistentMeanPhaseAndAzimutShiftImageFilter
 * \sa PersistentImageFilter
 * \sa PersistentFilterStreamingDecorator
 * \sa StreamingImageVirtualWriter
 * \ingroup Streamed
 * \ingroup Multithreaded
 * \ingroup MathematicalStatisticsImageFilters
 *
 * \ingroup DiapOTBModule
 */

template<class TInputImage>
class ITK_EXPORT SARStreamingMeanPhaseAndAzimutShiftImageFilter :
  public PersistentFilterStreamingDecorator<PersistentMeanPhaseAndAzimutShiftImageFilter<TInputImage> >
{
public:
  /** Standard Self typedef */
  typedef SARStreamingMeanPhaseAndAzimutShiftImageFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersistentMeanPhaseAndAzimutShiftImageFilter<TInputImage> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingMeanPhaseAndAzimutShiftImageFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::PixelType PixelType;
  typedef double ValueType;
  typedef typename StatFilterType::RealType  RealType;
  typedef TInputImage                        InputImageType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>      LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<ValueType> ValueObjectType;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  void SetPhaseMean(bool phaseMean)
  {
    this->GetFilter()->SetPhaseMean(phaseMean);
  }

  /** Return the computed number of finite pixel. */
  long GetCount() const
  {
    return this->GetFilter()->GetCountOutput()->Get();
  }
  LongObjectType* GetCountOutput()
  {
    return this->GetFilter()->GetCountOutput();
  }
  const LongObjectType* GetCountOutput() const
  {
    return this->GetFilter()->GetCountOutput();
  }

  // Estimate Mean for valid points only
  ValueType GetMean()
  {
    // Get values estimated into our PersistentFilter
    ValueType sum = this->GetFilter()->GetSumOutput()->Get();
    long count_ValidPts = this->GetFilter()->GetCountOutput()->Get();

    // Estimate Mean
    ValueType countConvert = count_ValidPts; // In order to match with float or double for the next
                                                         // division
    ValueType mean= sum/countConvert;

    return mean; 
  }

  bool getOverlapLinesAndSamples(ImageMetadata SLCImageKWL,
			    std::pair<unsigned long,unsigned long> & linesUp, 
			    std::pair<unsigned long,unsigned long> & linesLow,
			    std::pair<unsigned long,unsigned long> & samplesUp,
			    std::pair<unsigned long,unsigned long> & samplesLow,
			    unsigned int burstIndUp,
			    bool inputWithInvalidPixels)
    {
      // Try to create a SarSensorModelAdapter
      SarSensorModel* sarSensorModel = new SarSensorModel(SLCImageKWL);

      // Try to call the deburstAndConcatenate function
      bool deburstAndConcatenateOk = sarSensorModel->Overlap(linesUp, linesLow, 
							     samplesUp, samplesLow,
							     burstIndUp, inputWithInvalidPixels);

      if(!deburstAndConcatenateOk)
	itkExceptionMacro(<<"Could not specify overlap between input bursts");

      return true;
 }

  // Set Threshold for valid point
  void SetThreshold(double threshold)
  {
    this->GetFilter()->SetThreshold(threshold);
  }

  otbSetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);
  otbGetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);

  otbSetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);
  otbGetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);

  otbSetObjectMemberMacro(Filter, UserIgnoredValue, RealType);
  otbGetObjectMemberMacro(Filter, UserIgnoredValue, RealType);

protected:
  /** Constructor */
  SARStreamingMeanPhaseAndAzimutShiftImageFilter() {};
  /** Destructor */
  ~SARStreamingMeanPhaseAndAzimutShiftImageFilter() ITK_OVERRIDE {}

private:
  SARStreamingMeanPhaseAndAzimutShiftImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented
};

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARStreamingMeanPhaseAndAzimutShiftImageFilter.txx"
#endif

#endif
