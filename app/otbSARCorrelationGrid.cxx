/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "itkMacro.h"
#include "itkFFTNormalizedCorrelationImageFilter.h"

#include "otbMultiToMonoChannelExtractROI.h"

#include "otbSARStreamingMaximumMinimumImageFilter.h"
#include "otbSARTemporalCorrelationGridImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARCorrelationGrid : public Application
{
public:
  typedef SARCorrelationGrid Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARCorrelationGrid, otb::Wrapper::Application);

  // Filters
  typedef itk::FFTNormalizedCorrelationImageFilter<FloatImageType, FloatImageType>          CorFilterType;
  typedef otb::SARStreamingMaximumMinimumImageFilter< FloatImageType>		   MinMaxFilterType;
  typedef otb::SARTemporalCorrelationGridImageFilter<FloatImageType, FloatVectorImageType>  CorGridFilterType;
  typedef otb::MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
					    FloatImageType::InternalPixelType> ExtractROIFilterType;

private:
  void DoInit() override
  {
    SetName("SARCorrelationGrid");
    SetDescription("Computes SAR correlation shift (into temporal domain).");

    SetDocLongDescription("This application computes correlation shifts between two images : "
    "shift in range and shift in azimut. "
    "The inputs of this application are MultiLooked images (real images).");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "inmaster",   "Input Master image (real image)");
    SetParameterDescription("inmaster", "Master Image (real image).");

    AddParameter(ParameterType_InputImage,  "inslave",   "Input Slave image (real image)");
    SetParameterDescription("inslave", "Slave Image (real image).");

    AddParameter(ParameterType_Int, "mlran", "MultiLook factor on distance");
    SetParameterDescription("mlran","MultiLook factor on distance.");
    SetDefaultParameterInt("mlran", 3);
    SetMinimumParameterIntValue("mlran", 1);
    MandatoryOff("mlran");
    
    AddParameter(ParameterType_Int, "mlazi", "MultiLook factor on azimut");
    SetParameterDescription("mlazi","MultiLook factor on azimut.");
    SetDefaultParameterInt("mlazi", 3);
    SetMinimumParameterIntValue("mlazi", 1);
    MandatoryOff("mlazi");

    AddParameter(ParameterType_Int, "gridsteprange", "Grid step for range dimension (into SLC/SAR geometry)");
    SetParameterDescription("gridsteprange","Grid step for range dimension (into SLC/SAR geometry).");
    SetDefaultParameterInt("gridsteprange", 150);
    SetMinimumParameterIntValue("gridsteprange", 1);
    MandatoryOff("gridsteprange");

    AddParameter(ParameterType_Int, "gridstepazimut", "Grid step for azimut dimension (into SLC/SAR geometry)");
    SetParameterDescription("gridstepazimut","Grid step for azimut dimension (into SLC/SAR geometry).");
    SetDefaultParameterInt("gridstepazimut", 150);
    SetMinimumParameterIntValue("gridstepazimut", 1);
    MandatoryOff("gridstepazimut");

    AddParameter(ParameterType_Int, "nooffset", "Set 0 to offset of first Line and Colunm of output grid");
    SetParameterDescription("nooffset", "If 1, then no offset for the first L/C if output grid.");
    SetDefaultParameterInt("nooffset", 0);
    SetMinimumParameterIntValue("nooffset", 0);
    MandatoryOff("nooffset");

    // Parameter Output
    AddParameter(ParameterType_OutputImage, "out", "Output Correlation grid (Vector Image)");
    SetParameterDescription("out","Output Correlation Grid Vector Image (Shift_ran, Shift_azi, Correlation_rate).");
    
    AddRAMParameter();

    SetDocExampleParameterValue("inmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_ML.tiff");
    SetDocExampleParameterValue("inslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002_ML.tiff");
    SetDocExampleParameterValue("out", "out_CorrelationGrid.tiff");
  }

  void DoUpdateParameters() override
  {
// Nothing to do here : all parameters are independent
  }

void DoExecute() override
{  
  // Get numeric parameters
  int factorML_azi = GetParameterInt("mlazi");
  int factorML_ran = GetParameterInt("mlran");
  int grid_step_azi = GetParameterInt("gridstepazimut");
  int grid_step_ran = GetParameterInt("gridsteprange");
  int nooffset = GetParameterInt("nooffset");

  // Log information
  otbAppLogINFO(<<"ML Factor on azimut :"<<factorML_azi);
  otbAppLogINFO(<<"ML Factor on range : "<<factorML_ran);
  otbAppLogINFO(<<"Grid Step for range : "<<grid_step_ran);
  otbAppLogINFO(<<"Grid Step for azimut : "<<grid_step_azi);
 
  // Get master and slave image
  FloatVectorImageType::Pointer MasterPtr = GetParameterFloatVectorImage("inmaster");
  FloatVectorImageType::Pointer SlavePtr = GetParameterFloatVectorImage("inslave");
   
  // Initialize values 
  float shiftML_range = 0;
  float shiftML_azimut = 0;
  int startx = 0;
  int starty = 0;
  int sizex = std::min(MasterPtr->GetLargestPossibleRegion().GetSize()[0],
		       SlavePtr->GetLargestPossibleRegion().GetSize()[0]);
  int sizey = std::min(MasterPtr->GetLargestPossibleRegion().GetSize()[1],
		       SlavePtr->GetLargestPossibleRegion().GetSize()[1]);

  
  // Extract ROI from Master and Slave Image to estimate global shifts without a too large amount
  // of RAM => size = 3000*3000 at the center
  if (sizex > 3000 && 
      sizey > 3000)
    {
      startx = MasterPtr->GetLargestPossibleRegion().GetSize()[0]/2 - 1500;
      starty = MasterPtr->GetLargestPossibleRegion().GetSize()[1]/2 - 1500;

      sizex = 3000;
      sizey = 3000;
    }

  ExtractROIFilterType::Pointer extractROIFilter_master = ExtractROIFilterType::New();
  m_Ref.push_back(extractROIFilter_master.GetPointer());
  extractROIFilter_master->SetInput(MasterPtr);
  extractROIFilter_master->SetChannel(1);
  extractROIFilter_master->SetStartX(startx);
  extractROIFilter_master->SetStartY(starty);
  extractROIFilter_master->SetSizeX(sizex);
  extractROIFilter_master->SetSizeY(sizey);
  
  ExtractROIFilterType::Pointer extractROIFilter_slave = ExtractROIFilterType::New();
  m_Ref.push_back(extractROIFilter_slave.GetPointer());
  extractROIFilter_slave->SetInput(SlavePtr);
  extractROIFilter_slave->SetChannel(1);
  extractROIFilter_slave->SetStartX(startx);
  extractROIFilter_slave->SetStartY(starty);
  extractROIFilter_slave->SetSizeX(sizex);
  extractROIFilter_slave->SetSizeY(sizey);

  // Correlation Filter
  CorFilterType::Pointer correlationFilter = CorFilterType::New();
  m_Ref.push_back(correlationFilter.GetPointer());
  correlationFilter->SetFixedImage(extractROIFilter_master->GetOutput());
  correlationFilter->SetMovingImage(extractROIFilter_slave->GetOutput());
  
  MinMaxFilterType::Pointer minMaxFilter = MinMaxFilterType::New();
  minMaxFilter->SetInput(correlationFilter->GetOutput());
  // Adapt streaming with ram parameter (default 256 MB)
  minMaxFilter->GetStreamer()->SetAutomaticStrippedStreaming(GetParameterInt("ram"));
  minMaxFilter->Update();
  
  shiftML_range = ((correlationFilter->GetOutput()->GetLargestPossibleRegion().GetSize()[0]/2.)-
		   minMaxFilter->GetIndexOfMax()[0]);
  shiftML_azimut = ((correlationFilter->GetOutput()->GetLargestPossibleRegion().GetSize()[1]/2.)
		    -minMaxFilter->GetIndexOfMax()[1]);
  
  // Correlation Grid Filter 
  CorGridFilterType::Pointer filterCorrelationGrid = CorGridFilterType::New();
  m_Ref.push_back(filterCorrelationGrid.GetPointer());

  // Configure CorrelationGrid Filter
  filterCorrelationGrid->SetMLran(factorML_ran);
  filterCorrelationGrid->SetMLazi(factorML_azi);
  filterCorrelationGrid->SetGridStep(grid_step_ran, grid_step_azi);
  filterCorrelationGrid->SetRoughShift_ran(shiftML_range);
  filterCorrelationGrid->SetRoughShift_azi(shiftML_azimut);
  if (nooffset)
    {
      filterCorrelationGrid->SetFirstLCOffset(false);
    }

  
  // Define the main pipeline 
  ExtractROIFilterType::Pointer extractROIFilter_masterfull = ExtractROIFilterType::New();
  m_Ref.push_back(extractROIFilter_masterfull.GetPointer());
  extractROIFilter_masterfull->SetInput(MasterPtr);
  extractROIFilter_masterfull->SetChannel(1);
  ExtractROIFilterType::Pointer extractROIFilter_slavefull = ExtractROIFilterType::New();
  m_Ref.push_back(extractROIFilter_slavefull.GetPointer());
  extractROIFilter_slavefull->SetInput(SlavePtr);
  extractROIFilter_slavefull->SetChannel(1);

  
  filterCorrelationGrid->SetMasterInput(extractROIFilter_masterfull->GetOutput());
  filterCorrelationGrid->SetSlaveInput(extractROIFilter_slavefull->GetOutput());
  SetParameterOutputImage("out", filterCorrelationGrid->GetOutput());
}
   // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARCorrelationGrid)
