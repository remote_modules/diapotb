/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDerampImageFilter_txx
#define otbSARDerampImageFilter_txx

#include "otbSARDerampImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include <cmath>
#include <algorithm>

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImage> 
  SARDerampImageFilter< TImage >::SARDerampImageFilter()
    :  m_DerampMode(true),  m_ShiftMode(false), m_FirstAziTime(), m_FirstRangeTime(0), m_MidAziTime(), 
       m_VSatAtMidAziTime(0), m_Ks(0), m_AziTimeInt(0),m_RangeSamplingRate(0),
       m_RefTime0(0), m_FirstEstimation(true)
  {
    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(1);
    this->SetNumberOfIndexedInputs(2);
  }
    
  /** 
   * Destructor
   */
  template <class TImage> 
  SARDerampImageFilter< TImage >::~SARDerampImageFilter()
  {
   
  }

  /**
   * Print
   */
  template<class TImage>
  void
  SARDerampImageFilter< TImage >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);

    os << indent << "Mode for deramp filter, if true then Deramp Mode else Reramp mode : " << m_DerampMode 
       << std::endl;
  }

  


   /**
   * Set Image
   */ 
  template<class TImage>
  void
 SARDerampImageFilter< TImage >
  ::SetImageInput(const ImageType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageType *>(image));
  }

  /**
   * Set Grid
   */ 
  template<class TImage>
  void
 SARDerampImageFilter< TImage >
  ::SetGridInput(const GridType* grid)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<GridType *>(grid));
  }

  /**
   * Get mage
   */ 
  template<class TImage>
  const typename SARDerampImageFilter< TImage >::ImageType *
  SARDerampImageFilter< TImage >
  ::GetImageInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Grid
   */ 
  template<class TImage>
  const typename SARDerampImageFilter< TImage >::GridType *
  SARDerampImageFilter< TImage >
  ::GetGridInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const GridType *>(this->itk::ProcessObject::GetInput(1));
  }


  /**
   * Get shifts into input grod with the current index
   */
  template<class TImage>
  void
  SARDerampImageFilter<TImage>
  ::getShifts(ImageIndexType index, double & shift_ran, double & shift_azi)
  {
    // Get the index of current tile into grid to retrive the shifts 
    // Output Geo = Master Geo = (Grid geo / GridStep) 
    int Cgrid =  std::round( index[0] / m_GridStep[0]); 
    int Lgrid =  std::round( index[1] / m_GridStep[1]); 
	    
    Cgrid = std::min (std::max (Cgrid, 0), 
		      static_cast<int>(this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0])-1); 
    Lgrid = std::min (std::max (Lgrid, 0), 
		      static_cast<int>(this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1])-1); 
	    
    GridIndexType gridIndex;
    gridIndex[0] = Cgrid;	    
    gridIndex[1] = Lgrid;
	    
    // Shifts 
    shift_ran = this->GetGridInput()->GetPixel(gridIndex)[0];
    shift_azi = this->GetGridInput()->GetPixel(gridIndex)[1];
  }

    /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImage>
  void
  SARDerampImageFilter< TImage >
  ::GenerateInputRequestedRegion()
  { 
    // call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
    // Get Output requested region
    ImageRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
     ///////////// For Input image same region  /////////////
    ImagePointer  inputPtr = const_cast< ImageType * >( this->GetImageInput() );
    inputPtr->SetRequestedRegion(outputRequestedRegion);

    if (m_ShiftMode)
      {
	if (this->GetGridInput() != nullptr)
	  {	    
	    ///////////// Find the region into Shift Grid ////////////
	    GridRegionType gridRequestedRegion = OutputRegionToInputGridRegion(outputRequestedRegion);
	    GridPointer  gridPtr = const_cast< GridType * >( this->GetGridInput() );
	    gridPtr->SetRequestedRegion(gridRequestedRegion);
	  }
	else
	  {
	    itkExceptionMacro(<<"Missing Input Grid for shift mode");
	  }  
      }
  }
  

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImage>
  typename SARDerampImageFilter< TImage >::GridRegionType 
  SARDerampImageFilter< TImage >
  ::OutputRegionToInputGridRegion(const ImageRegionType& outputRegion) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input grid requested region
    // that will provide the proper range
    const ImageSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageIndexType outputRequestedRegionIndex = outputRegion.GetIndex();
        
    // Define the index and size for grid requested region. The input grid is on output/master geometry with
    // m_GridStep as factor
    GridIndexType indexGrid;
    indexGrid[0] = static_cast<GridIndexValueType>(outputRequestedRegionIndex[0]/m_GridStep[0]) - 1; 
    indexGrid[1] = static_cast<GridIndexValueType>(outputRequestedRegionIndex[1]/m_GridStep[1]) - 1;

    GridSizeType sizeGrid;
    sizeGrid[0] = static_cast<GridSizeValueType>(outputRequestedRegionSize[0]/m_GridStep[0]) + 3; 
    sizeGrid[1] = static_cast<GridSizeValueType>(outputRequestedRegionSize[1]/m_GridStep[1]) + 3;

    // Check Index and Size
    if (indexGrid[0] < this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[0])
      {
	indexGrid[0] = this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[0];
      }
    if (indexGrid[1] < this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[1])
      {
	indexGrid[1] = this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[1];
      }
    if ((sizeGrid[0] + indexGrid[0]) > 
	this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0])
      {
	sizeGrid[0] = this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0] - 
	  indexGrid[0];
      }
    if ((sizeGrid[1] + indexGrid[1]) > 
	this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1])
      {
	sizeGrid[1] = this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1] - 
	  indexGrid[1];
      }
    
    // Transform into a region1
    GridRegionType gridRequestedRegion = outputRegion;
    gridRequestedRegion.SetIndex(indexGrid);
    gridRequestedRegion.SetSize(sizeGrid);

    return gridRequestedRegion;
  }


  /**
   * Method ThreadedGenerateData
   */
  template<class TImage>
  void
  SARDerampImageFilter<TImage>
  ::BeforeThreadedGenerateData()
  {
    // Estimates general parameters for the current burst, if m_FirstEstimation == true
    if (m_FirstEstimation)
      {
	m_FirstEstimation = false;

	// Get input
	ImagePointer inputPtr = const_cast< ImageType * >( this->GetImageInput() );
	// Choose Metadata
	ImageMetadata inputKWL;
	if (m_ShiftMode)
	  {
	    if (this->GetGridInput() != nullptr)
	      {
		inputKWL = m_SarKWL;
	      }
	    else
	      {
		itkExceptionMacro(<<"Missing Image Metadata for shift mode");
	      }
	  }
	else
	  {
	    inputKWL = inputPtr->GetImageMetadata();
	  }

	// Check version of header/kwl (at least 3)
	// int headerVersion = std::stoi(inputKWL["header.version"]);

	// if (headerVersion < 3)
	//   {
	//     itkExceptionMacro(<<"Header version is inferior to 3. Please Upgrade your geom file");
	//   }
	
        this->ThreadInit(inputKWL);// this method is overwritten in each subclass 
      }
  }
   

  /**
   * Method ThreadedGenerateData
   */
  template<class TImage>
  void
  SARDerampImageFilter<TImage>
  ::ThreadedGenerateData(const ImageRegionType & outputRegionForThread,
			 itk::ThreadIdType /*threadId*/)
  {
    // Compute corresponding input region for master and slave cartesian mean
    ImageRegionType inputRegionForThread = outputRegionForThread;

    // Iterator on output
    OutputIterator OutIt(this->GetOutput(), outputRegionForThread);
    OutIt.GoToBegin();

    // Iterator on input 
    InputIterator  InIt(this->GetImageInput(), inputRegionForThread);
    InIt.GoToBegin();

    // For each line
    while (!OutIt.IsAtEnd() && !InIt.IsAtEnd())
      {
	OutIt.GoToBeginOfLine();
	InIt.GoToBeginOfLine();


	// For each colunm
	while (!OutIt.IsAtEndOfLine() && !InIt.IsAtEndOfLine()) 
	  {
	    // Output index
	    ImageIndexType index = OutIt.GetIndex();

	    double indC = static_cast<double>(index[0]);
	    double indL = static_cast<double>(index[1]);

	    // If shift mode, apply shifts on current index
	    if (m_ShiftMode)
	      {
		double shift_ran, shift_azi;
		this->getShifts(index, shift_ran, shift_azi);

		indC += shift_ran; 
		indL += shift_azi; 
	      }

	    // Phi
	    double Phi = this->getPhi(indL,indC);// this method is overwritten in each subclass

	    // Deramping or Reramping 
	    if (!m_DerampMode)
	      {
		Phi *= -1;
	      }

	    ImagePixelType outPixel; // Complex 
	    outPixel.real((InIt.Get().real() * std::cos(Phi)) - (InIt.Get().imag() * std::sin(Phi)));
	    outPixel.imag((InIt.Get().real() * std::sin(Phi)) + (InIt.Get().imag() * std::cos(Phi)));
	    
	    //////////// Assign Output ////////////
	    OutIt.Set(outPixel);

	    // Increment iterators
	    ++OutIt;
	    ++InIt;
	  } // End colunms (ouput)

	// Next Line
	OutIt.NextLine();
	InIt.NextLine();
      } // End lines (ouput)
  }


} /*namespace otb*/

#endif
