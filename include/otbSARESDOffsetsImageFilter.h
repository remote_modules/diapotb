/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARESDOffsetsImageFilter_h
#define otbSARESDOffsetsImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"


namespace otb
{
/** \class SARESDOffsetsImageFilter 
 * \brief ESDOffsetss or Reramps a burst from Sentinel-1 IW product. 
 * 
 * This filter deramps or reramps an input burst.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImage> 
  class ITK_EXPORT SARESDOffsetsImageFilter :
    public itk::ImageToImageFilter<TImage,TImage>
{
public:

  // Standard class typedefs
  typedef SARESDOffsetsImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImage,TImage>         Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARESDOffsetsImageFilter,ImageToImageFilter);

  /** Typedef to image input/output type VectorImage (Complex Image)   */
  typedef TImage                                  ImageType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageType::Pointer             ImagePointer;
  typedef typename ImageType::ConstPointer        ImageConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageType::RegionType          ImageRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageType::PixelType           ImagePixelType;
  typedef typename ImageType::PointType           ImagePointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageType::IndexType           ImageIndexType;
  typedef typename ImageType::IndexValueType      ImageIndexValueType;
  typedef typename ImageType::SizeType            ImageSizeType;
  typedef typename ImageType::SizeValueType       ImageSizeValueType;
  typedef typename ImageType::SpacingType         ImageSpacingType;
  typedef typename ImageType::SpacingValueType    ImageSpacingValueType;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageType > OutputIterator;


  // Setter
  itkSetMacro(PhaseDiffMean, float);
  itkSetMacro(AziTimeInt, double);
  
  // Getter
  itkGetMacro(PhaseDiffMean, float);
  itkGetMacro(AziTimeInt, double);

  // Setter/Getter for inputs (two images) 
  /** Connect one of the operands for registration */
  void SetInput1( const ImageType* image);

  /** Connect one of the operands for registration */
  void SetInput2(const ImageType* image);

  /** Get the inputs */
  const ImageType * GetInput1() const;
  const ImageType * GetInput2() const;

protected:
  // Constructor
  SARESDOffsetsImageFilter();

  // Destructor
  virtual ~SARESDOffsetsImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** 
   * SARESDOffsetsImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageRegionType& outputRegionForThread, 
				    itk::ThreadIdType threadId) ITK_OVERRIDE;

  
  /** SARESDOffsetsImageFilter needs a input requested region that corresponds to our output requested region 
   * for all input images.
   * As such, ESDOffsetsImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

   
 private:
  SARESDOffsetsImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 

  float m_PhaseDiffMean;

  double m_AziTimeInt;

  bool m_FirstEstimation;
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARESDOffsetsImageFilter.txx"
#endif



#endif
