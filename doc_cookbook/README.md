To generate quickly the wiki, the process to follow is made up of multiple steps.

However, before it is executed, the module conda must be loaded and the environment sphinx and diapotb must be ready to be loaded.

The environment sphinx can be created with the file doc_sphinx_wiki.yml or we can follow the following steps:
```shell
conda create -n <name_env_sphinx> python=3.7.2
conda activate <name_env_sphinx>
pip install sphinx_rtd_theme
pip install sphinx_markdown_builder
pip install myst-parser
```

1. The module conda is loaded. Then, the environment diapotb is loaded, the environment variables used by the repository diapotb are defined.

2. The source files for the applications C++ are generated:

- First, we go to the repertory `doc_cookbook` inside the repository diapotb:

```shell
cd <diapotb_root_path>/doc_cookbook
```
<diapotb_root_path> : root path of the repository diapotb

- The temporary repertory used to store all the source files is created:
```shell
mkdir -p rst_tmp
cp -R src/* rst_tmp
```

- Then, the rst files are generated:

```shell
python otbGenerateWrappersRstDoc.py rst_tmp
```

3. The new wiki is generated:

- The environment sphinx is loaded:

```shell
conda activate <name_env_sphinx>
```

- We run the shell script `script_doc_sphinx.sh` which will generate the source files for the python script then use all the source files inside the temporary repertory `rst_tmp` to generate the new wiki at the output path requested.

```shell
./script_doc_sphinx.sh <path_wiki_output> <type_output>
```

<path_wiki_output> : Path of the repertory where the new wiki will be generated

<type_output>      : Type of the files generated for the wiki (md or html)

Example:

```shell
./script_doc_sphinx.sh /work/ADM/C3/salehma md
```

We will have the repertory `diapotb_wiki` at `/work/ADM/C3/salehma` with all the new content of the wiki in markdown

For now, the link inside the md files does not appear
