/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbTilesAnalysisWithStepImageFilter_txx
#define otbTilesAnalysisWithStepImageFilter_txx

#include "otbTilesAnalysisWithStepImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include <cmath>
#include <algorithm>

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageIn, class TImageOut, class TFunction> 
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >::
  TilesAnalysisWithStepImageFilter()
    :  m_Step(16)
  {
    this->m_SizeTiles = 64;
    this->m_Margin = 0;

    m_Mutex = new MutexType(); 

    m_saveOutputBuffer = 0;
  }

  /** 
   * Destructor with default initialization
   */
  template <class TImageIn, class TImageOut, class TFunction> 
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >::
  ~TilesAnalysisWithStepImageFilter()
  {
    delete m_Mutex;
    m_Mutex = 0;

    if (m_saveOutputBuffer)
      {
	delete m_saveOutputBuffer;
	m_saveOutputBuffer = 0;
      }
  }
 

  /**
   * Print
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);

    os << indent << "Size of Tiles : " << this->m_SizeTiles << std::endl;
    os << indent << "Step for Tiles building : " << m_Step << std::endl;
    os << indent << "Margin for input requested : " << this->m_Margin << std::endl;
  }

 
  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the input and output
    ImageInConstPointer inputPtr = this->GetImageInput();
    ImageOutPointer     outputPtr = this->GetOutput();

    ImageOutRegionType outputLargestPossibleRegion = inputPtr->GetLargestPossibleRegion();
    ImageOutPointType outOrigin = inputPtr->GetOrigin();
    ImageOutSpacingType outSP = inputPtr->GetSpacing();
    
    // Define output Largest Region
    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSpacing(outSP);
   
    // Check Step and SizeTiles : Step must be a multiple of SizeTiles
    if (m_Step < this->m_SizeTiles)
      {
	if ((this->m_SizeTiles%m_Step) != 0)
	  {
	    itkExceptionMacro(<<"Step must be multiple of SizeTiles.");
	  }
      }
    else
      {
	itkExceptionMacro(<<"Step must be inferior and multiple of SizeTiles.");
      }
    
  }

  
  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateInputRequestedRegion()
  {
    ////// Set Output Requestion Region (Contruct Tiles with given size and step for output) ///////
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

    // New Index : closest inf multiple of Step and shift to this->m_SizeTiles - m_Step 
    ImageOutIndexType outputIndex;

    
    if (outputRequestedRegion.GetIndex()[0]/this->m_Step < (this->m_SizeTiles/m_Step))
      {
	outputIndex[0] = outputRequestedRegion.GetIndex()[0];

	m_expectedIndex[0] = outputIndex[0]; 
      }
    else
      {
	outputIndex[0] = (static_cast<int>(outputRequestedRegion.GetIndex()[0]/this->m_Step))*
	  this->m_Step - (this->m_SizeTiles - m_Step) ;

	m_expectedIndex[0] = m_saveIndex[0] + m_saveSize[0] - (this->m_SizeTiles - m_Step); 
      }

    if (outputRequestedRegion.GetIndex()[1]/this->m_Step < (this->m_SizeTiles/m_Step))
      {
	outputIndex[1] = outputRequestedRegion.GetIndex()[1];
	m_expectedIndex[1] = outputIndex[1];
      }
    else
      {
	outputIndex[1] = (static_cast<int>(outputRequestedRegion.GetIndex()[1]/this->m_Step))*
	 this->m_Step - (this->m_SizeTiles - m_Step);

	m_expectedIndex[1] = m_saveIndex[1] + m_saveSize[1] - (this->m_SizeTiles - m_Step);
      }


    // New Size : closest sup multiple of Step with addtional when difference between new and 
    // old index and SizeTiles to handles the last tiles
    ImageOutSizeType outputSize;

    outputSize[0] = (static_cast<int>((outputRequestedRegion.GetIndex()[0] - outputIndex[0] +
				       outputRequestedRegion.GetSize()[0])/this->m_Step))*
                                       this->m_Step + this->m_SizeTiles;
    outputSize[1] = (static_cast<int>((outputRequestedRegion.GetIndex()[1] - outputIndex[1] +
				       outputRequestedRegion.GetSize()[1])/this->m_Step))*
		                       this->m_Step + this->m_SizeTiles;

    // Region affectation (with Crop to respect image dimension)
    outputRequestedRegion.SetSize(outputSize);
    outputRequestedRegion.SetIndex(outputIndex);
    outputRequestedRegion.Crop(this->GetOutput()->GetLargestPossibleRegion());
    this->GetOutput()->SetRequestedRegion(outputRequestedRegion);


    // input requested region same than output
    ImageInRegionType inputRequestedRegion = outputRequestedRegion;
    ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetImageInput() );
    inputPtr->SetRequestedRegion(inputRequestedRegion);

    // Store index and size (for next call of this function)
    m_saveIndex[0] = outputIndex[0]; 
    m_saveIndex[1] = outputIndex[1];
    m_saveSize[0] = outputSize[0]; 
    m_saveSize[1] = outputSize[1];
  }

  /**
   * Method BeforeThreadedGenerateData
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >
 ::BeforeThreadedGenerateData()
  {
    // Typedef for Iterators on selected region
    typedef itk::ImageRegionIterator<ImageOutType> OutItType;

    // Initialize output region 
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

    
    OutItType OutIt(this->GetOutput(), outputRequestedRegion);
    OutIt.GoToBegin();

    ImageOutIndexType first_index = OutIt.GetIndex();
   
    // Limitations : Works only with a streaing by strips
    // TODO : See to allow others kind of streamings
    int counter = (m_oldSize[1] - (this->m_SizeTiles - m_Step + 
				   m_expectedIndex[1]-m_saveIndex[1])) * m_oldSize[0];


    while (!OutIt.IsAtEnd())
      {
	ImageOutIndexType index_current = OutIt.GetIndex();
	
	// First Streaming
	if (first_index[0] < (this->m_SizeTiles-m_Step) && 
	    first_index[1] < (this->m_SizeTiles-m_Step))
	  {
	    OutIt.Set(0);
	  }
	else
	  {
	    // First pixels have already been initialized or estimated => saveBuffer 
	    // and 0 for others
	    // TODO : See to allow another kind of streaming (not only strips)
	    if ((index_current[1]) >= (m_expectedIndex[1] + this->m_SizeTiles - m_Step)) 
	      {
		// Init for accumulations
		OutIt.Set(0);
	      }
	    else
	      {	
		// Keep values from last streaming
		OutIt.Set(m_saveOutputBuffer[counter]);
		
		counter++; 
	      }

	  }
	
	++OutIt;
      }

    // Free memory
    delete m_saveOutputBuffer;
    m_saveOutputBuffer = 0;
  }


  /**
   * Method AfterThreadedGenerateData
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >
  ::AfterThreadedGenerateData()
  {
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

    // Store all output region 
    ImageOutIndexType saveOutputIndex;
    saveOutputIndex[0] = outputRequestedRegion.GetIndex()[0];
    saveOutputIndex[1] = outputRequestedRegion.GetIndex()[1];

    ImageOutSizeType saveOutputSize;
    saveOutputSize[0] = outputRequestedRegion.GetSize()[0];
    saveOutputSize[1] = outputRequestedRegion.GetSize()[1];

    ImageOutRegionType saveOutputRegion;
    saveOutputRegion.SetSize(saveOutputSize);
    saveOutputRegion.SetIndex(saveOutputIndex);
    saveOutputRegion.Crop(this->GetOutput()->GetLargestPossibleRegion());

    m_oldSize[0] = saveOutputSize[0];
    m_oldSize[1] = saveOutputSize[1];
    
    // Typedef for Iterators on selected region
    typedef itk::ImageRegionIterator<ImageOutType> OutItType;

    OutItType OutIt(this->GetOutput(), saveOutputRegion);
    OutIt.GoToBegin();


    m_saveOutputBuffer = new ImageOutPixelType[outputRequestedRegion.GetSize()[0]*
					       outputRequestedRegion.GetSize()[1]];


    // Loop for assignations
    int counter = 0;
    while (!OutIt.IsAtEnd())
      {
	
	m_saveOutputBuffer[counter] = OutIt.Get();

	++counter; 
	++OutIt;
      }
    
  }


  /**
   * Method GenerateData
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateData()
  {
    // Allocate outputs
    this->AllocateOutputs();

    int nbThreads = this->GetNumberOfThreads();
	
    // Get Output Requested region
    ImageOutRegionType outputRegion = this->GetOutput()->GetRequestedRegion();
            
    // Define the number of tiles for processing
    int nbTiles_Lines = (outputRegion.GetSize()[1]-this->m_SizeTiles)/m_Step; 
    int nbTiles_Col = (outputRegion.GetSize()[0]-this->m_SizeTiles)/m_Step;
    

    // Check if additionnal tiles are required
    if (outputRegion.GetSize()[1] % m_Step != 0)  
      {
	++nbTiles_Lines;
      }
    if (outputRegion.GetSize()[0] % m_Step != 0)
      {
	++nbTiles_Col;
      }
    
    // First Tile = index of output requested region
    //int IndL_FirstTiles = outputRegion.GetIndex()[1];
    //int IndC_FirstTiles = outputRegion.GetIndex()[0];
    
    // First Tile = index of expected output requested region (real output region might be larger 
    // than expected) => do not process all tiles because some of its had been estimated.
    int IndL_FirstTiles = m_expectedIndex[1];
    int IndC_FirstTiles = m_expectedIndex[0];
     
    nbTiles_Lines -=  (m_expectedIndex[1] - outputRegion.GetIndex()[1])/m_Step;
    nbTiles_Col -=  (m_expectedIndex[0] - outputRegion.GetIndex()[0])/m_Step;

    nbTiles_Lines++;

    ///////////////////////// Pre Processing ///////////////////////
    // Call Function initialization
    this->m_FunctionOnTiles->Initialize(nbThreads);

    this->m_NbTiles = nbTiles_Lines*nbTiles_Col;
    this->m_NbTilesCol = nbTiles_Col;
    this->m_IndLFirstTile = IndL_FirstTiles;
    this->m_IndCFirstTile = IndC_FirstTiles;
    
    this->BeforeThreadedGenerateData();

    ///////////////////////// Processing ///////////////////////
    // Set up the multithreaded processing
    ThreadStruct str;
    str.Filter = this;

    // Setting up multithreader
    this->GetMultiThreader()->SetNumberOfThreads(this->GetNumberOfThreads());
    this->GetMultiThreader()->SetSingleMethod(this->ThreaderCallback, &str);

    // multithread the execution
    this->GetMultiThreader()->SingleMethodExecute();
    
    ///////////// Post Processing ////////////
    this->AfterThreadedGenerateData();
    
    // Call Function terminate
    this->m_FunctionOnTiles->Terminate();
  }



 template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >
 ::ThreadedGenerateData(unsigned int startTileId, unsigned int stopTileId, 
			unsigned int firstTileIndexL, unsigned int firstTileIndexC, 
			itk::ThreadIdType threadId)
 {
   ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetImageInput() );

   // Typedef for Iterators on selected region
   typedef itk::ImageRegionIterator<ImageOutType> OutItType;


   for (unsigned int k = startTileId; k < stopTileId; k++)  //<= ??????
     {
       // Transform Tiles id into index
       ImageOutIndexType index_current;
       int id_L = k/this->m_NbTilesCol;        // quotient of Euclidean division (integer division)
       int id_C = k - id_L*this->m_NbTilesCol; // remainder of Euclidean division
       index_current[1] = firstTileIndexL + id_L*m_Step;
       index_current[0] = firstTileIndexC + id_C*m_Step;
	    
       // Construct the current tile
       ImageOutIndexType currentOutTilesIndex;
       ImageOutSizeType currentOutTilesSize;
	    
       // Create regions with index and the given size
       // Output tiles
       currentOutTilesIndex[0] = index_current[0];
       currentOutTilesIndex[1] = index_current[1];
       currentOutTilesSize[0] = this->m_SizeTiles;
       currentOutTilesSize[1] = this->m_SizeTiles;
       ImageOutRegionType outputCurrentRegion;
       outputCurrentRegion.SetIndex(currentOutTilesIndex);
       outputCurrentRegion.SetSize(currentOutTilesSize);
	
       outputCurrentRegion.Crop(this->GetOutput()->GetLargestPossibleRegion());		

       // Process the current tile
       ImageOutPixelType * TilesCache = new ImageOutPixelType[outputCurrentRegion.GetSize()[1]*
							      outputCurrentRegion.GetSize()[0]];

       // Call Function Process : 
       // Inputs : Ptr Image In and the output tile
       // Output : Ptr on result array
       this->m_FunctionOnTiles->Process(inputPtr, outputCurrentRegion, TilesCache, threadId);

       int compteur = 0;

       // Assignate Output with function result
       // Mutex to guarantee Thread-Safety
       // Iterators on output
       OutItType OutIt(this->GetOutput(), outputCurrentRegion);
       OutIt.GoToBegin();
       m_Mutex->Lock();
       while (!OutIt.IsAtEnd())
	 {
	   // Accumulations
	   ImageOutPixelType outTmp = OutIt.Get();
	   OutIt.Set(outTmp + TilesCache[compteur]);

	   ++compteur; 
	   ++OutIt;
	 }
       m_Mutex->Unlock();
	   
       delete [] TilesCache;
       TilesCache = 0;
	   
     }
 }


  // Callback routine used by the threading library. This routine just calls
  // the ThreadedGenerateData method after setting the correct region for this
  // thread.
  template<class TImageIn, class TImageOut, class TFunction>
  ITK_THREAD_RETURN_TYPE
  TilesAnalysisWithStepImageFilter< TImageIn, TImageOut, TFunction >
  ::ThreaderCallback(void *arg)
  {
    ThreadStruct *str;
    int           threadId, threadCount;
    unsigned int  total, start, stop, indLFirstTile, indCFirstTile;

    threadId = ((itk::MultiThreader::ThreadInfoStruct *) (arg))->ThreadID;
    threadCount = ((itk::MultiThreader::ThreadInfoStruct *) (arg))->NumberOfThreads;
    str = (ThreadStruct *) (((itk::MultiThreader::ThreadInfoStruct *) (arg))->UserData);

    total = str->Filter->GetNbTiles();
    indLFirstTile = str->Filter->GetIndLFirstTile();
    indCFirstTile = str->Filter->GetIndCFirstTile();


    if (threadId < static_cast<int>(total))
      {
	start =
	  static_cast<unsigned int>(threadId * 
				    std::ceil(static_cast<double>(total)/(static_cast<double>(threadCount))));
	stop =
	  static_cast<unsigned int>((threadId+1) * 
				    std::ceil(static_cast<double>(total)/(static_cast<double>(threadCount))));


	if (stop > total) stop = total;

	// For very small graphs it might occur that start = stop. In this
	// case the vertex at that index will be processed in the next strip.
	if (start != stop)
	  {
	    str->Filter->ThreadedGenerateData(start, stop, indLFirstTile, indCFirstTile, threadId);
	  }
      }
    // else
    //   {
    //   otherwise don't use this thread. Sometimes the threads don't
    //   break up very well and it is just as efficient to leave a
    //   few threads idle.
    //   }

    return ITK_THREAD_RETURN_VALUE;
  }




} /*namespace otb*/

#endif
