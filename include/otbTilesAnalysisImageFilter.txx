/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbTilesAnalysisImageFilter_txx
#define otbTilesAnalysisImageFilter_txx

#include "otbTilesAnalysisImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include <cmath>
#include <algorithm>

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageIn, class TImageOut, class TFunction> 
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::TilesAnalysisImageFilter()
    :  m_SizeTiles(50), m_Margin(7), m_UseCache(false), m_MaxMemoryCacheInMB(0), m_NbTiles(0), m_NbTilesCol(0), 
       m_IndLFirstTile(0), m_IndCFirstTile(0), m_MaxShiftInRange(0.), m_MaxShiftInAzimut(0.), 
       m_UseMasterGeo(false)
  {
    m_ProcessedTilesIndex.clear();
    m_ProcessedTiles.clear();

    m_GridStep.Fill(1);

    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(1);
    this->SetNumberOfIndexedInputs(2);
  }
    
  /** 
   * Destructor
   */
  template <class TImageIn, class TImageOut, class TFunction> 
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::~TilesAnalysisImageFilter()
  {
    // Clear our caches
    typedef typename std::map<int, ImageOutPixelType *>::iterator mapIt;

    if (!m_ProcessedTiles.empty())
      {
	// Free Memory into m_ProcessedTiles
	for (mapIt mapPT = m_ProcessedTiles.begin(); mapPT != m_ProcessedTiles.end(); ++mapPT)
	  {
	      delete mapPT->second;
	      mapPT->second = 0;    
	  }	

	m_ProcessedTiles.clear();
	m_ProcessedTilesIndex.clear();
      }
  }

  /**
   * Print
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);

    os << indent << "Size of Tiles : " << m_SizeTiles << std::endl;
    os << indent << "Margin for input requested : " << m_Margin << std::endl;
  }

    /**
   * SetFunctorPtr
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetFunctorPtr(FunctionPointer functorPtr)
  {
    m_FunctionOnTiles = functorPtr;
  }

    /**
   * Set (Slave) Image
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  void
 TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetImageInput(const ImageInType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageInType *>(image));
  }

  /**
   * Set Grid
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  void
 TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetGridInput(const GridType* grid)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<GridType *>(grid));
  }

  /**
   * Get (Slave) Image
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  const typename TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::ImageInType *
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GetImageInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Grid
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  const typename TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::GridType *
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GetGridInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const GridType *>(this->itk::ProcessObject::GetInput(1));
  }


  /**
   * SetConfigForCache
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetConfigForCache(int maxMemoryInMB)
  {
    m_UseCache = true;
    m_MaxMemoryCacheInMB = maxMemoryInMB;
  }

    /**
   * Set Sar Image Metadata
   */ 
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetMasterImagePtr(ImageInPointer masterImagePtr)
  {
    // Check if masterImageMD not NULL
    assert(masterImagePtr && "Master Image Metadata don't exist.");
    m_MasterImagePtr = masterImagePtr;
    m_UseMasterGeo = true;
  }
  
/**
 * SetGridParameters
 */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::SetGridParameters(float maxShiftInRange, float maxShiftInAzimut, unsigned int gridStepRange, 
		      unsigned int gridStepAzimut)
  {
    m_MaxShiftInRange = maxShiftInRange;
    m_MaxShiftInAzimut = maxShiftInAzimut;
    m_GridStep[0] = gridStepRange;
    m_GridStep[1] = gridStepAzimut;

    int gridStep = std::max(gridStepRange, gridStepAzimut);

    // Check the max shift
    if (m_MaxShiftInRange > 10e5 || m_MaxShiftInAzimut > 10e5)
      {
	itkExceptionMacro(<<"Provided MaxShifts are not consistents. Check the input grid");
      }

    // Adapt the tolerance for the two inputs
    this->SetCoordinateTolerance(gridStep);
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the input and output
    ImageInConstPointer inputPtr = this->GetImageInput();
    ImageOutPointer     outputPtr = this->GetOutput();

    ImageOutRegionType outputLargestPossibleRegion = inputPtr->GetLargestPossibleRegion();
    ImageOutPointType outOrigin = inputPtr->GetOrigin();
    ImageOutSpacingType outSP = inputPtr->GetSpacing();
    // The output can be defined with the m_MasterImageKwl
    // Origin, Spacing and Size 
    if (m_UseMasterGeo)
      {
	ImageOutSizeType outputSize;
	outputSize[0] = m_MasterImagePtr->GetLargestPossibleRegion().GetSize()[0];
	outputSize[1] = m_MasterImagePtr->GetLargestPossibleRegion().GetSize()[1];
	
	outOrigin = m_MasterImagePtr->GetOrigin();
	
	outSP = m_MasterImagePtr->GetSpacing();

	// Define Output Largest Region
	outputLargestPossibleRegion = m_MasterImagePtr->GetLargestPossibleRegion();
	outputLargestPossibleRegion.SetSize(outputSize);

	// Metadata
	ImageMetadata outputMD = m_MasterImagePtr->GetImageMetadata();
	outputPtr->SetImageMetadata(outputMD);
      }
    
    // Define output Largest Region
    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSpacing(outSP);
    

    // Check gridSteps
    if (this->GetGridInput() != nullptr)
      {
	ImageMetadata gridMD = this->GetGridInput()->GetImageMetadata();
	if (gridMD.Has("gridstep.range") && gridMD.Has("gridstep.azimut"))
	  {
	    unsigned int gridStepRange = atoi(gridMD["gridstep.range"].c_str());
	    unsigned int gridStepAzimut = atoi(gridMD["gridstep.azimut"].c_str());

	    if (gridStepRange != m_GridStep[0] || gridStepAzimut != m_GridStep[1])
	      {
		itkExceptionMacro(<<"Provided GridSteps are not consistent with grid keywordlist.");
	      }
	  }
      }
  }

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut, class TFunction >
  typename TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::ImageInRegionType 
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::OutputRegionToInputRegion(const ImageOutRegionType& outputRegion) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();

    // Define the margin for each dimension to get the correct input requested region
    int margin_range = m_Margin + static_cast<int>(std::abs(m_MaxShiftInRange)) + 1;
    int margin_azimut = m_Margin + static_cast<int>(std::abs(m_MaxShiftInAzimut)) + 1;

    // Add to output region Margin to get input region
    ImageInIndexType inputRequestedRegionIndex; 
    inputRequestedRegionIndex[0] = static_cast<ImageInIndexValueType>(outputRequestedRegionIndex[0] - 
								      margin_range);
    inputRequestedRegionIndex[1] = static_cast<ImageInIndexValueType>(outputRequestedRegionIndex[1] - 
								      margin_azimut);
    ImageInSizeType inputRequestedRegionSize;
    inputRequestedRegionSize[0] = static_cast<ImageInSizeValueType>(outputRequestedRegionSize[0] + 
								    2*margin_range);
    inputRequestedRegionSize[1] = static_cast<ImageInSizeValueType>(outputRequestedRegionSize[1] + 
								    2*margin_azimut);  
   
    // Check Index and Size
    if (inputRequestedRegionIndex[0] < this->GetImageInput()->GetLargestPossibleRegion().GetIndex()[0])
      {
	inputRequestedRegionIndex[0] = this->GetImageInput()->GetLargestPossibleRegion().GetIndex()[0];
      }
    if (inputRequestedRegionIndex[1] < this->GetImageInput()->GetLargestPossibleRegion().GetIndex()[1])
      {
	inputRequestedRegionIndex[1] = this->GetImageInput()->GetLargestPossibleRegion().GetIndex()[1];
      }
    if ((inputRequestedRegionSize[0] + inputRequestedRegionIndex[0]) > 
	this->GetImageInput()->GetLargestPossibleRegion().GetSize()[0])
      {
	inputRequestedRegionSize[0] = this->GetImageInput()->GetLargestPossibleRegion().GetSize()[0] - 
	  inputRequestedRegionIndex[0];
      }
    if ((inputRequestedRegionSize[1] + inputRequestedRegionIndex[1]) > 
	this->GetImageInput()->GetLargestPossibleRegion().GetSize()[1])
      {
	inputRequestedRegionSize[1] = this->GetImageInput()->GetLargestPossibleRegion().GetSize()[1] - 
	  inputRequestedRegionIndex[1];
      }


    // Transform into a region
    ImageInRegionType inputRequestedRegion = outputRegion;
    inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
    inputRequestedRegion.SetSize(inputRequestedRegionSize);
    
    //inputRequestedRegion.Crop(this->GetImageInput()->GetLargestPossibleRegion());
    
    return inputRequestedRegion;  
  }

/** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut, class TFunction >
  typename TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >::GridRegionType 
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::OutputRegionToInputGridRegion(const ImageOutRegionType& outputRegion) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input grid requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();
        
    // Define the index and size for grid requested region. The input grid is on output/master geometry with
    // m_GridStep as factor
    GridIndexType indexGrid;
    indexGrid[0] = static_cast<GridIndexValueType>(outputRequestedRegionIndex[0]/m_GridStep[0]) - 1; 
    indexGrid[1] = static_cast<GridIndexValueType>(outputRequestedRegionIndex[1]/m_GridStep[1]) - 1;

    GridSizeType sizeGrid;
    sizeGrid[0] = static_cast<GridSizeValueType>(outputRequestedRegionSize[0]/m_GridStep[0]) + 3; 
    sizeGrid[1] = static_cast<GridSizeValueType>(outputRequestedRegionSize[1]/m_GridStep[1]) + 3;

    // Check Index and Size
    if (indexGrid[0] < this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[0])
      {
	indexGrid[0] = this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[0];
      }
    if (indexGrid[1] < this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[1])
      {
	indexGrid[1] = this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[1];
      }
    if ((sizeGrid[0] + indexGrid[0]) > 
	this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0])
      {
	sizeGrid[0] = this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0] - 
	  indexGrid[0];
      }
    if ((sizeGrid[1] + indexGrid[1]) > 
	this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1])
      {
	sizeGrid[1] = this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1] - 
	  indexGrid[1];
      }
    
    // Transform into a region1
    GridRegionType gridRequestedRegion = outputRegion;
    gridRequestedRegion.SetIndex(indexGrid);
    gridRequestedRegion.SetSize(sizeGrid);

    return gridRequestedRegion;
  }

  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateInputRequestedRegion()
  {
    ///////////// Set Output Requestion Region (Contruct Tiles with given size for output) ///////////////
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
    
    // New Index : closest inf multiple of SizeTiles
    ImageOutIndexType outputIndex;
    outputIndex[0] = (static_cast<int>(outputRequestedRegion.GetIndex()[0]/m_SizeTiles))*m_SizeTiles;;
    outputIndex[1] = (static_cast<int>(outputRequestedRegion.GetIndex()[1]/m_SizeTiles))*m_SizeTiles;

    // New Size : closest sup multiple of SizeTiles with addtional when difference between new and old index is 
    // too important
    ImageOutSizeType outputSize;
    outputSize[0] = (static_cast<int>((outputRequestedRegion.GetIndex()[0] - outputIndex[0] + 
				       outputRequestedRegion.GetSize()[0])/m_SizeTiles) + 1)*m_SizeTiles;
    outputSize[1] = (static_cast<int>((outputRequestedRegion.GetIndex()[1] - outputIndex[1] + 
				       outputRequestedRegion.GetSize()[1])/m_SizeTiles) + 1)*m_SizeTiles;
    
    
    // Region affectation (with Crop to respect image dimension)
    outputRequestedRegion.SetSize(outputSize);
    outputRequestedRegion.SetIndex(outputIndex);
    outputRequestedRegion.Crop(this->GetOutput()->GetLargestPossibleRegion());
    this->GetOutput()->SetRequestedRegion(outputRequestedRegion);
    
    ///////////// With the new output requested region, find the region into input image /////////////
    ImageInRegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion);
    ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetImageInput() );
    inputPtr->SetRequestedRegion(inputRequestedRegion);

    ///////////// With the new output requested region, find the region into grid (if needed) /////////////
    if (this->GetGridInput() != nullptr)
      {
	GridRegionType gridRequestedRegion = OutputRegionToInputGridRegion(outputRequestedRegion);
	GridPointer  gridPtr = const_cast< GridType * >( this->GetGridInput() );
	gridPtr->SetRequestedRegion(gridRequestedRegion);
      }
  }


  /**
   * Method GenerateData
   */
  template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::GenerateData()
  {
    // Allocate outputs
    this->AllocateOutputs();

    int nbThreads = this->GetNumberOfThreads();
	
    // Get Output Requested region
    ImageOutRegionType outputRegion = this->GetOutput()->GetRequestedRegion();
            
    // Define the number of tiles for processing
    int nbTiles_Lines = outputRegion.GetSize()[1]/m_SizeTiles; 
    int nbTiles_Col = outputRegion.GetSize()[0]/m_SizeTiles;
    
    // Check if additionnal tiles are required
    if (outputRegion.GetSize()[1] % m_SizeTiles != 0)  
      {
	++nbTiles_Lines;
      }
    if (outputRegion.GetSize()[0] % m_SizeTiles != 0)
      {
	++nbTiles_Col;
      }
    
    // First Tile = index of output requested region
    int IndL_FirstTiles = outputRegion.GetIndex()[1];
    int IndC_FirstTiles = outputRegion.GetIndex()[0];
     
    ///////////////////////// Pre Processing ///////////////////////
    // Call Function initialization
    m_FunctionOnTiles->Initialize(nbThreads);

    m_NbTiles = nbTiles_Lines*nbTiles_Col;
    m_NbTilesCol = nbTiles_Col;
    m_IndLFirstTile = IndL_FirstTiles;
    m_IndCFirstTile = IndC_FirstTiles;

    ///////////////////////// Processing ///////////////////////
    // Set up the multithreaded processing
    ThreadStruct str;
    str.Filter = this;

    // Setting up multithreader
    this->GetMultiThreader()->SetNumberOfThreads(this->GetNumberOfThreads());
    this->GetMultiThreader()->SetSingleMethod(this->ThreaderCallback, &str);

    // multithread the execution
    this->GetMultiThreader()->SingleMethodExecute();
    
    ///////////// Post Processing ////////////
    // Call Function terminate
    m_FunctionOnTiles->Terminate();
  }


 template<class TImageIn, class TImageOut, class TFunction>
  void
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
 ::ThreadedGenerateData(unsigned int startTileId, unsigned int stopTileId, 
			unsigned int firstTileIndexL, unsigned int firstTileIndexC, 
			itk::ThreadIdType threadId)
 {
   ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetImageInput() );

   // Typedef for Iterators on selected region
   typedef itk::ImageRegionIterator<ImageOutType> OutItType;

   for (unsigned int k = startTileId; k < stopTileId; k++)  //<= ??????
     {
       // Transform Tiles id into index
       ImageOutIndexType index_current;
       int id_L = k/m_NbTilesCol;        // quotient of Euclidean division (integer division)
       int id_C = k - id_L*m_NbTilesCol; // remainder of Euclidean division
       index_current[1] = firstTileIndexL + id_L*m_SizeTiles;
       index_current[0] = firstTileIndexC + id_C*m_SizeTiles;
	    
       // Construct the current tile
       ImageOutIndexType currentOutTilesIndex;
       ImageOutSizeType currentOutTilesSize;
	    
       // Create regions with index and the given size
       // Output tiles
       currentOutTilesIndex[0] = index_current[0];
       currentOutTilesIndex[1] = index_current[1];
       currentOutTilesSize[0] = m_SizeTiles;
       currentOutTilesSize[1] = m_SizeTiles;
       ImageOutRegionType outputCurrentRegion;
       outputCurrentRegion.SetIndex(currentOutTilesIndex);
       outputCurrentRegion.SetSize(currentOutTilesSize);
	
       outputCurrentRegion.Crop(this->GetOutput()->GetLargestPossibleRegion());		

       // Process the current tile
       ImageOutPixelType * TilesCache = new ImageOutPixelType[outputCurrentRegion.GetSize()[1]*
							      outputCurrentRegion.GetSize()[0]];

       // Call Function Process : 
       // Inputs : Ptr Image In and the output tile
       // Output : Ptr on result array
       m_FunctionOnTiles->Process(inputPtr, outputCurrentRegion, TilesCache, threadId);

       int compteur = 0;

       // Assignate Output with function result
       // Iterators on output
       OutItType OutIt(this->GetOutput(), outputCurrentRegion);
       OutIt.GoToBegin();
       while (!OutIt.IsAtEnd())
	 {
	   OutIt.Set(TilesCache[compteur]);
	   ++compteur; 
	   ++OutIt;
	 }

	   
       delete [] TilesCache;
       TilesCache = 0;
	   
     }
 }


  // Callback routine used by the threading library. This routine just calls
  // the ThreadedGenerateData method after setting the correct region for this
  // thread.
  template<class TImageIn, class TImageOut, class TFunction>
  ITK_THREAD_RETURN_TYPE
  TilesAnalysisImageFilter< TImageIn, TImageOut, TFunction >
  ::ThreaderCallback(void *arg)
  {
    ThreadStruct *str;
    int           threadId, threadCount;
    unsigned int  total, start, stop, indLFirstTile, indCFirstTile;

    threadId = ((itk::MultiThreader::ThreadInfoStruct *) (arg))->ThreadID;
    threadCount = ((itk::MultiThreader::ThreadInfoStruct *) (arg))->NumberOfThreads;
    str = (ThreadStruct *) (((itk::MultiThreader::ThreadInfoStruct *) (arg))->UserData);

    total = str->Filter->GetNbTiles();
    indLFirstTile = str->Filter->GetIndLFirstTile();
    indCFirstTile = str->Filter->GetIndCFirstTile();

    if (threadId < static_cast<int>(total))
      {
	start =
	  static_cast<unsigned int>(threadId * 
				    std::ceil(static_cast<double>(total)/(static_cast<double>(threadCount))));
	stop =
	  static_cast<unsigned int>((threadId+1) * 
				    std::ceil(static_cast<double>(total)/(static_cast<double>(threadCount))));

	if (stop > total) stop = total;

	// For very small graphs it might occur that start = stop. In this
	// case the vertex at that index will be processed in the next strip.
	if (start != stop)
	  {
	    str->Filter->ThreadedGenerateData(start, stop, indLFirstTile, indCFirstTile, threadId);
	  }
      }
    // else
    //   {
    //   otherwise don't use this thread. Sometimes the threads don't
    //   break up very well and it is just as efficient to leave a
    //   few threads idle.
    //   }

    return ITK_THREAD_RETURN_VALUE;
  }



} /*namespace otb*/

#endif
