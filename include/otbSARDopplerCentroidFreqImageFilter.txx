/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDopplerCentroidFreqImageFilter_txx
#define otbSARDopplerCentroidFreqImageFilter_txx

#include "otbSARDopplerCentroidFreqImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include "otbSarSensorModel.h"

#include <cmath>
#include <algorithm>

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImage> 
  SARDopplerCentroidFreqImageFilter< TImage >::SARDopplerCentroidFreqImageFilter()
    :  m_FirstAziTime(), m_FirstRangeTime(0), m_MidAziTime(),
       m_VSatAtMidAziTime(0), m_Ks(0), m_AziTimeInt(0),m_RangeSamplingRate(0), m_FM_C0(0), m_FM_C1(0), 
       m_FM_C2(0), m_FM_Tau0(0), m_DCF_C0(0), m_DCF_C1(0), m_DCF_C2(0), m_DCF_Tau0(0), m_RefTime0(0), 
       m_FirstEstimation(true)
  {
  }
    
  /** 
   * Destructor
   */
  template <class TImage> 
  SARDopplerCentroidFreqImageFilter< TImage >::~SARDopplerCentroidFreqImageFilter()
  {
   
  }

  /**
   * Print
   */
  template<class TImage>
  void
  SARDopplerCentroidFreqImageFilter< TImage >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);
  }

  template<class TImage>
  void
  SARDopplerCentroidFreqImageFilter< TImage >
  ::getAllCoefs(ImageMetadata  const& kwl, std::vector<FMRateRecordType> & FMRateRecords)
  {
    char fmRatePrefix_[1024];
    std::size_t nbLists(0);
    
    nbLists = std::stoi(kwl["azimuthFmRate.azi_fm_rate_coef_nb_list"]);

    std::string FM_PREFIX = "azimuthFmRate.azi_fm_rate_coef_list";
    
    for (std::size_t listId=0; listId!= nbLists ; ++listId) 
      {
	const int pos = sprintf(fmRatePrefix_, "%s%zu.", FM_PREFIX.c_str(), (listId+1));
	assert(pos > 0 && (unsigned long) pos < sizeof(fmRatePrefix_));
	const std::string FMPrefix(fmRatePrefix_, pos);

	FMRateRecordType fmRateRecord;
	fmRateRecord.azimuthFMRateTime = otb::MetaData::ReadFormattedDate(kwl[FMPrefix+ 
										      "azi_fm_rate_coef_time"]);
	fmRateRecord.coef0FMRate = std::stod(kwl[FMPrefix + "1.azi_fm_rate_coef"]);
	fmRateRecord.coef1FMRate = std::stod(kwl[FMPrefix + "2.azi_fm_rate_coef"]);
	fmRateRecord.coef2FMRate = std::stod(kwl[FMPrefix + "3.azi_fm_rate_coef"]);
	fmRateRecord.tau0FMRate = std::stod(kwl[FMPrefix + "slant_range_time"]);

	FMRateRecords.push_back(fmRateRecord);
      }
  }

  template<class TImage>
  void
  SARDopplerCentroidFreqImageFilter< TImage >
  ::getAllCoefs(ImageMetadata  const& kwl, std::vector<DCFRecordType> & DCFRecords)
  {
     char dcfPrefix_[1024];
    std::size_t nbLists(0);
    
    nbLists = std::stoi(kwl["dopplerCentroid.dop_coef_nb_list"]);

    std::string DCF_PREFIX = "dopplerCentroid.dop_coef_list";
    
    for (std::size_t listId=0; listId!= nbLists ; ++listId) 
      {
	const int pos = sprintf(dcfPrefix_, "%s%zu.", DCF_PREFIX.c_str(), (listId+1));
	assert(pos > 0 && (unsigned long) pos < sizeof(dcfPrefix_));
	const std::string DCFPrefix(dcfPrefix_, pos);

	DCFRecordType dcfRecord;
	dcfRecord.azimuthDCFTime = otb::MetaData::ReadFormattedDate(kwl[DCFPrefix + 
									      "dop_coef_time"]);
	dcfRecord.coef0DCF = std::stod(kwl[DCFPrefix + "1.dop_coef"]);
	dcfRecord.coef1DCF = std::stod(kwl[DCFPrefix + "2.dop_coef"]);
	dcfRecord.coef2DCF = std::stod(kwl[DCFPrefix + "3.dop_coef"]);
	dcfRecord.tau0DCF = std::stod(kwl[DCFPrefix + "slant_range_time"]);

	 DCFRecords.push_back(dcfRecord);
      }
  }

  /**
   * Method selectFMRateCoef
   */
  template<class TImage>
  bool 
  SARDopplerCentroidFreqImageFilter< TImage >
  ::selectFMRateCoef()
  {
    // Get input
    ImagePointer inputPtr = const_cast< ImageType * >( this->GetInput() );

     // Retrieve all polynomials
    std::vector<otb::AzimuthFmRate> FMRateRecords;

    ImageMetadata inputKWL = inputPtr->GetImageMetadata();
    
    //this->getAllCoefs(inputKWL, FMRateRecords);
    FMRateRecords = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).azimuthFmRates;

    DurationType diffAziTimeMin;
    unsigned int polySelected = 0;
    
    // Select one polynomial (with m_MidAziTime)
    for (unsigned int i = 0; i < FMRateRecords.size(); i++)
      {
	// Difference between midAziTime and aziTime of the current polynomial
	DurationType diffAziTime;
	if (m_MidAziTime > FMRateRecords[i].azimuthTime)
	  {
	    diffAziTime = DurationType(m_MidAziTime - FMRateRecords[i].azimuthTime);
	  }
	else
	  {
	    diffAziTime = DurationType(FMRateRecords[i].azimuthTime - m_MidAziTime);
	  }
	
	if (diffAziTime < diffAziTimeMin || i == 0)
	  {
	    diffAziTimeMin = diffAziTime;
	    polySelected = i;
	  }
      }

    // Assign m_FM_C0, m_FM_C1, m_FM_C2, m_FM_Tau0
    m_FM_C0 = FMRateRecords[polySelected].azimuthFmRatePolynomial[0];
    m_FM_C1 = FMRateRecords[polySelected].azimuthFmRatePolynomial[1];
    m_FM_C2 = FMRateRecords[polySelected].azimuthFmRatePolynomial[2];
    m_FM_Tau0 = FMRateRecords[polySelected].t0;
   
    return true;
  }

 /**
   * Method selectDCFCoef
   */
  template<class TImage>
  bool 
  SARDopplerCentroidFreqImageFilter< TImage >
  ::selectDCFCoef()
  {
     // Get input
    ImagePointer inputPtr = const_cast< ImageType * >( this->GetInput() );

     // Retrieve all polynomials
    std::vector<otb::DopplerCentroid> DCFRecords;
    
    ImageMetadata inputKWL = inputPtr->GetImageMetadata();
    
    // this->getAllCoefs(inputKWL, DCFRecords);
    DCFRecords = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).dopplerCentroids;

    DurationType diffAziTimeMin;
    unsigned int polySelected = 0;
    
    // Select one polynomial (with m_MidAziTime)
    for (unsigned int i = 0; i < DCFRecords.size(); i++)
      {
	// Difference between midAziTime and aziTime of the current polynomial
	DurationType diffAziTime;
	if (m_MidAziTime > DCFRecords[i].azimuthTime)
	  {
	    diffAziTime = DurationType(m_MidAziTime - DCFRecords[i].azimuthTime);
	  }
	else
	  {
	    diffAziTime = DurationType(DCFRecords[i].azimuthTime - m_MidAziTime);
	  }
	
	if (diffAziTime < diffAziTimeMin || i == 0)
	  {
	    diffAziTimeMin = diffAziTime;
	    polySelected = i;
	  }
      }

    // Assign m_FM_C0, m_FM_C1, m_FM_C2, m_FM_Tau0
    m_DCF_C0 = DCFRecords[polySelected].dopCoef[0];
    m_DCF_C1 = DCFRecords[polySelected].dopCoef[1];
    m_DCF_C2 = DCFRecords[polySelected].dopCoef[2];
    m_DCF_Tau0 = DCFRecords[polySelected].t0;

    return true;
  }

  /**
   * Apply FM Rate coefficients Method
   */
  template<class TImage>
  long double
  SARDopplerCentroidFreqImageFilter<TImage>
  ::applyFMRateCoefs(double index_sample)
  {
    double slant_range_time = m_FirstRangeTime + (index_sample / m_RangeSamplingRate);
    
    return (m_FM_C0 + m_FM_C1*(slant_range_time - m_FM_Tau0) + m_FM_C2*(slant_range_time - m_FM_Tau0)*
	    (slant_range_time - m_FM_Tau0));
  }

  /**
   * Apply doppler centroid Frequency coefficients Method
   */
  template<class TImage>
  long double
  SARDopplerCentroidFreqImageFilter<TImage>
  ::applyDCFCoefs(double index_sample)
  {
    double slant_range_time = m_FirstRangeTime + (index_sample / m_RangeSamplingRate);
    
    return (m_DCF_C0 + m_DCF_C1*(slant_range_time - m_DCF_Tau0) + m_DCF_C2*(slant_range_time - m_DCF_Tau0)*
	    (slant_range_time - m_DCF_Tau0));
  }
  
  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImage>
  void
  SARDopplerCentroidFreqImageFilter< TImage >
  ::GenerateInputRequestedRegion()
  {
    // call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
    
    // Get Output requested region
    ImageRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

     ///////////// For Input image same region  /////////////
    ImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
    inputPtr->SetRequestedRegion(outputRequestedRegion);
  }
  

  /**
   * Method ThreadedGenerateData
   */
  template<class TImage>
  void
  SARDopplerCentroidFreqImageFilter<TImage>
  ::BeforeThreadedGenerateData()
  {
    // Estimates general parameters for the current burst, if m_FirstEstimation == true
    if (m_FirstEstimation)
      {
	m_FirstEstimation = false;

	// Get input
	ImagePointer inputPtr = const_cast< ImageType * >( this->GetInput() );
	// Choose Metadata
	ImageMetadata inputKWL = inputPtr->GetImageMetadata();
	
	// Check version of header/kwl (at least 3)
	// int headerVersion = std::stoi(inputKWL.GetMetadataByKey("header.version"));

	// if (headerVersion < 3)
	//   {
	//     itkExceptionMacro(<<"Header version is inferior to 3. Please Upgrade your geom file");
	//   }

	// Get some metadata
  double aziSteeringRate = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).azimuthSteeringRate;
	// Conversion to radians per seconds
	aziSteeringRate *= (M_PI/180); 

  m_FirstAziTime = inputKWL[MDTime::AcquisitionStartTime];
  m_FirstRangeTime = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).nearRangeTime;

  m_AziTimeInt = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).azimuthTimeInterval.TotalSeconds();
  m_RangeSamplingRate = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).rangeSamplingRate;

  double radarFrequency = inputKWL[MDNum::RadarFrequency];
	
  int nbLineBurst = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).numberOfLinesPerBurst;
  int nbSampleBurst = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).numberOfSamplesPerBurst;
	// Estimation m_Ks
	m_LineAtMidBurst = nbLineBurst/2.;
	m_MidAziTime = m_FirstAziTime + DurationType::Seconds(m_AziTimeInt * m_LineAtMidBurst);
	
	// Try to create a SarSensorModelAdapter
  SarSensorModel* sarSensorModel = new SarSensorModel(inputKWL);

	Point3DType satpos, satvel;
 
	bool lineToSatPosAndVelOk = sarSensorModel->LineToSatPositionAndVelocity(m_LineAtMidBurst, satpos, satvel);

	if (!lineToSatPosAndVelOk)
	  itkExceptionMacro(<<"Failed to estimate satellite position and velocity.");
  
	m_VSatAtMidAziTime = std::sqrt(satvel[0]*satvel[0] + satvel[1]*satvel[1] + satvel[2]*satvel[2]);

	m_Ks = (2*m_VSatAtMidAziTime/C) * radarFrequency * aziSteeringRate;

	DurationType diffTime = m_MidAziTime - m_FirstAziTime;

	m_MidRanTime = m_FirstRangeTime + (nbSampleBurst / (2*m_RangeSamplingRate));

	// Polynomial selection (FM Rate and Doppler Centroid Frequency)
	this->selectFMRateCoef();
	this->selectDCFCoef();

	// Estimate Reference time at first sample
	m_RefTime0 = - (this->applyDCFCoefs(0) / this->applyFMRateCoefs(0));

	m_RefTimeMid =  - (this->applyDCFCoefs(nbSampleBurst / 2) / this->applyFMRateCoefs(nbSampleBurst / 2));
      }
  }
   

  /**
   * Method ThreadedGenerateData
   */
  template<class TImage>
  void
  SARDopplerCentroidFreqImageFilter<TImage>
  ::ThreadedGenerateData(const ImageRegionType & outputRegionForThread,
			 itk::ThreadIdType /*threadId*/)
  {
    // Compute corresponding input region for master and slave cartesian mean
    ImageRegionType inputRegionForThread = outputRegionForThread;

    // Iterator on output
    OutputIterator OutIt(this->GetOutput(), outputRegionForThread);
    OutIt.GoToBegin();

    // Iterator on input 
    InputIterator  InIt(this->GetInput(), inputRegionForThread);
    InIt.GoToBegin();

    // For each line
    while (!OutIt.IsAtEnd() && !InIt.IsAtEnd())
      {
	OutIt.GoToBeginOfLine();
	InIt.GoToBeginOfLine();


	// For each colunm
	while (!OutIt.IsAtEndOfLine() && !InIt.IsAtEndOfLine()) 
	  {
	    // Input index
	    ImageIndexType currentInputIndex = InIt.GetIndex();
	    Point2DType currentInputPoint;
	    this->GetInput()->TransformIndexToPhysicalPoint(currentInputIndex,currentInputPoint);
	    
	    double indL = currentInputPoint[1] - 0.5;
	    double indC = currentInputPoint[0] - 0.5;

	    // Zero Doppler azimuth Time
	    TimeType aziDur = m_FirstAziTime + DurationType::Seconds(m_AziTimeInt*indL);

	    // Reference Time
	    TimeType refDur = m_MidAziTime + (DurationType::Seconds( - (this->applyDCFCoefs(indC) / this->applyFMRateCoefs(indC)) - m_RefTimeMid));
	    // Kt
	    double Kt = (this->applyFMRateCoefs(indC) * m_Ks) / (this->applyFMRateCoefs(indC) - m_Ks);

	    double diffTime = (aziDur - refDur).TotalSeconds();
	    
	    // Shift for Doppler centroid
	    ImagePixelType dopCentroid_shift = static_cast<ImagePixelType>(this->applyDCFCoefs(indC) + 
									   Kt * diffTime);
   
	    //////////// Assign Output ////////////
	    OutIt.Set(dopCentroid_shift);

	    // Increment iterators
	    ++OutIt;
	    ++InIt;
	  } // End colunms (ouput)

	// Next Line
	OutIt.NextLine();
	InIt.NextLine();
      } // End lines (ouput)
  }


} /*namespace otb*/

#endif
