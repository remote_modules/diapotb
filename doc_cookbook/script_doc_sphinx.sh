#!/bin/bash
# Output Path where the wiki for the diapotb will be generated
wiki_output_path=$1
# Decide which mode will be used to generate the wiki (html or markdown)
mode=$2
#check number of arguments
if [[ $# -ne 2 ]]
then
    echo "Illegal number of parameters" >&2
    exit 2
fi

# Path directory of the current script
path_dir=$(dirname $0)
# Generate the source files for the python scripts of the processing chains
sphinx-apidoc -f -d 1 -o $path_dir/rst_tmp/PythonAPI $path_dir/../python_src/

# Generate the wiki repository from the source repository
if  [[ $mode = "html" ]]
then
  sphinx-build -b html $path_dir/rst_tmp/ $wiki_output_path/diapotb_wiki/ -c $path_dir
elif [[ $mode = "md"  ]]
then
  sphinx-build -M markdown $path_dir/rst_tmp/ $wiki_output_path/diapotb_wiki/ -c $path_dir
fi

# Delete the temporary repertory
if [[ -d "${path_dir}/rst_tmp" ]]
then
    rm -r $path_dir/rst_tmp
fi
