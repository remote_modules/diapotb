/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARGroupedByMLImageFilter_h
#define otbSARGroupedByMLImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkSimpleFastMutexLock.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageMetadata.h"

#include <vector>
#include <map>

namespace otb
{
/** \class SARGroupedByMLImageFilter 
 * \brief Creates an interferogram into ml geometry between two SAR images. 
 * 
 * This filter built the ML interferogram between a master and a slave image.
 *
 * The output is a vector image with amplitude, phase coherency and occurrences. 
 * \ingroup DiapOTBModule
 */

  template <typename TImageSAR, typename TImageOut> 
  class ITK_EXPORT SARGroupedByMLImageFilter :
    public itk::ImageToImageFilter<TImageSAR,TImageOut>
{
public:

  // Standard class typedefs
  typedef SARGroupedByMLImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageSAR,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARGroupedByMLImageFilter,ImageToImageFilter);

  /** Typedef to image input type (master and slave reech images) :  otb::Image (Complex)  */
  typedef TImageSAR                                  ImageSARType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageSARType::Pointer             ImageSARPointer;
  typedef typename ImageSARType::ConstPointer        ImageSARConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageSARType::RegionType          ImageSARRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageSARType::PixelType           ImageSARPixelType;
  typedef typename ImageSARType::PointType           ImageSARPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageSARType::IndexType           ImageSARIndexType;
  typedef typename ImageSARType::IndexValueType      ImageSARIndexValueType;
  typedef typename ImageSARType::SizeType            ImageSARSizeType;
  typedef typename ImageSARType::SizeValueType       ImageSARSizeValueType;
  typedef typename ImageSARType::SpacingType         ImageSARSpacingType;
  typedef typename ImageSARType::SpacingValueType    ImageSARSpacingValueType;
  
  /** Typedef to image output type : otb::VectorImage  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageSARType > InputSARIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;

  typedef itk::SimpleFastMutexLock         MutexType;

  // Setter
  itkSetMacro(MLRan, unsigned int);
  itkSetMacro(MLAzi, unsigned int);
  itkSetMacro(MarginRan, unsigned int);
  itkSetMacro(MarginAzi, unsigned int);
  itkSetMacro(Gain, float);

  void bandSelection(std::vector<std::string> bands);

  // Getter
  itkGetMacro(MLRan, unsigned int);
  itkGetMacro(MLAzi, unsigned int);
  itkGetMacro(MarginRan, unsigned int);
  itkGetMacro(MarginAzi, unsigned int);
  itkGetMacro(Gain, float);


protected:
  // Constructor
  SARGroupedByMLImageFilter();

  // Destructor
  virtual ~SARGroupedByMLImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** SARGroupedByMLImageFilter produces an image which are into output geometry. The differences between 
   * output and input images can be the size of images and the dimensions. 
   * As such, SARGroupedByMLImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** SARGroupedByMLImageFilter needs a input requested region that corresponds to the margin and ML factors 
   * into the requested region of our output requested region. 
   * As such, SARGroupedByMLImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input SAR region 
   */
  ImageSARRegionType OutputRegionToInputRegion(const ImageOutRegionType& outputRegion, bool withMargin) const;


  /** 
   * SARGroupedByMLImageFilterr can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread, 
				    itk::ThreadIdType threadId) ITK_OVERRIDE;
  


 private:
  SARGroupedByMLImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 
  
  // ML factor in range 
  unsigned int m_MLRan;
  
  // ML factor in azimut
  unsigned int m_MLAzi;
  
  // gain for amplitude estimation
  float m_Gain;

  // SAR Dimensions
  int m_nbLinesSAR;
  int m_nbColSAR;

  // Margin in order to spend our averaging
  unsigned int m_MarginRan;
  unsigned int m_MarginAzi;

  // Selected bands
  std::map<std::string, bool> m_SelectedBands;
  unsigned int m_nbBands;
   
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARGroupedByMLImageFilter.txx"
#endif



#endif
