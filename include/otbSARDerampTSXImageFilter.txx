/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDerampTSXImageFilter_txx
#define otbSARDerampTSXImageFilter_txx

#include "otbSARDerampTSXImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include "otbSarSensorModel.h"
#include <cmath>
#include <algorithm>

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImage> 
  SARDerampTSXImageFilter< TImage >::SARDerampTSXImageFilter()
  {
    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(1);
    this->SetNumberOfIndexedInputs(2);
  }
    
  /** 
   * Destructor
   */
  template <class TImage> 
  SARDerampTSXImageFilter< TImage >::~SARDerampTSXImageFilter()
  {
   
  }



  template<class TImage>
  void
  SARDerampTSXImageFilter< TImage >
  ::setPolynomeFMRate(ImageMetadata  const& kwl, std::vector<FMRateRecordType> & FMRateRecords)
  {
    char fmRatePrefix_[1024];
    std::size_t firstInd(1);
    std::size_t nbLists(0);
    nbLists = std::stoi(kwl["azimuthFmRate.azi_fm_rate_coef_nb_list"]);

    std::string FM_PREFIX = "azimuthFmRate.azi_fm_rate_coef_list";
    
    //set coefficient for 1st polynome FMRate
    const int pos1 = sprintf(fmRatePrefix_, "%s%zu.", FM_PREFIX.c_str(), firstInd);
    assert(pos1 > 0 && (unsigned long) pos1 < sizeof(fmRatePrefix_));
    const std::string FMPrefix1(fmRatePrefix_, pos1);
    FMRateRecordType fmRateRecord1;
    fmRateRecord1.coef0FMRate = std::stod(kwl[FMPrefix1 + "1.azi_fm_rate_coef"]);
    FMRateRecords.push_back(fmRateRecord1);
    
    //set coefficient for last polynome FMRate
    const int posn = sprintf(fmRatePrefix_, "%s%zu.", FM_PREFIX.c_str(), nbLists);
    assert(posn > 0 && (unsigned long) posn < sizeof(fmRatePrefix_));
    const std::string FMPrefixn(fmRatePrefix_, posn);
    FMRateRecordType fmRateRecordn;
    fmRateRecordn.coef0FMRate = std::stod(kwl[FMPrefixn + "1.azi_fm_rate_coef"]);
    FMRateRecords.push_back(fmRateRecordn);
  }

  template<class TImage>
  void
  SARDerampTSXImageFilter< TImage >
  ::setPolynomeDCF(ImageMetadata  const& kwl, std::vector<DCFRecordType> & DCFRecords)
  {
    char dcfPrefix_[1024];
    std::size_t firstInd(1);
    std::size_t nbLists(0);
    nbLists = std::stoi(kwl["dopplerCentroid.dop_coef_nb_list"]);

    std::string DCF_PREFIX = "dopplerCentroid.dop_coef_list";
    
    //set coefficients for 1st polynome DCF
    const int pos1 = sprintf(dcfPrefix_, "%s%zu.", DCF_PREFIX.c_str(), firstInd);
    assert(pos1 > 0 && (unsigned long) pos1 < sizeof(dcfPrefix_));
    const std::string DCFPrefix1(dcfPrefix_, pos1);
    DCFRecordType dcfRecord1;

    dcfRecord1.azimuthDCFTime =  otb::MetaData::ReadFormattedDate(kwl[DCFPrefix1 + "dop_coef_time"]);
    dcfRecord1.coef0DCF = std::stod(kwl[DCFPrefix1 + "1.dop_coef"]);
    dcfRecord1.coef1DCF = std::stod(kwl[DCFPrefix1 + "2.dop_coef"]);
    dcfRecord1.tau0DCF = std::stod(kwl[DCFPrefix1 + "slant_range_time"]);

    DCFRecords.push_back(dcfRecord1);
    //set coefficients for last polynome DCF
    const int posn = sprintf(dcfPrefix_, "%s%zu.", DCF_PREFIX.c_str(), nbLists);
    assert(posn > 0 && (unsigned long) posn < sizeof(dcfPrefix_));
    const std::string DCFPrefixn(dcfPrefix_, posn);
    DCFRecordType dcfRecordn;

    dcfRecordn.azimuthDCFTime =  otb::MetaData::ReadFormattedDate(kwl[DCFPrefixn + "dop_coef_time"]);
    dcfRecordn.coef0DCF = std::stod(kwl[DCFPrefixn + "1.dop_coef"]);
    dcfRecordn.coef1DCF = std::stod(kwl[DCFPrefixn + "2.dop_coef"]);
    dcfRecordn.tau0DCF = std::stod(kwl[DCFPrefixn + "slant_range_time"]);
    
    DCFRecords.push_back(dcfRecordn);
     
  }

   /**
   * Get FM
   */
  template<class TImage>
  double
  SARDerampTSXImageFilter<TImage>
  ::getFM()
  {
    double FM(0);

    FM = (this->FMRatePolynoms.front().coef0FMRate + this->FMRatePolynoms.back().coef0FMRate)/2.;

    return FM;

  }

  
   /**
   * Get AzTime for line indL
   */
  template<class TImage>
  typename SARDerampTSXImageFilter<TImage>::TimeType
  SARDerampTSXImageFilter<TImage>
  ::getAzTimeL(double indL)
  {
    TimeType azTime(0);

    azTime = this->m_FirstAziTime + DurationType::Seconds(indL / this->m_PRF);

    return azTime;

  }

  
   /**
   * Get polynome DCF for index column indC
   */
  template<class TImage>
  double
  SARDerampTSXImageFilter<TImage>
  ::getFDC(double indC, char index)
  {
    double fdc(0);

    double slant_range_time = this->m_FirstRangeTime + (indC / this->m_RangeSamplingRate);
    
    if(index == '1'){
      fdc = this->DCFPolynoms.front().coef0DCF + this->DCFPolynoms.front().coef1DCF*(slant_range_time - this->DCFPolynoms.front().tau0DCF);
    }
    else if(index == 'N'){
      fdc = this->DCFPolynoms.back().coef0DCF + this->DCFPolynoms.back().coef1DCF*(slant_range_time - this->DCFPolynoms.back().tau0DCF);
    }
    

    return fdc;

  }


  
   /**
   * Get tdc for index column indC
   */
  template<class TImage>
  typename SARDerampTSXImageFilter<TImage>::TimeType
  SARDerampTSXImageFilter<TImage>
  ::getTDC(double fdc, char index)
  {
    TimeType tdc;

    
    if(index == '1'){
      tdc = this->DCFPolynoms.front().azimuthDCFTime - DurationType::Seconds(fdc/m_FM);
    }
    else if(index == 'N'){
      tdc = this->DCFPolynoms.back().azimuthDCFTime - DurationType::Seconds(fdc/m_FM);
    }
    

    return tdc;

  }

  /**
   * Get Phi
   */
  template<class TImage>
  double
  SARDerampTSXImageFilter<TImage>
  ::getPhi(double indL, double indC)
  {
    // Difference in azimuth time
    double azi_diff = indL/this->m_PRF;
    // FDC
    double fdc1 = this->getFDC(indC,'1');
    double fdcn = this->getFDC(indC,'N');
    // TDC
    TimeType tdc1 = this->getTDC(fdc1,'1');
    TimeType tdcn = this->getTDC(fdcn,'N');
    // FDC Derive
    DurationType diff_tdc = DurationType(tdcn - tdc1);
    double fdc_derive = (fdcn - fdc1)/diff_tdc.TotalSeconds();
    // Phi
    double Phi = - M_PI * fdc_derive * (azi_diff)*(azi_diff);

    return Phi;

  }


  /**
  * Method Initialize attributes for BeforeGenerateData()
   */
  template<class TImage>
  void 
  SARDerampTSXImageFilter< TImage >
  ::ThreadInit(ImageMetadata inputKWL)
  {
  std::cout << "ThreadInit" << std::endl;
  this->m_FirstAziTime = inputKWL[MDTime::AcquisitionStartTime];
	this->m_FirstRangeTime = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).nearRangeTime;
	this->m_RangeSamplingRate = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).rangeSamplingRate;
	
	this->setPolynomeFMRate(inputKWL,this->FMRatePolynoms);
	this->setPolynomeDCF(inputKWL,this->DCFPolynoms);
	// Do not get PRF directly for TSX (confusion in TSX metadata between RSF and PRF)
	// Use 1/line_time_interval, instead
	this->m_PRF = 1/boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).azimuthTimeInterval.TotalSeconds();
	this->m_FM = this->getFM();


  }
  

} /*namespace otb*/

#endif
