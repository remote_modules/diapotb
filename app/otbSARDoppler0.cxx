/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARStreamingCorrelationImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>
#include <complex>
#include <cmath>

namespace otb
{
namespace Wrapper
{

class SARDoppler0 : public Application
{
public:
  typedef SARDoppler0 Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARDoppler0, otb::Wrapper::Application);
 
  // Filter
  typedef otb::SARStreamingCorrelationImageFilter<ComplexDoubleImageType> CorrelationType;
  
  

private:
  void DoInit() override
  {
    SetName("SARDoppler0");
    SetDescription("SAR Doppler 0 calculation.");

    SetDocLongDescription("This application estimates the value of Doppler 0 "
      "for a SAR image. The algorithm is based on the CDE method described "
      "into [Estimating the Doppler centroid of SAR data].");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "insar",   "Input image");
    SetParameterDescription("insar", "Image to perform computation on.");

    AddParameter(ParameterType_OutputFilename, "outfile", "Output file to store Doppler 0 value");
    SetParameterDescription("outfile","Output file to store Doppler 0 value.");
    MandatoryOff("outfile");

    // Parameter Output
    AddParameter(ParameterType_Float,"doppler0","doppler0 value (output of this application)");
    SetParameterDescription("doppler0", "Doppler0 value.");
    SetParameterRole("doppler0", Role_Output);
    SetDefaultParameterFloat("doppler0", 0);
    
    AddRAMParameter();

    SetDocExampleParameterValue("insar","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002.tiff");
    SetDocExampleParameterValue("outfile","doppler_file.txt");

  }

  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {  
    
    // Create our StreamingCorrelationImageFilter in order to launch our persistent filter 
    // and get the wanted values : correlation coefficient, sum of finite pixel, nb of finite pixel  
    CorrelationType::Pointer corFilter = CorrelationType::New();


    // Define pipeline 
    corFilter->SetInput(GetParameterComplexDoubleImage("insar"));

     // Adapt streaming with ram parameter (default 256 MB)
    corFilter->GetStreamer()->SetAutomaticStrippedStreaming(GetParameterInt("ram"));

    // Execute pipeline
    corFilter->Update();

    // Add Log information
    otbAppLogINFO(<<"Sum of finite pixel = "<<corFilter->GetSum());
    otbAppLogINFO(<<"Correlation coefficient = "<<corFilter->GetCorrelation());
    otbAppLogINFO(<<"Nb of finite pixel = "<<corFilter->GetCount());
    

    // Retrieve Doppler0 value
    double doppler0 = corFilter->GetDoppler();
    
    // if outfile parameter, fill the file with doppler 0 value
    std::string outFile = GetParameterString("outfile");

    if (!outFile.empty())
      {
	std::ofstream fichierDoppler(outFile, std::ios::app);
	    
	if (fichierDoppler)
	  {
	    fichierDoppler << "AZIMUTH DOPPLER NUMBER OF COLUMN : " << 1 << std::endl;
	    fichierDoppler << "AZIMUTH DOPPLER NUMBER OF LINE : " << 1 << std::endl;
	    fichierDoppler << std::setprecision(11);
	    fichierDoppler << "AZIMUTH DOPPLER VALUE : " << doppler0 << std::endl;
	    fichierDoppler.close();
	  }
      }

    otbAppLogINFO(<<"Doppler 0 = "<<doppler0);

    // Assigne Doppler0
    SetParameterFloat("doppler0", doppler0);

  }
  
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARDoppler0)
