/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdlib>
#include "itkMacro.h"
#include "otbImage.h"
#include "otbVectorImage.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbSARDopplerCentroidFreqImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"

int otbSARDopplerCentroidFreqFilterTest(int argc, char * argv[])
{
  // Verify the number of parameters in the command line
  if (argc != 3)
    {
      std::cerr << "Usage: " << std::endl;
      std::cerr << argv[0] << " inputImageFile outputImageFile" << std::endl;
      return EXIT_FAILURE;
    }
  
  // Image type
  const unsigned int Dimension = 2;
  typedef otb::Image <float, Dimension>              FloatImageType;
  typedef otb::VectorImage<float>                    FloatVectorImageType;

  // Reader
  typedef otb::ImageFileReader<FloatVectorImageType> ReaderType;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(argv[1]);
  reader->UpdateOutputInformation();

  // Filter Extract
  typedef otb::MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
					    FloatImageType::PixelType>                ExtractROIFilterType;
  ExtractROIFilterType::Pointer extractor = ExtractROIFilterType::New();
  
  unsigned long startL = 0;
  unsigned long startS = 0;
  unsigned long sizeL = reader->GetOutput()->GetLargestPossibleRegion().GetSize()[1];
  unsigned long sizeS = reader->GetOutput()->GetLargestPossibleRegion().GetSize()[0];
 

  // Filter
  typedef otb::SARDopplerCentroidFreqImageFilter<FloatImageType> FilterType;
  FilterType::Pointer filter = FilterType::New();
  

  std::cout<<"Print filter information: "<< std::endl;
  std::cout << filter << std::endl;

  // Writer
  typedef otb::ImageFileWriter<FloatImageType> WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName(argv[2]);

  // Define pipeline
  extractor->SetInput(reader->GetOutput());
  extractor->SetChannel(1);      
  extractor->SetStartX(startS);
  extractor->SetStartY(startL);
  extractor->SetSizeX(sizeS);
  extractor->SetSizeY(sizeL);

  filter->SetInput(extractor->GetOutput());

  writer->SetInput(filter->GetOutput());

  // Execute pipeline
  try
    {
      writer->Update();
    }
  catch (itk::ExceptionObject& err)
    {
      std::cerr << "ExceptionObject caught !" << std::endl;
      std::cerr << err << std::endl;
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

