/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDopplerCentroidFreqImageFilter_h
#define otbSARDopplerCentroidFreqImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageMetadata.h"
#include "otbSarSensorModel.h"

#include "otbDateTime.h"

namespace otb
{
/** \class SARDopplerCentroidFreqImageFilter 
 * \brief DopplerCentroidFreq to dermine the doppler Centroid of a whole burst from Sentinel-1 IW product. 
 * 
 * This filter estimates doppler xentroid of an input burst.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImage> 
  class ITK_EXPORT SARDopplerCentroidFreqImageFilter :
    public itk::ImageToImageFilter<TImage,TImage>
{
public:

  // Standard class typedefs
  typedef SARDopplerCentroidFreqImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImage,TImage>         Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARDopplerCentroidFreqImageFilter,ImageToImageFilter);

  /** Typedef to image input/output type VectorImage (Complex Image)   */
  typedef TImage                                  ImageType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageType::Pointer             ImagePointer;
  typedef typename ImageType::ConstPointer        ImageConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageType::RegionType          ImageRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageType::PixelType           ImagePixelType;
  typedef typename ImageType::PointType           ImagePointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageType::IndexType           ImageIndexType;
  typedef typename ImageType::IndexValueType      ImageIndexValueType;
  typedef typename ImageType::SizeType            ImageSizeType;
  typedef typename ImageType::SizeValueType       ImageSizeValueType;
  typedef typename ImageType::SpacingType         ImageSpacingType;
  typedef typename ImageType::SpacingValueType    ImageSpacingValueType;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageType > OutputIterator;

  using TimeType = otb::MetaData::TimePoint;
  using DurationType = otb::MetaData::Duration;

   // Struture declaration
   struct FMRateRecordType
   {
  TimeType      azimuthFMRateTime;
  double        coef0FMRate;
  double        coef1FMRate;
  double        coef2FMRate;
  double        tau0FMRate;
  friend std::ostream & operator<<(std::ostream & os, const FMRateRecordType & v)
  {
  return os << "{ azimuthFMRateTime: " << v.azimuthFMRateTime
	    <<        ", coefficient 0: "  << v.coef0FMRate
	    <<        ", coefficient 1: "        << v.coef1FMRate
	    <<        ",coefficient 2: "         << v.coef2FMRate
	    <<        ",slant range time (tau 0): "         << v.tau0FMRate
	    <<        "}";
}
};

   struct DCFRecordType
   {
  TimeType      azimuthDCFTime;
  double        coef0DCF;
  double        coef1DCF;
  double        coef2DCF;
  double        tau0DCF;
  friend std::ostream & operator<<(std::ostream & os, const DCFRecordType & v)
  {
  return os << "{ azimuthDCFTime: " << v.azimuthDCFTime
	    <<        ", coefficient 0: "  << v.coef0DCF
	    <<        ", coefficient 1: "        << v.coef1DCF
	    <<        ",coefficient 2: "         << v.coef2DCF
	    <<        ",slant range time (tau 0): "         << v.tau0DCF
	    <<        "}";
}
};

protected:
  // Constructor
  SARDopplerCentroidFreqImageFilter();

  // Destructor
  virtual ~SARDopplerCentroidFreqImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** 
   * SARDopplerCentroidFreqImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageRegionType& outputRegionForThread, 
				    itk::ThreadIdType threadId) ITK_OVERRIDE;

  
  /** SARDopplerCentroidFreqImageFilter needs a input requested region that corresponds to our output 
   * requested region. 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /** 
   * SARDopplerCentroidFreqImageFilter reuses calculations into ThreadedGeneratedData. 
   * The aim is to estimate once and for all, some calculations and store results into argument of this class.
   *
   * \sa ImageToImageFilter::BeforeThreadedGenerateData()*/
  void BeforeThreadedGenerateData() ITK_OVERRIDE;
  

  void getAllCoefs(ImageMetadata  const& kwl, std::vector<FMRateRecordType> & FMRateRecords);
  void getAllCoefs(ImageMetadata  const& kwl, std::vector<DCFRecordType> & DCFRecords);

  long double applyFMRateCoefs(double index_sample);
  long double applyDCFCoefs(double index_sample);
   
 private:
  SARDopplerCentroidFreqImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 

  // Polynomial Selection
  bool selectFMRateCoef();
  bool selectDCFCoef();

  // First azimuth/range time
  TimeType m_FirstAziTime;
  double m_FirstRangeTime;

  // Mid burst zero Doppler azimuth time [s] 
  TimeType m_MidAziTime;

  double m_MidRanTime;
  
  // Spacecraft velocity computed at mid-burst time (m_MidAziTime) [m/s]
  double m_VSatAtMidAziTime;

  // Doppler Centroid rate introduced by the scanning og the antenna [Hz/s]
  double m_Ks;
  
  // Azimuth time interval [s]
  double m_AziTimeInt;

  // Range sampling rate [Hz]
  double m_RangeSamplingRate;

  // Parameters coefficients for Doppler FM Rate
  double m_FM_C0, m_FM_C1, m_FM_C2, m_FM_Tau0;

  // Parameters coefficients for Doppler Centroid Frequency
  double m_DCF_C0, m_DCF_C1, m_DCF_C2, m_DCF_Tau0;

  // Reference time 0 [s]
  long double m_RefTime0;

  long double m_RefTimeMid;

  // Mid Line Burst
  double m_LineAtMidBurst;

  // First estimation
  bool m_FirstEstimation;

  const double C = 299792458;

};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARDopplerCentroidFreqImageFilter.txx"
#endif



#endif
