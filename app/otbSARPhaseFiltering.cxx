/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbTilesAnalysisWithStepImageFilter.h"
#include "otbTilesExampleFunctor.h"
#include "otbSARTilesPhaseFilteringFunctor.h"
#include "otbSARCompensatedComplexFromInterferogramImageFilter.h"

#include "otbSARGroupedByMLImageFilter.h"

#include "otbWrapperOutputImageParameter.h"
#include "otbWrapperTypes.h"

#include "otbRealAndImaginaryImageToComplexImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"

#include "otbImageMetadata.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SARPhaseFiltering : public Application
    {
    public:
      typedef SARPhaseFiltering Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SARPhaseFiltering, otb::Wrapper::Application);

      // Function
      typedef otb::Function::SARTilesPhaseFilteringFunctor<ComplexFloatImageType, ComplexFloatImageType> TilesPhaseFilteringFunctorType;

      // Filters
      typedef otb::SARCompensatedComplexFromInterferogramImageFilter<FloatVectorImageType, 
								     FloatVectorImageType> CompensatedComplexFromInterferogramFilterType;
      typedef otb::TilesAnalysisWithStepImageFilter<ComplexFloatImageType, ComplexFloatImageType, TilesPhaseFilteringFunctorType> TilesFilterType;
 
      typedef otb::SARGroupedByMLImageFilter<ComplexFloatImageType, FloatVectorImageType> GroupedByMLFilterType;

     typedef otb::RealAndImaginaryImageToComplexImageFilter<FloatImageType, FloatImageType, ComplexFloatImageType> RealImagToComplexFilterType;
      
     typedef otb::MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType, FloatImageType::PixelType> ExtractROIFilterType;

    private:
      void DoInit() override
      {
	SetName("SARPhaseFiltering");
	SetDescription("Phase filtering for input interferogram.");

	SetDocLongDescription("This application does the filtering for an input interferogram to" 
        " to obtain a denoising phase.");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "incomplex",   "Input Compensated complex image");
	SetParameterDescription("incomplex", "Input Compensated complex image.");
	  
	AddParameter(ParameterType_InputImage,  "ininterf",   "Input Interferogram");
	SetParameterDescription("ininterf", "Input Interferogram.");
        
	AddParameter(ParameterType_Int,  "sizetiles", "Size Tiles for output cut");
	SetParameterDescription("sizetiles", "Size Tiles for output cut.");
	SetDefaultParameterInt("sizetiles", 64);
	MandatoryOff("sizetiles");

	AddParameter(ParameterType_Int,  "step", "Step for window extractions into input interferogram (complex conjugate image)");
	SetParameterDescription("step", "Step for window extractions.");
	SetDefaultParameterInt("step", 16);
	MandatoryOff("step");

	AddParameter(ParameterType_Int,  "mlran", "Averaging on distance for output interferogram");
	SetParameterDescription("mlran", "Averaging on distance for output interferogram.");
	SetDefaultParameterInt("mlran", 1);
	SetMinimumParameterIntValue("mlran", 1);
	MandatoryOff("mlran");

	AddParameter(ParameterType_Int,  "mlazi", "Averaging on azimut for output interferogram");
	SetParameterDescription("mlazi", "Averaging on azimut for output interferogram.");
	SetDefaultParameterInt("mlazi", 1);
	SetMinimumParameterIntValue("mlazi", 1);
	MandatoryOff("mlazi");

	AddParameter(ParameterType_Float, "gain", "Gain to apply on output amplitude");
	SetParameterDescription("gain","Gain to apply on output amplitude");
	SetDefaultParameterFloat("gain", 0.1);
	SetMinimumParameterFloatValue("gain", 0);
	MandatoryOff("gain");

	AddParameter(ParameterType_Float, "alpha", "alpha parameter for Goldstein filter");
	SetParameterDescription("alpha","alpha parameter for Goldstein filter.");
	SetDefaultParameterFloat("alpha", 0.7);
	SetMinimumParameterFloatValue("alpha", 0.0001);
	SetMaximumParameterFloatValue("alpha", 1);
	MandatoryOff("alpha");

	AddParameter(ParameterType_OutputImage, "out", "Output interferogram (after filtering)");
	SetParameterDescription("out","Output Image : Interferogram after filtering with 3 bands (amp, pha, coh).");
	
	AddRAMParameter();

	SetDocExampleParameterValue("incomplex","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002.tiff");
	SetDocExampleParameterValue("out","s1b-s4-coregistrated.tiff");
      }

      void DoUpdateParameters() override
      {
	// Handle the dependency between incomplex and ininterf (two kinds of pipelines)
	if (GetParameterByKey("incomplex")->HasValue())
	  {
	    DisableParameter("ininterf");
	    MandatoryOff("ininterf");

	    otbAppLogINFO(<<"Complex input");
	  }
	else
	  {
	    EnableParameter("ininterf");
	    MandatoryOn("ininterf");

	    MandatoryOff("incomplex");

	    otbAppLogINFO(<<"Interferogram input");
	  }
      }

      void DoExecute() override
      { 
	// Get numeric parameters : sizeTiles, step and alpha
	float alpha  = GetParameterFloat("alpha");
	int step = GetParameterInt("step");
	int sizeTiles = GetParameterInt("sizetiles");
	int factor_azi = GetParameterInt("mlazi");
	int factor_ran = GetParameterInt("mlran");
	float gain = GetParameterFloat("gain");

	otbAppLogINFO(<<"Alpha for Goldstein filter : "<<alpha);
	otbAppLogINFO(<<"Step for window extraction (for each step pixel) : "<<step);
	otbAppLogINFO(<<"Size of tiles  : "<<sizeTiles);
	otbAppLogINFO(<<"Averaging Factor on azimut for final interferogram :"<<factor_azi);
	otbAppLogINFO(<<"Averaging Factor on range for final interferogram : "<<factor_ran);
	otbAppLogINFO(<<"Gain to apply on output amplitude : "<<gain);
	
	// Complex Ptr
	ComplexFloatImageType::Pointer inComplexPtr;

	// Two kinds of Pipeline : one with complex image and one with interferogram
	if (GetParameterByKey("incomplex")->HasValue())
	  {
	    inComplexPtr = GetParameterComplexFloatImage("incomplex");
	  }
	else
	  {
	    FloatVectorImageType::Pointer inInterfPtr = GetParameterFloatVectorImage("ininterf");

	    // Check ML factors (must be 1x1) thanks to the Metadata
	    otb::ImageMetadata MD = inInterfPtr->GetImageMetadata();
	    int mlran_interf = 1;
	    int mlazi_interf = 1;
	    if (MD.Has("ml_ran"))
	      {
		mlran_interf = std::atoi(MD["ml_ran"].c_str());
	      }
	    if (MD.Has("ml_azi"))
	      {
		mlazi_interf = std::atoi(MD["ml_azi"].c_str());
	      }
	    
	    if (mlran_interf != 1 || mlazi_interf != 1)
	      {
		// Send an exception
		itkExceptionMacro(<<"Input interferogram must be into master geometry (ML : 1x1)");
	      }
	    

	    // Estimate ComplexCompensated image (VectorImage with : real, imag and mod as bands)
	    CompensatedComplexFromInterferogramFilterType::Pointer compensatedComplexFromInterf = 
	      CompensatedComplexFromInterferogramFilterType::New();
	    m_Ref.push_back(compensatedComplexFromInterf.GetPointer());
	    compensatedComplexFromInterf->SetInput(inInterfPtr);

	    // Transform VectorImage To Complex Type
	    // Extract the first and second bands
	    ExtractROIFilterType::Pointer extractRealFilter = ExtractROIFilterType::New();
	    m_Ref.push_back(extractRealFilter.GetPointer());
	    ExtractROIFilterType::Pointer extractImagFilter = ExtractROIFilterType::New();
	    m_Ref.push_back(extractImagFilter.GetPointer());
	    
	    // Real part
	    extractRealFilter->SetInput(compensatedComplexFromInterf->GetOutput());
	    extractRealFilter->SetChannel(1);
	    extractRealFilter->GetOutput()->UpdateOutputInformation();
	    // Imag part
	    extractImagFilter->SetInput(compensatedComplexFromInterf->GetOutput());
	    extractImagFilter->SetChannel(2);
	    extractImagFilter->GetOutput()->UpdateOutputInformation();

	    // RealAndImagToComplex filter
	    RealImagToComplexFilterType::Pointer realImagToComplexFilter = 
	      RealImagToComplexFilterType::New();
	    m_Ref.push_back(realImagToComplexFilter.GetPointer());
	
	    realImagToComplexFilter->SetInputRealPart(extractRealFilter->GetOutput());
	    realImagToComplexFilter->SetInputImaginaryPart(extractImagFilter->GetOutput());
	    
	    
	    inComplexPtr = realImagToComplexFilter->GetOutput();
	    inComplexPtr->UpdateOutputInformation();
	  }


	///////////////////////////////////// Phase Filter ////////////////////////////////////////
	// Function Type : TilesPhaseFiltering
	TilesPhaseFilteringFunctorType::Pointer functor =  TilesPhaseFilteringFunctorType::New(); 
	functor->SetSizeTiles(sizeTiles);
	functor->Setalpha(alpha);

	// Instanciate the Tiles Analysis (with Step) filter
	// Limitation : Works only with a streaming by strips
	TilesFilterType::Pointer filterPhase = TilesFilterType::New();
	m_Ref.push_back(filterPhase.GetPointer());
	filterPhase->SetSizeTiles(sizeTiles);
	filterPhase->SetStep(step);
	
	filterPhase->SetFunctorPtr(functor);

      
	// Define the main pipeline (controlled with extended FileName in order to obtain 
	// better performances)
	//std::string origin_FileName = GetParameterString("out"); // Doesn't work with ComplexOutputImage
	Parameter* param = GetParameterByKey("out");
	OutputImageParameter* paramDown = dynamic_cast<OutputImageParameter*>(param);
	std::string origin_FileName = paramDown->GetFileName();

	// Check if FileName is extended (with the ? caracter)
	// If not extended then override the FileName
	if (origin_FileName.find("?") == std::string::npos && !origin_FileName.empty()) 
	  {
	    std::string extendedFileName = origin_FileName;

	    // Get the ram value (in MB)
	    int ram = GetParameterInt("ram");
	
	    // Define with the ram value, the number of lines for the streaming
	    int nbColSAR = inComplexPtr->GetLargestPossibleRegion().GetSize()[0];
	    int nbLinesSAR = inComplexPtr->GetLargestPossibleRegion().GetSize()[1];
	    // To determine the number of lines : 
	    // nbColSAR * nbLinesStreaming * sizeof(OutputPixel) = RamValue/2 (/2 to be sure)
	    // OutputPixel are float => sizeof(OutputPixel) = 4 (Bytes)
	    long int ram_In_KBytes = (ram/2) * 1024;
	    long int nbLinesStreaming = (ram_In_KBytes / (nbColSAR)) * (1024/4);
	
	    // Check the value of nbLinesStreaming
	    int nbLinesStreamingMax = 2000;
	    if (nbLinesStreamingMax > nbLinesSAR)
	      {
		nbLinesStreamingMax = nbLinesSAR;
	      }
	    if (nbLinesStreaming <= 0 || nbLinesStreaming >  nbLinesStreamingMax)
	      {
		nbLinesStreaming =  nbLinesStreamingMax;
	      } 
	
	    // Construct the extendedPart
	    std::ostringstream os;
	    os << "?&streaming:type=stripped&streaming:sizevalue=" << nbLinesStreaming;
	
	    // Add the extendedPart
	    std::string extendedPart = os.str();
	    extendedFileName.append(extendedPart);
	
	    // Set the new FileName with extended options
	    SetParameterString("out", extendedFileName);
	  }

	// Execute the main Pipeline
	filterPhase->SetImageInput(inComplexPtr);

	// Create or Re-create the interferogram (with 2 selected components : phase and coh).
	// Only Phase and Coherence are consistents => May be extract these bands into separate 
	// images (?)
	GroupedByMLFilterType::Pointer filterGroupedBy = GroupedByMLFilterType::New();
	m_Ref.push_back(filterGroupedBy.GetPointer());
	
	// Only for phase and coherence
	std::vector<std::string> bands;
	bands.push_back("pha");
	bands.push_back("coh");
	filterGroupedBy->bandSelection(bands);

	filterGroupedBy->SetInput(filterPhase->GetOutput());
	filterGroupedBy->SetMLRan(factor_ran);
	filterGroupedBy->SetMLAzi(factor_azi);
	filterGroupedBy->SetGain(gain);
	filterGroupedBy->SetMarginRan(1);
	filterGroupedBy->SetMarginAzi(1);


	// Output
	SetParameterOutputImage("out", filterGroupedBy->GetOutput());
      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;

    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SARPhaseFiltering)
