/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingDEMClosestHgtFilter_h
#define otbSARStreamingDEMClosestHgtFilter_h

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "itkPoint.h"

#include "otbPersistentFilterStreamingDecorator.h"

#include <complex>
#include <cmath>
#include <vector>
#include <map>

namespace otb
{

/** \class PersistentDEMClosestHgtFilter
 * \brief Retrive some pixels from the input DEM
 *
 *  This filter persists its temporary data. It means that if you Update it n times on n different
 * requested regions, the output statistics will be the statitics of the whole set of n regions.
 *
 * To reset the temporary data, one should call the Reset() function.
 *
 * To get the wanted values once the regions have been processed via the pipeline, use the Synthetize() method.
 *
 * \ingroup DiapOTBModule
 */
template<class TInputImage>
class ITK_EXPORT PersistentDEMClosestHgtFilter :
  public PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentDEMClosestHgtFilter                 Self;
  typedef PersistentImageFilter<TInputImage, TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                         Pointer;
  typedef itk::SmartPointer<const Self>                   ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentDEMClosestHgtFilter, PersistentImageFilter);

  /** Image related typedefs. */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType RegionType;
  typedef typename TInputImage::SizeType   SizeType;
  typedef typename TInputImage::IndexType  IndexType;
  typedef typename TInputImage::PixelType  PixelType;
  typedef typename ImageType::IndexValueType IndexValueType;
  typedef typename ImageType::SizeValueType  SizeValueType;

  using Point2DType = itk::Point<double,2>;

  itkStaticConstMacro(InputImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Type to use for computations. */
  //  typedef typename itk::NumericTraits<PixelType>::RealType RealType;
  typedef typename itk::NumericTraits<double>::RealType RealType;
 
  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;

  typedef itk::SimpleDataObjectDecorator<std::vector<PixelType> *>  PixelVectorObjectType;
  typedef itk::SimpleDataObjectDecorator<std::vector<int> *>  IntVectorObjectType;
  typedef itk::SimpleDataObjectDecorator<std::map<int, int> *>  IntMapObjectType;

  /** Return the vectors of selected Hgt. */
  std::vector<PixelType> * GetVectorHgt() const
  {
    return this->GetVectorHgt_Output()->Get();
  }
  PixelVectorObjectType* GetVectorHgt_Output();
  const PixelVectorObjectType* GetVectorHgt_Output() const;

  
  /** Return the vectors of selected Id. */
  std::vector<int> * GetVectorId() const
  {
    return this->GetVectorId_Output()->Get();
  }
  IntVectorObjectType* GetVectorId_Output();
  const IntVectorObjectType* GetVectorId_Output() const;
  
 
  /** Return the map to handle possible duplications. */
  std::map<int, int> * GetMapDuplicates() const
  {
    return this->GetMapDuplicates_Output()->Get();
  }
  IntMapObjectType* GetMapDuplicates_Output();
  const IntMapObjectType* GetMapDuplicates_Output() const;

  // Setter for input vecotr of Lon/lat
  void SetVectorLonLat(std::vector<double *> * vectorLonLat);

  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) ITK_OVERRIDE;
  using Superclass::MakeOutput;

  /**
   * Synthetize and Reset function called by our PersistentFilterStreamingDecorator
   */
  void Synthetize(void) ITK_OVERRIDE;
  void Reset(void) ITK_OVERRIDE;

  itkSetMacro(UserIgnoredValue, RealType);
  itkGetMacro(UserIgnoredValue, RealType);

protected:
  PersistentDEMClosestHgtFilter();
  ~PersistentDEMClosestHgtFilter() ITK_OVERRIDE {}
  void PrintSelf(std::ostream& os, itk::Indent indent) const ITK_OVERRIDE;

  
  /** PersistentDEMClosestHgtFilter needs a input requested region with the same size of
      the output requested region.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType&
                             outputRegionForThread,
                             itk::ThreadIdType threadId) ITK_OVERRIDE;

  
  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() ITK_OVERRIDE;
  void GenerateOutputInformation() ITK_OVERRIDE;

private:
  PersistentDEMClosestHgtFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  std::vector<std::vector<PixelType> * >       m_Thread_Hgt;
  std::vector<std::vector<int> * >             m_Thread_Id;

  /* Ignored values */
  RealType                   m_UserIgnoredValue;

  std::vector<double *> *    m_VectorLonLat;
  
  std::vector<IndexType> *   m_indexCL;

}; // end of class PersistentDEMClosestHgtFilter

/*===========================================================================*/

/** \class SARStreamingDEMClosestHgtFilter
 * \brief This class streams the whole input image through the PersistentDEMClosestHgtFilter.
 *
 * This way, it allows computing the first order global statistics of this image. It calls the
 * Reset() method of the PersistentDEMClosestHgtFilter before streaming the image and the
 * Synthetize() method of the PersistentDEMClosestHgtFilter after having streamed the image
 * to compute the statistics. The accessor on the results are wrapping the accessors of the
 * internal PersistentDEMClosestHgtFilter.
 * By default infinite values are ignored, use IgnoreInfiniteValues accessor to consider
 * infinite values in the computation.
 *
 * This filter can be used as:
 * \code
 * typedef otb::StreamingDEMClosestHgtFilter<ImageType> StatisticsType;
 * StatisticsType::Pointer statistics = StatisticsType::New();
 * statistics->SetInput(DEM);
 * statistics->SetVectorLonLat(vectorLonLat);
 * statistics->Update();
 * statistics->GetDEMHgt(vectorClosestHgt);
 * \endcode
 *
 * \sa PersistentDEMClosestHgtFilter
 * \sa PersistentImageFilter
 * \sa PersistentFilterStreamingDecorator
 * \sa StreamingImageVirtualWriter
 * \ingroup Streamed
 * \ingroup Multithreaded
 * \ingroup MathematicalStatisticsImageFilters
 *
 * \ingroup DiapOTBModule
 */

template<class TInputImage>
class ITK_EXPORT SARStreamingDEMClosestHgtFilter :
  public PersistentFilterStreamingDecorator<PersistentDEMClosestHgtFilter<TInputImage> >
{
public:
  /** Standard Self typedef */
  typedef SARStreamingDEMClosestHgtFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersistentDEMClosestHgtFilter<TInputImage> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingDEMClosestHgtFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::PixelType PixelType;
  typedef typename StatFilterType::RealType  RealType;
  typedef TInputImage                        InputImageType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<int>  IntObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;

  typedef itk::SimpleDataObjectDecorator<std::vector<PixelType> *>  PixelVectorObjectType;
  typedef itk::SimpleDataObjectDecorator<std::vector<int> *>  IntVectorObjectType;
  typedef itk::SimpleDataObjectDecorator<std::map<int, int> *>  IntMapObjectType;

  /** Image related typedefs. */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType RegionType;
  typedef typename TInputImage::SizeType   SizeType;
  typedef typename TInputImage::IndexType  IndexType;

   // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  using Superclass::SetInput;

  // Setter/Getter for the Persistent Filter
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  // Set vectors
  void SetVectorLonLat(std::vector<double *> * vectorLonLat)
  {
    this->GetFilter()->SetVectorLonLat(vectorLonLat);
  }
  

  /** Return vectors. */
  std::vector<PixelType> * GetVectorHgt() const
  {
    return this->GetFilter()->GetVectorHgt_Output()->Get();
  }
  PixelVectorObjectType* GetVectorVectorHgt_Output()
  {
    return this->GetFilter()->GetVectorHgt_Output();
  }
  const PixelVectorObjectType* GetVectorHgt_Output() const
  {
    return this->GetFilter()->GetVectorHgt_Output();
  }

  std::vector<int> * GetVectorId() const
  {
    return this->GetFilter()->GetVectorId_Output()->Get();
  }
  IntVectorObjectType* GetVectorId_Output()
  {
    return this->GetFilter()->GetVectorId_Output();
  }
  const IntVectorObjectType* GetVectorId_Output() const
  {
    return this->GetFilter()->GetVectorId_Output();
  }

  std::map<int, int> * GetMapDuplicates() const
  {
    return this->GetFilter()->GetMapDuplicates_Output()->Get();
  }
  IntMapObjectType* GetMapDuplicates_Output()
  {
    return this->GetFilter()->GetMapDuplicates_Output();
  }
  const IntMapObjectType* GetMapDuplicates_Output() const
  {
    return this->GetFilter()->GetMapDuplicates_Output();
  }

  
  // Get required hgt
  int GetDEMHgt(std::vector<double> * closestHgt)
  {
    // Retrieve selected hgts 
    std::vector<PixelType> * hgts = this->GetVectorHgt();

    // Get Id to resort the required hgts
    std::vector<int> * id = this->GetVectorId();
    
    // Get Map to idnetify duplications
    std::map<int, int> * duplicates = this->GetMapDuplicates();
    std::map<int, int>::iterator itMap;

    // Check sizes 
    if (closestHgt->size() < hgts->size()) 
      {
	// Wrong size => return 1
	return 1;
      }


    // Copy the selected hgt into closestHgt at given index
    for (unsigned int i = 0; i < hgts->size(); i++) 
      {
	closestHgt->at(id->at(i)) = hgts->at(i);
      }

    // Handle duplications by searching for each elt into the duplicates map
     for (unsigned int i = 0; i < closestHgt->size(); i++) 
       {
	//closestHgt->at(id->at(i)) = hgts->at(i);
	itMap = duplicates->find(i);
	if (itMap != duplicates->end())
	  {
	    int ind = duplicates->at(i);

	    closestHgt->at(i) = closestHgt->at(ind);
	  } 
      }

    return 0;
  }

  otbSetObjectMemberMacro(Filter, UserIgnoredValue, RealType);
  otbGetObjectMemberMacro(Filter, UserIgnoredValue, RealType);

protected:
  /** Constructor */
  SARStreamingDEMClosestHgtFilter() {};
  /** Destructor */
  ~SARStreamingDEMClosestHgtFilter() ITK_OVERRIDE {}

private:
  SARStreamingDEMClosestHgtFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented
  
};

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARStreamingDEMClosestHgtFilter.txx"
#endif

#endif
