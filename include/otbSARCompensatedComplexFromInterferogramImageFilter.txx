/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCompensatedComplexFromInterferogramImageFilter_txx
#define otbSARCompensatedComplexFromInterferogramImageFilter_txx

#include "otbSARCompensatedComplexFromInterferogramImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "itkContinuousIndex.h"

#include <cmath>
#include <algorithm>


namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageSAR, class TImageOut> 
  SARCompensatedComplexFromInterferogramImageFilter< TImageSAR, TImageOut >::SARCompensatedComplexFromInterferogramImageFilter()
  {
  }
    
  /** 
   * Destructor
   */
  template <class TImageSAR, class TImageOut> 
  SARCompensatedComplexFromInterferogramImageFilter< TImageSAR, TImageOut >::~SARCompensatedComplexFromInterferogramImageFilter()
  {
  }

  /**
   * Print
   */
  template<class TImageSAR, class TImageOut>
  void
  SARCompensatedComplexFromInterferogramImageFilter< TImageSAR, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageSAR, class TImageOut>
  void
  SARCompensatedComplexFromInterferogramImageFilter< TImageSAR, TImageOut >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the inputs and output
    ImageInConstPointer interfPtr = this->GetInput();
    ImageOutPointer outputPtr = this->GetOutput();

    // Metadata
    ImageMetadata interfMD = interfPtr->GetImageMetadata();

    
    /////////////////////// Main Output : conjugate multiplication (into interferogram geometry) ///////////////////////
    // Vector Image  :
    // At Least 3 Components :  
    //                _ real part
    //                _ imag part
    //                _ modulus
    outputPtr->SetNumberOfComponentsPerPixel(3);

    // The output is defined with the Master SAR Image
    // Origin, Spacing and Size (SAR master geometry)
    ImageOutPointType outOrigin;
    outOrigin = interfPtr->GetOrigin();
    ImageOutSpacingType outSP;
    outSP = interfPtr->GetSpacing();

    // Define Output Largest Region
    ImageOutRegionType outputLargestPossibleRegion = interfPtr->GetLargestPossibleRegion();

    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSpacing(outSP);

    // Add ML factors and bands meaning into Metadata
    ImageMetadata outputMD = interfMD;
    // outputMD.AddKey("band.Real", std::to_string(0));
    // outputMD.AddKey("band.Phase", std::to_string(1));
    // outputMD.AddKey("band.Coherency", std::to_string(2));

    // Set new keyword list to output image
    outputPtr->SetImageMetadata(outputMD);    
  }

  

  /**
   * Method ThreadedGenerateData
   */
  template<class TImageSAR, class TImageOut>
  void
  SARCompensatedComplexFromInterferogramImageFilter< TImageSAR, TImageOut >
  ::ThreadedGenerateData(const ImageOutRegionType & outputRegionForThread,
			 itk::ThreadIdType threadId)
  {
    //  Define/declare an iterator that will walk the input regions for this
    // thread. 
    InputIterator  inIt(this->GetInput(), outputRegionForThread);
    
    // Define/declare an iterator that will walk the output region for this
    // thread.
    OutputIterator outIt(this->GetOutput(), outputRegionForThread);

    // Support progress methods/callbacks
    itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

    inIt.GoToBegin();
    outIt.GoToBegin();
    
    // Output Pixel (VectorImage Pixel)
    ImageOutPixelType outPixel;
    outPixel.Reserve(3);

    // For each Line
    while ( !inIt.IsAtEnd() && !outIt.IsAtEnd())
      {
	inIt.GoToBeginOfLine();
	outIt.GoToBeginOfLine();
	
	    // For each column
	    while (!inIt.IsAtEndOfLine() && !outIt.IsAtEndOfLine())
	      {
		// Conjugate multiplication (master * conj(slave))
		double real_interfero = (inIt.Get()[0]*cos(inIt.Get()[1]));

		double imag_interfero = (inIt.Get()[0]*sin(inIt.Get()[1]));

		///////////// Output assignations ///////////////
		outPixel[0] = real_interfero;

			
		outPixel[1] = imag_interfero;
			
			
		outPixel[2] = sqrt((real_interfero*real_interfero) + 
				   (imag_interfero*imag_interfero));

		
		outIt.Set(outPixel);
	    
		// Next colunm inputs
		++inIt;
		// Next colunm output
		++outIt;
	      }
	  
	    // Next line intputs
	    inIt.NextLine();
	    // Next line output
	    outIt.NextLine();
      }
  }


  } /*namespace otb*/

#endif
