DiapOTB is an official OTB remote module. It is available from OTB distributions.

All notable changes to DiapOTB will be documented in this file.

### OTB 8.x/v2.0.0

Coming, see [v2.0.0](https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/-/tree/diapotb-2.0)

### OTB 7.4.1/v1.1.0

Coming with :

#### Added

* Unit tests with ctest
* TSX/PAZ/TDX support 
* Refactoring of python_src

#### Fixed

* Correction on SARDEMGrid with Ossim projection
* Update utils script to retrieve fine orbit files

#### Removed

* old python chains


### OTB 7.3/v1.0.1

#### Added

* New utils scripts in python_src:
  * Add GCPs on tiff images
  * Generate configuration file from user's QA
  * Retrieve fine orbit files from ESA website
* Improve memory consumption
* New post-processing : Goldstein filtering

#### Fixed

* Correction on projetion for `SARCartesianMeanEstimation` application
* Imporve fine orbit selection
* Update ESD to handle if only one burst is processed

### OTB 7.2/v1.0.0

#### Added

* Processing chains with S1 (mode IW and SM) and Cosmo (mode SM adn Spotligth) support.
* Burst sélection (S1IW) between reference and secondary image.
* Use fine orbit files (if available) in processing.



