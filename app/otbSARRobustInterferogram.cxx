/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperCompositeApplication.h"
#include "otbWrapperApplicationFactory.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SARRobustInterferogram : public CompositeApplication
    {
    public:
      typedef SARRobustInterferogram Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SARRobustInterferogram, otb::Wrapper::CompositeApplication);
     
     
    private:
      void DoInit() override
      {
	// Clear all internal applications
	ClearApplications();
       
	// Add the three internal applications
	AddApplication("SARTopographicPhase", "TopographicPhaseApp", "Application for topographic phase estimation");
	AddApplication("SARInterferogram", "InterferogramApp", "Application for Interferogram");

	// SARRobustInterferogram descripttion
	SetName("SARRobustInterferogram");
	SetDescription("Estimates robust interferogram with flatenning and averaging.");

	SetDocLongDescription("This application estimates the flatenning and "
        "averaging interferogram between two SAR images (Master and "
        "Coregistrated Slave).");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "incoregistratedslave",   "Input SAR Slave image (Coregistrated image)");
	SetParameterDescription("incoregistratedslave", "Input SAR Slave image (Coregistrated image).");

	AddParameter(ParameterType_InputImage,  "insarslave",   "Input SAR Slave image (Metadata)");
	SetParameterDescription("insarslave", "Input SAR Slave image (Metadata).");

	AddParameter(ParameterType_InputImage,  "insarmaster",   "Input SAR Master image");
	SetParameterDescription("insarmaster", "Input SAR Master image.");

	AddParameter(ParameterType_InputImage,  "ingrid",   "Input deformation grid");
	SetParameterDescription("ingrid", "Input deformation grid.");

	AddParameter(ParameterType_InputImage,  "incartmeanmaster",   "Input Cartesian Mean Master image");
	SetParameterDescription("incartmeanmaster", "Input Cartesian Mean Master image.");

	AddParameter(ParameterType_Int,  "gridsteprange", "Grid Step for range dimension into SLC geometry");
	SetParameterDescription("gridsteprange", "Grid Step for range dimension into  SLC geometry.");
	SetDefaultParameterInt("gridsteprange", 150);
	MandatoryOff("gridsteprange");

	AddParameter(ParameterType_Int,  "gridstepazimut", "Grid Step for azimut dimension into SLC geometry");
	SetParameterDescription("gridstepazimut", "Grid Step for azimut dimension into  SLC geometry.");
	SetDefaultParameterInt("gridstepazimut", 150);
	MandatoryOff("gridstepazimut");
        
	AddParameter(ParameterType_Int,  "mlran", "ML factor on range");
	SetParameterDescription("mlran", "ML factor on range.");
	SetDefaultParameterInt("mlran", 3);
	MandatoryOff("mlran");

	AddParameter(ParameterType_Int,  "mlazi", "ML factor on azimut");
	SetParameterDescription("mlazi", "ML factor on azimut.");
	SetDefaultParameterInt("mlazi", 3);
	MandatoryOff("mlazi");

	AddParameter(ParameterType_Int, "marginran", "Margin on distance for averaging");
	SetParameterDescription("marginran","Margin on distance for averaging");
	SetDefaultParameterInt("marginran", 1);
	SetMinimumParameterIntValue("marginran", 0);
	MandatoryOff("marginran");
    
	AddParameter(ParameterType_Int, "marginazi", "Margin on azimut for averaging");
	SetParameterDescription("marginazi","Margin on azimut for averaging");
	SetDefaultParameterInt("marginazi", 1);
	SetMinimumParameterIntValue("marginazi", 0);
	MandatoryOff("marginazi");
	
	AddParameter(ParameterType_Float, "gain", "Gain to apply for amplitude estimation");
	SetParameterDescription("gain","Gain to apply for amplitude estimation");
	SetDefaultParameterFloat("gain", 0.1);
	SetMinimumParameterFloatValue("gain", 0);
	MandatoryOff("gain");
	
	AddParameter(ParameterType_OutputImage, "out", "Output interferogram (ML geometry)");
	SetParameterDescription("out","Output interferogram (ML geometry).");

	AddRAMParameter();

	SetDocExampleParameterValue("insarslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002.tiff");
	SetDocExampleParameterValue("incoregistratedslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002_coregistrated.tiff");
	SetDocExampleParameterValue("insarmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
	SetDocExampleParameterValue("incartmeanmaster","CartesianMaster.tiff");
	SetDocExampleParameterValue("ingrid","./FineDeformation.tiff");
	SetDocExampleParameterValue("gain","0.1");
	SetDocExampleParameterValue("out","s1b-s1a-s4-RobustInterferogram.tiff");
      }

      void DoUpdateParameters() override
      {
	// Nothing to do here : all parameters are independent
      }

      void DoExecute() override
      { 
	// Get numeric parameters : gridStep and ML factors
	int gridStepRan = GetParameterInt("gridsteprange");
	int gridStepAzi = GetParameterInt("gridstepazimut");
	int mlRan = GetParameterInt("mlran");
	int mlAzi = GetParameterInt("mlazi");
	int margin_Ran = GetParameterInt("marginran");
	int margin_Azi = GetParameterInt("marginazi");
	float gain = GetParameterFloat("gain");
	    
	otbAppLogINFO(<<"Grid Step into Range : "<<gridStepRan);
	otbAppLogINFO(<<"Grid Step into Azimut : "<<gridStepAzi);
	otbAppLogINFO(<<"ML factor on range : "<<mlRan);
	otbAppLogINFO(<<"ML factor on azimut : "<<mlAzi);
	otbAppLogINFO(<<"Averaging Margin on azimut :"<<margin_Azi);
	otbAppLogINFO(<<"Averaging Margin on range : "<<margin_Ran);
	otbAppLogINFO(<<"Gain : "<<gain);
    
	// Retrive inputs
	FloatVectorImageType::Pointer GridPtr = GetParameterFloatVectorImage("ingrid");
	FloatVectorImageType::Pointer MasterCartMeanPtr = GetParameterFloatVectorImage("incartmeanmaster");

	ComplexFloatImageType::Pointer MasterSAR = GetParameterComplexFloatImage("insarmaster");
	ComplexFloatImageType::Pointer SlaveSAR = GetParameterComplexFloatImage("insarslave");
	ComplexFloatImageType::Pointer CoRegistedSlaveSAR = GetParameterComplexFloatImage("incoregistratedslave");
	
	// Execute the Pipeline with Internal applications
	// First Application : SARTopographicPhase
	GetInternalApplication("TopographicPhaseApp")->SetParameterInputImage("ingrid", GridPtr);
	GetInternalApplication("TopographicPhaseApp")->SetParameterInputImage("incartmeanmaster", 
									     MasterCartMeanPtr);
	GetInternalApplication("TopographicPhaseApp")->SetParameterInputImage("insarmaster", MasterSAR);
	GetInternalApplication("TopographicPhaseApp")->SetParameterInputImage("insarslave", SlaveSAR);
	GetInternalApplication("TopographicPhaseApp")->SetParameterInt("gridsteprange", gridStepRan);
	GetInternalApplication("TopographicPhaseApp")->SetParameterInt("gridstepazimut", gridStepAzi);
	GetInternalApplication("TopographicPhaseApp")->SetParameterInt("mlran", mlRan);
	GetInternalApplication("TopographicPhaseApp")->SetParameterInt("mlazi", mlAzi);
	GetInternalApplication("TopographicPhaseApp")->SetParameterInt("ram", GetParameterInt("ram"));
	
	ExecuteInternal("TopographicPhaseApp");

	// Second Application : SARInterferogram
	GetInternalApplication("InterferogramApp")->SetParameterInputImage("insarmaster", MasterSAR);
	GetInternalApplication("InterferogramApp")->SetParameterInputImage("insarslave", 
										  CoRegistedSlaveSAR);
	
	GetInternalApplication("InterferogramApp")->SetParameterInputImage("topographicphase", 
									   GetInternalApplication("TopographicPhaseApp")->GetParameterOutputImage("out"));
	
	GetInternalApplication("InterferogramApp")->SetParameterInt("mlran", mlRan);
	GetInternalApplication("InterferogramApp")->SetParameterInt("mlazi", mlAzi);
	GetInternalApplication("InterferogramApp")->SetParameterInt("marginran", margin_Ran);
	GetInternalApplication("InterferogramApp")->SetParameterInt("marginazi", margin_Azi);
	GetInternalApplication("InterferogramApp")->SetParameterFloat("gain", gain);
	GetInternalApplication("InterferogramApp")->SetParameterInt("ram", GetParameterInt("ram"));

	ExecuteInternal("InterferogramApp");

	// Main output 
	SetParameterOutputImage("out", GetInternalApplication("InterferogramApp")->GetParameterOutputImage("out"));

      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;

    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SARRobustInterferogram)
