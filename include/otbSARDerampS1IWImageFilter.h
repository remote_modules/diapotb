/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDerampS1IWImageFilter_h
#define otbSARDerampS1IWImageFilter_h

#include "otbSARDerampImageFilter.h"

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageMetadata.h"
#include "otbVectorImage.h"

namespace otb
{
/** \class SARDerampImageFilter 
 * \brief Deramps or Reramps a burst from Sentinel-1 IW product. 
 * 
 * This filter deramps or reramps an input burst.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImage> 
  class ITK_EXPORT SARDerampS1IWImageFilter :
    public SARDerampImageFilter<TImage>
{
public:

  // Standard class typedefs
  typedef SARDerampS1IWImageFilter                         Self;
  typedef SARDerampImageFilter<TImage>                   Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARDerampS1IWImageFilter,SARDerampImageFilter);

  /** Typedef to image input/output type VectorImage (Complex Image)   */
  typedef TImage                                  ImageType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageType::Pointer             ImagePointer;
  typedef typename ImageType::ConstPointer        ImageConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageType::RegionType          ImageRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageType::PixelType           ImagePixelType;
  typedef typename ImageType::PointType           ImagePointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageType::IndexType           ImageIndexType;
  typedef typename ImageType::IndexValueType      ImageIndexValueType;
  typedef typename ImageType::SizeType            ImageSizeType;
  typedef typename ImageType::SizeValueType       ImageSizeValueType;
  typedef typename ImageType::SpacingType         ImageSpacingType;
  typedef typename ImageType::SpacingValueType    ImageSpacingValueType;

  
  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageType > OutputIterator;

  // Time/Date
  using TimeType = otb::MetaData::TimePoint;
  using DurationType = otb::MetaData::Duration;

   // Struture declaration
   struct FMRateRecordType
   {
  TimeType      azimuthFMRateTime;
  double        coef0FMRate;
  double        coef1FMRate;
  double        coef2FMRate;
  double        tau0FMRate;
  friend std::ostream & operator<<(std::ostream & os, const FMRateRecordType & v)
  {
  return os << "{ azimuthFMRateTime: " << v.azimuthFMRateTime
	    <<        ", coefficient 0: "  << v.coef0FMRate
	    <<        ", coefficient 1: "        << v.coef1FMRate
	    <<        ",coefficient 2: "         << v.coef2FMRate
	    <<        ",slant range time (tau 0): "         << v.tau0FMRate
	    <<        "}";
}
};

   struct DCFRecordType
   {
  TimeType      azimuthDCFTime;
  double        coef0DCF;
  double        coef1DCF;
  double        coef2DCF;
  double        tau0DCF;
  friend std::ostream & operator<<(std::ostream & os, const DCFRecordType & v)
  {
  return os << "{ azimuthDCFTime: " << v.azimuthDCFTime
	    <<        ", coefficient 0: "  << v.coef0DCF
	    <<        ", coefficient 1: "        << v.coef1DCF
	    <<        ",coefficient 2: "         << v.coef2DCF
	    <<        ",slant range time (tau 0): "         << v.tau0DCF
	    <<        "}";
}
};


protected:
  // Constructor
  SARDerampS1IWImageFilter();

  // Destructor
  virtual ~SARDerampS1IWImageFilter() ITK_OVERRIDE;

  void getAllCoefs(ImageMetadata  const& kwl, std::vector<FMRateRecordType> & FMRateRecords);
  void getAllCoefs(ImageMetadata  const& kwl, std::vector<DCFRecordType> & DCFRecords);

  long double applyFMRateCoefs(double index_sample);
  long double applyDCFCoefs(double index_sample);

  double getPhi(double indL, double indC) override;
  void ThreadInit(ImageMetadata inputKWL) override;

 private:
  SARDerampS1IWImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 

  // Polynomial Selection
  bool selectFMRateCoef();
  bool selectDCFCoef();

  // Parameters coefficients for Doppler FM Rate
  double m_FM_C0, m_FM_C1, m_FM_C2, m_FM_Tau0;

  // Parameters coefficients for Doppler Centroid Frequency
  double m_DCF_C0, m_DCF_C1, m_DCF_C2, m_DCF_Tau0;


  const double C = 299792458;
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARDerampS1IWImageFilter.txx"
#endif



#endif
