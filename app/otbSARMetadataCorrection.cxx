
/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARStreamingDEMClosestHgtFilter.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <complex>
#include <cmath>
#include <ctime>
#include <iosfwd>

#include "otb_tinyxml.h"
#include "itksys/SystemTools.hxx"

#include "otbDateTime.h"
#include "date.h"

enum
{
  Mode_UserDefined,
  Mode_GCP,
  Mode_Orbits
};



// Time/Date
using TimeType = otb::MetaData::TimePoint;
using DurationType = otb::MetaData::Duration;

// Keys for ImageMetadata
// Gcp keys
const std::string keyAzimuthTime      = "azimuthTime";
const std::string keySlantRangeTime   = "slant_range_time";
const std::string keyImPtX            = "im_pt.x";
const std::string keyImPtY            = "im_pt.y";
const std::string keyWorldPtLat       = "world_pt.lat";
const std::string keyWorldPtLon       = "world_pt.lon";
const std::string keyWorldPtHgt       = "world_pt.hgt";
const std::string GCP_PREFIX          = "support_data.geom.gcp";
const std::string GCP_NUMBER_KEY      = "support_data.geom.gcp.number";
const std::string SUPPORT_DATA_PREFIX = "support_data.";
// Orbit keys
const std::string ORBIT_PREFIX        = "orbitList.orbit";
const std::string ORBIT_NUMBER_KEY    = "orbitList.nb_orbits";
const std::string keyTime             = "time";
const std::string keyPosX             = "x_pos";
const std::string keyPosY             = "y_pos";
const std::string keyPosZ             = "z_pos";
const std::string keyVelX             = "x_vel";
const std::string keyVelY             = "y_vel";
const std::string keyVelZ             = "z_vel";


///// Functions to handle metadata //////
// Get the four corners and estimate all useful info //
void getCorners(otb::ImageMetadata imgMD, std::vector<double *>* vector_lonlat,
		std::vector<int *>* vector_colrow, std::vector<int> & vector_ind,
		std::vector<TimeType> &  vector_aziTime,
		std::vector<double> & vector_ranTime)
{
  // Always the same order to fill all vector : ul, ur, lr and center

  // Retrieve lon and lat for the four corners from imgMD
  double ullat = imgMD.GetSARParam().ulSceneCoord.latitude;
  double ullon = imgMD.GetSARParam().ulSceneCoord.longitude;
  double urlat = imgMD.GetSARParam().urSceneCoord.latitude;
  double urlon = imgMD.GetSARParam().urSceneCoord.longitude;
  double lrlat = imgMD.GetSARParam().lrSceneCoord.latitude;
  double lrlon = imgMD.GetSARParam().lrSceneCoord.longitude;
  double lllat = imgMD.GetSARParam().llSceneCoord.latitude;
  double lllon = imgMD.GetSARParam().llSceneCoord.longitude;

  // Create center lon lat
  double centerlat = 0.25 * (ullat + urlat + lrlat + lllat);
  double centerlon = 0.25 * (ullon + urlon + lrlon + lllon);

  // Fill our vectors
  // Lon lat
  double * ul = new double[2];
  double * ur = new double[2];
  double * lr = new double[2];
  double * ll = new double[2];
  double * center = new double[2];
  ul[0] = ullon;
  ul[1] = ullat;
  ur[0] = urlon;
  ur[1] = urlat;
  ll[0] = lllon;
  ll[1] = lllat;
  lr[0] = lrlon;
  lr[1] = lrlat;
  center[0] = centerlon;
  center[1] = centerlat;
  vector_lonlat->push_back(ul);
  vector_lonlat->push_back(ur);
  vector_lonlat->push_back(ll);
  vector_lonlat->push_back(lr);
  vector_lonlat->push_back(center);

  // idx
  vector_ind.push_back(0);
  vector_ind.push_back(1);
  vector_ind.push_back(2);
  vector_ind.push_back(3);
  vector_ind.push_back(4);

  // col row
  int nbRows = std::atoi(imgMD["NumberOfLines"].c_str());
  int nbCols = std::atoi(imgMD["NumberOfColumns"].c_str());
  int * ul_cl = new int[2];
  int * ur_cl = new int[2];
  int * ll_cl = new int[2];
  int * lr_cl = new int[2];
  int * center_cl = new int[2];
  ul_cl[0] = 0;
  ul_cl[1] = 0;
  ur_cl[0] = nbCols;
  ur_cl[1] = 0;
  ll_cl[0] = 0;
  ll_cl[1] = nbRows;
  lr_cl[0] = nbCols;
  lr_cl[1] = nbRows;
  center_cl[0] = nbCols/2;
  center_cl[1] = nbRows/2;
  vector_colrow->push_back(ul_cl);
  vector_colrow->push_back(ur_cl);
  vector_colrow->push_back(ll_cl);
  vector_colrow->push_back(lr_cl);
  vector_colrow->push_back(center_cl);

  // azi and ran Time vectors
  TimeType aziFirstTime = imgMD[otb::MDTime::AcquisitionStartTime];
  double ranFirst = imgMD.GetSARParam().nearRangeTime;
  double aziTimeInterval = imgMD.GetSARParam().azimuthTimeInterval.TotalSeconds();
  double ranRate = imgMD.GetSARParam().rangeSamplingRate;

  TimeType time_first_pixel_Date = aziFirstTime;
  // Estimate time_line for center and last : time_line = time_first_line + ind_line*aziTimeInterval
  TimeType center_Azitime = time_first_pixel_Date +
    DurationType::Seconds(center_cl[1]*aziTimeInterval);
  TimeType last_Azitime = time_first_pixel_Date +
    DurationType::Seconds(lr_cl[1]*aziTimeInterval);

  // Estimate range distance for center and last : ran_dist = ran_first_dist + ind_col/ranRate
  double center_ranDist = ranFirst + center_cl[0]/ranRate;
  double last_ranDist = ranFirst + lr_cl[0]/ranRate;

  vector_aziTime.push_back(time_first_pixel_Date);
  vector_aziTime.push_back(time_first_pixel_Date);
  vector_aziTime.push_back(last_Azitime);
  vector_aziTime.push_back(last_Azitime);
  vector_aziTime.push_back(center_Azitime);

  vector_ranTime.push_back(ranFirst);
  vector_ranTime.push_back(last_ranDist);
  vector_ranTime.push_back(ranFirst);
  vector_ranTime.push_back(last_ranDist);
  vector_ranTime.push_back(center_ranDist);
}

// Get GCP //
void getGCP(otb::Wrapper::ComplexFloatImageType::Pointer metadataInterface,
	    int gcpcount, otb::ImageMetadata md,
	    std::vector<double *>* vector_lonlat,
	    std::vector<int *>* vector_colrow, std::vector<int> & vector_ind)
{
  // TODO : Check if it is still necessary to do this check or if we can just use the Metadata directly
  // Get the count of GCPs and check if metadataInterface is consistent
  // Sometimes (for S1 IW) metadataInterace is not updated
  int gcpcount_interface = metadataInterface->GetGCPCount();

  // if consistent, use it
  if (gcpcount == gcpcount_interface)
    {
      // Loop on all GCPs
      for (int gcpIdx = 0; gcpIdx < gcpcount; ++gcpIdx)
	{
	  // Fill our vectors
	  // Lon lat
	  double * gcpLonLat = new double[2];
	  gcpLonLat[0] = static_cast<double>(metadataInterface->GetGCPX(gcpIdx));
	  gcpLonLat[1] = static_cast<double>(metadataInterface->GetGCPY(gcpIdx));
	  vector_lonlat->push_back(gcpLonLat);

	  // idx
	  vector_ind.push_back(std::atoi(metadataInterface->GetGCPId(gcpIdx).c_str()));

	  // col row
	  int * gcpColRow = new int[2];
	  gcpColRow[0] = metadataInterface->GetGCPCol(gcpIdx);
	  gcpColRow[1] = metadataInterface->GetGCPRow(gcpIdx);
	  vector_colrow->push_back(gcpColRow);
	}
    }
  // else use the md
  else
    {
      // Loop on all GCPs
      for (int gcpIdx = 0; gcpIdx < gcpcount; ++gcpIdx)
	{
	  // Fill our vectors
	  // Lon lat
	  double * gcpLonLat = new double[2];
    gcpLonLat[0] = md.GetGCPParam().GCPs[gcpIdx].m_GCPX;
    gcpLonLat[1] = md.GetGCPParam().GCPs[gcpIdx].m_GCPY;
	  vector_lonlat->push_back(gcpLonLat);

	  // idx
	  vector_ind.push_back(gcpIdx);

	  // col row
	  int * gcpColRow = new int[2];
    gcpColRow[0] = md.GetGCPParam().GCPs[gcpIdx].m_GCPCol;
    gcpColRow[0] = md.GetGCPParam().GCPs[gcpIdx].m_GCPRow;
	  vector_colrow->push_back(gcpColRow);
	}
    }
}

// Create new GCPs and update outMD //
int createGCPAndUpdateMD(std::vector<double *>* vector_lonlat, std::vector<double> * vector_hgt,
			  std::vector<int *>* vector_colrow, std::vector<int> vector_ind,
			  std::vector<TimeType>  vector_aziTime,
			  std::vector<double> vector_ranTime, otb::ImageMetadata & outMD)
{
  // Check size (same size for all vectors)
  unsigned int vecSize = vector_lonlat->size();
  if (vector_colrow->size() != vecSize || vector_ind.size() != vecSize ||
      vector_aziTime.size() != vecSize || vector_ranTime.size() != vecSize)
    {
      // retrun 1 => Pb
      return 1;
    }


  std::string gcpCount = std::to_string(vecSize);
  outMD.Add(GCP_NUMBER_KEY, gcpCount);
  otb::Projection::GCPParam gcpPrm;
  std::unordered_map<std::string, otb::GCPTime> gcpTimes;
  // Create or update each GCP
  for (unsigned int i = 0; i < vecSize; i++)
    {
      // Add the GCP to md
      gcpPrm.GCPs.emplace_back(std::to_string(i),                              // ID
                               "",                                             // Comment
                               vector_colrow->at(i)[0],                        // col
                               vector_colrow->at(i)[1],                        // row
                               vector_lonlat->at(i)[1],                        // px
                               vector_lonlat->at(i)[0],                        // py
                               vector_hgt->at(i));                             // pz
      otb::GCPTime time;
      time.slantRangeTime = vector_ranTime[i];
      time.azimuthTime = vector_aziTime[i];
      gcpTimes[std::to_string(i)] = time;
    }
  gcpPrm.GCPProjection = otb::SpatialReference::FromWGS84().ToWkt();
  outMD.Add(otb::MDGeom::GCP, gcpPrm);  // This step will erase the GCP read by GDAL if any.
  auto sarParam = outMD.GetSARParam();
  sarParam.gcpTimes = std::move(gcpTimes);
  outMD.Add(otb::MDGeom::SAR, sarParam);

  return 0;
}

// Update only GCP height and MD //
int updateGCPAndMD(std::vector<double> * vector_hgt, unsigned int gcpcount, otb::ImageMetadata & outMD)
{
  // Check vector size with gcp count
  if (vector_hgt->size() != gcpcount)
    {
      std::cout << "Wrong size, does not correspond to the GCP count" << std::endl;
      return 1;
    }

  otb::Projection::GCPParam gcpPrm = outMD.GetGCPParam();
  // Create or update each GCP
  for (unsigned int i = 0; i < vector_hgt->size(); i++)
    {
      // Add the GCP to md
      // only update the keyWorldPtHgt
      gcpPrm.GCPs[i].m_GCPZ = vector_hgt->at(i);
    }
  outMD.Add(otb::MDGeom::GCP, gcpPrm);  // This step will erase the GCP read by GDAL if any.
  return 0;
}

// Selection for fine orbits (choose only orbits that fit with the image)
int selectOrbits(std::vector<std::string> * vector_time,
		 const otb::ImageMetadata inMD, int & ind_first, int & ind_last)
{
  // Initialize indexes
  ind_first = 0;
  ind_last = vector_time->size() - 1;

  std::stringstream stream;

  // Get time for first and last original orbit states
  int orbitCount = inMD.GetSARParam().orbits.size();
  stream.str("");
  stream << inMD.GetSARParam().orbits[0].time;
  std::string UTC_first = stream.str();
  stream.str("");
  stream << inMD.GetSARParam().orbits[orbitCount-1].time;
  std::string UTC_last = stream.str();

  // Transform string to t_time (in seconds)
  std::tm tm1 ={};
  std::tm tm2 ={};

  std::istringstream UTC_first_ss (UTC_first);
  UTC_first_ss.imbue(std::locale(setlocale(LC_ALL, nullptr)));
  UTC_first_ss >> std::get_time(&tm1, "%Y-%m-%dT%H:%M:%SZ");
  time_t t_first = mktime(&tm1);

  std::istringstream UTC_last_ss (UTC_last);
  UTC_last_ss.imbue(std::locale(setlocale(LC_ALL, nullptr)));
  UTC_last_ss >> std::get_time(&tm2, "%Y-%m-%dT%H:%M:%SZ");
  time_t t_last = mktime(&tm2);


  // Selection of indexes into vector_time
  // ind_first = last elt to be inferior to t_first
  // ind_last = first elt to be superior to t_last
  for (unsigned int i = 0; i < vector_time->size(); i++)
    {
      std::tm tm;
      std::istringstream UTC_ss (vector_time->at(i));
      UTC_ss >> std::get_time(&tm, "%Y-%m-%dT%H:%M:%SZ");
      time_t t = mktime(&tm);

      if (difftime(t, t_first) > 0 && ind_first == 0)
	{
	  if (i !=0)
	    {
	      ind_first = i - 1;
	    }
	}
      if (difftime(t_last, t) < 0)
	{
	  ind_last = i;

	  break;
	}
    }
  return 0;
}


// Create new Orbits and update outMD //
int createOrbitsAndUpdateMD(std::vector<double *>* vector_posvel,
			     std::vector<std::string> * vector_time,
			     int ind_first, int ind_last,
			     otb::ImageMetadata & outMD)
{
  // Check size (same size for all vectors)
  unsigned int vecSize = vector_posvel->size();
  if (vector_time->size() != vecSize)
    {
      // retrun 1 => Pb
      return 1;
    }

  int orbitCount = outMD.GetSARParam().orbits.size();
  if (vecSize < static_cast<unsigned int>(orbitCount))
    {
      // Fine orbit are inferior to original => pb
      return 1;
    }

  std::vector<otb::Orbit> orbitVector;
  std::string orbitFineCount = std::to_string(ind_last - ind_first + 1);
  outMD.Add(ORBIT_NUMBER_KEY, orbitFineCount);

  // Create or update each Orbit state
  for (unsigned int i = 0;
       i < static_cast<unsigned int>(std::atoi(orbitFineCount.c_str())); i++)
    {
      // Add the orbit to md
      otb::Orbit orbit;

      // time
      orbit.time = otb::MetaData::ReadFormattedDate(vector_time->at(ind_first + i));

      // position
      orbit.position[0] = vector_posvel->at(ind_first + i)[0];
      orbit.position[1] = vector_posvel->at(ind_first + i)[1];
      orbit.position[2] = vector_posvel->at(ind_first + i)[2];

      // velocity
      orbit.velocity[0] = vector_posvel->at(ind_first + i)[3];
      orbit.velocity[1] = vector_posvel->at(ind_first + i)[4];
      orbit.velocity[2] = vector_posvel->at(ind_first + i)[5];

      orbitVector.push_back(std::move(orbit));
    }
  auto sarParam = outMD.GetSARParam();
  sarParam.orbits = std::move(orbitVector);
  outMD.Add(otb::MDGeom::SAR, sarParam);
  return 0;
}

void WriteImageMetadataToGeomFileCorrected(const otb::ImageMetadata & imd, const std::string & filename)
{
  std::ofstream myfile(filename);
  auto writeKwl = [&myfile](const otb::MetaData::Keywordlist & kwl, const std::string prefix ="")
  {
    for (const auto & kv: kwl)
    {
      myfile << prefix << kv.first << ":" << kv.second << std::endl;
    }
  };

  otb::MetaData::Keywordlist kwl;
  imd.ToKeywordlist(kwl);
  // SARParam
  if (imd.Has(otb::MDGeom::SAR))
  {
    imd.GetSARParam().ToKeywordlist(kwl, "SAR.");
    // GCPParam
    if (imd.Has(otb::MDGeom::GCP))
    {
      const otb::Projection::GCPParam & gcpParam =
        boost::any_cast<const otb::Projection::GCPParam&>(imd[otb::MDGeom::GCP]);
      gcpParam.ToKeywordlist(kwl, "SAR.");
    }
  }
  // SARCalib
  if (imd.Has(otb::MDGeom::SARCalib))
  {
    const auto & param = boost::any_cast<const otb::SARCalib&>(imd[otb::MDGeom::SARCalib]);
    param.ToKeywordlist(kwl, "SARCalib.");
  }

  writeKwl(kwl);

  // Band are indexed starting at 1
  int bIdx = 1;
  for (const auto& band : imd.Bands)
  {
    band.ToKeywordlist(kwl);
    writeKwl(kwl, "Band_" + std::to_string(bIdx) + ".");
    ++bIdx;
  }
}


///// OTB Application //////
namespace otb
{
  namespace Wrapper
{

class SARMetadataCorrection : public Application
{
public:
  typedef SARMetadataCorrection Self;
  typedef itk::SmartPointer<Self> Pointer;

  itkNewMacro(Self);
  itkTypeMacro(SARMetadataCorrection, otb::Wrapper::Application);


  // Filter
  typedef otb::SARStreamingDEMClosestHgtFilter<FloatImageType> DEMClosestHgtFilterType;

private:
  void DoInit() override
  {
    SetName("SARMetadataCorrection");
    SetDescription("SAR metadata correction.");

    SetDocLongDescription("This application corrects metadata with a focus on GCP.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "Input DEM to extract height data.");

    AddParameter(ParameterType_InputImage,  "insar",   "Input SAR image");
    SetParameterDescription("insar", "SAR Image to extract initial metadata.");

    AddParameter(ParameterType_InputFilename, "infineorbits", "Input file to get precise orbits");
    SetParameterDescription("infineorbits","Input file to get precise orbits.");
    MandatoryOff("infineorbits");

    // mode selection : gcp (auto) or fine orbits
    AddParameter(ParameterType_Choice, "mode", "Parameters estimation modes");
    AddChoice("mode.auto", "User Defined");
    SetParameterDescription("mode.auto", "This mode allows you to fully modify default values : GCP corrections");
    AddChoice("mode.gcp", "GCP corrections");
    SetParameterDescription("mode.gcp",
                            "This mode allows you to automatically correct GCP into the output geom file");
    AddChoice("mode.orbits", "Fine orbits");
    SetParameterDescription("mode.orbits",
                            "This mode allows you to automatically retrieve the fine orbits and to save into a new geom file");


    AddParameter(ParameterType_OutputFilename, "outmd", "Write the OSSIM Metadata to a geom file");
    SetParameterDescription("outmd", "This option allows extracting the OSSIM Metadata of the image into a geom file.");
    MandatoryOff("outmd");

    AddRAMParameter();

    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("insar","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
  }

  void DoUpdateParameters() override
  {
    // Handle inputs following the mode
    // chose by the user
    switch (GetParameterInt("mode"))
      {
      case Mode_UserDefined:
	{
	  EnableParameter("indem");
	  EnableParameter("outmd");
	  DisableParameter("infineorbits");

	  MandatoryOn("indem");
	  MandatoryOff("outmd");
	  MandatoryOff("infineorbits");
	}
      break;
      case Mode_GCP:
	{
	  EnableParameter("indem");
	  EnableParameter("outmd");
	  DisableParameter("infineorbits");

	  MandatoryOn("indem");
	  MandatoryOff("outmd");
	  MandatoryOff("infineorbits");
	}
      break;
      case Mode_Orbits:
	{
	  DisableParameter("indem");
	  EnableParameter("outmd");
	  EnableParameter("infineorbits");

	  MandatoryOff("indem");
	  MandatoryOn("outmd");
	  MandatoryOn("infineorbits");
	}
      break;
      } // switch (GetParameterInt("mode") )
  }

  void DoExecute() override
  {
    std::ostringstream ossOutput;
    ////// Get inputs and Metadata /////
    ComplexFloatImageType::Pointer SARPtr = GetParameterComplexFloatImage("insar");

    ImageMetadata outputMD = SARPtr->GetImageMetadata();


    // Get mode to adapt estimation according to user choice
    bool doGcpCorrection = false;
    bool doFineOrbits = false;

    switch (GetParameterInt("mode"))
      {
      case Mode_UserDefined:
	{
	  doGcpCorrection = true;
	}
      break;
      case Mode_GCP:
	{
	  doGcpCorrection = true;
	}
      break;
      case Mode_Orbits:
	{
	  doFineOrbits = true;
	}
      break;
      } // switch (GetParameterInt("mode") )

    /////// GCP Correction ///////
    if (doGcpCorrection)
      {
	FloatImageType::Pointer inputDEM =  GetParameterFloatImage("indem");
	// Check with metadataInterface and keyword List
	int gcpcount = SARPtr->GetGCPCount();
	if (gcpcount == 0)
	  {
	    int gcpcount_md = outputMD.GetGCPParam().GCPs.size();
	    if (gcpcount_md > 5)
	      {
		gcpcount = gcpcount_md;
	      }
	  }

	std::vector<double *> * vector_lonlat = new std::vector<double *>();
	std::vector<int *>   * vector_colrow = new std::vector<int *>();
	std::vector<int> vector_ind;
	std::vector<TimeType>  vector_aziTime;
	std::vector<double>  vector_ranTime;
	// Build the input (vector_lonlat) for GetClosestMNTHeight filter
	if (gcpcount == 0)
	  {
	    // If no GCP => Retrieve Corners instead
	    // Get latitude and longitude for ur, ul, ll lr and center points
	    try
	      {
		getCorners(SARPtr->GetImageMetadata(), vector_lonlat,
			   vector_colrow, vector_ind, vector_aziTime,
			   vector_ranTime);
	      }
	    catch (itk::ExceptionObject& /*err*/)
	      {
	      }
	  }
	else
	  {
	    // If GCPs => Retrieve its
	    // Get latitude and longitude for each of its
	    getGCP(SARPtr, gcpcount, outputMD, vector_lonlat, vector_colrow, vector_ind);
	  }


	///// DEMClosestHgt filter to estimate accurate heights /////
	DEMClosestHgtFilterType::Pointer filterDEMClosestHgt = DEMClosestHgtFilterType::New();
	m_Ref.push_back(filterDEMClosestHgt.GetPointer());

	// Set inputs : DEM and the vector_LonLat
	filterDEMClosestHgt->SetInput(inputDEM);
	filterDEMClosestHgt->SetVectorLonLat(vector_lonlat);

	// Init a vector for hgt
	std::vector<double> * vector_hgt = new std::vector<double>();
	vector_hgt->resize(vector_lonlat->size()); // Same size than lon and lat

	for (unsigned int k = 0; k < vector_lonlat->size(); k++)
	  {
	    vector_hgt->at(k) = 0;
	  }

	// Launch our Persistent filter
	filterDEMClosestHgt->Update();
	filterDEMClosestHgt->GetDEMHgt(vector_hgt);

	// Add or update GCP into the output MD (according to the sensor (GCPs already present or not))
	if (gcpcount == 0)
	  {
	    // Create GPCs with the four corners and center
	    createGCPAndUpdateMD(vector_lonlat, vector_hgt,
				  vector_colrow, vector_ind,
				  vector_aziTime,
				  vector_ranTime, outputMD);
	  }
	else
	  {
	    // Just update heigths
	    updateGCPAndMD(vector_hgt, gcpcount, outputMD);
	  }


	// Display image information in  the dedicated logger
	otbAppLogINFO(<< ossOutput.str());

	// Free Memory
	for (unsigned int itab = 0; itab < vector_lonlat->size(); itab++)
	  {
	    delete vector_lonlat->at(itab);
	    vector_lonlat->at(itab) = 0;
	  }
	vector_lonlat->clear();
	delete vector_lonlat;
	vector_lonlat = 0;

	vector_hgt->clear();
	delete vector_hgt;
	vector_hgt = 0;

	for (unsigned int itab = 0; itab < vector_colrow->size(); itab++)
	  {
	    delete vector_colrow->at(itab);
	    vector_colrow->at(itab) = 0;
	  }
	vector_colrow->clear();
	delete vector_colrow;
	vector_colrow = 0;

	vector_ind.clear();
	vector_aziTime.clear();
	vector_ranTime.clear();
      }

    /////// Fine orbits ///////
    if (doFineOrbits)
      {
	// Add into SAR Image keyword list, the precise metadata
	std::string inFile = GetParameterString("infineorbits");

	// Check that the right extension is given : expected .EOF */
	std::string extension = itksys::SystemTools::GetFilenameLastExtension(inFile);
	if (itksys::SystemTools::LowerCase(extension) != ".eof")
	  {
	    itkExceptionMacro(<< extension << " is a wrong Extension FileName : Expected .EOF");
	  }

	// Open the xml file
	TiXmlDocument doc(inFile.c_str());
	if (!doc.LoadFile())
	  {
	    itkExceptionMacro(<< "Can't open file " << inFile);
	  }

	TiXmlHandle   hDoc(&doc);

	// Get root node
	TiXmlElement* root = hDoc.FirstChildElement("Earth_Explorer_File").ToElement();
	if (root)
	  {
	    // Init vectors
	    std::vector<double *> * vector_posvel = new std::vector<double *>();
	    std::vector<std::string> * vector_time = new std::vector<std::string>();

	    // Get Data_Block
	    TiXmlElement* dataBlock = root->FirstChildElement("Data_Block");

	    // Retrieve the number of orbits
	    int count = std::atoi(dataBlock->FirstChildElement("List_of_OSVs")->Attribute("count"));

	    int count_loop = 0;

	    // Iterate through the tree to get all the orbits
	    // Fill vectors with its
	    for (TiXmlElement* currentOrb = dataBlock->FirstChildElement("List_of_OSVs")->FirstChildElement();
		 currentOrb != nullptr; currentOrb = currentOrb->NextSiblingElement())
	      {
		double * posvel = new double[6];

		std::string UTCTime = std::string(currentOrb->FirstChildElement("UTC")->GetText());
		UTCTime = UTCTime.substr(UTCTime.find("=")+1, UTCTime.length());

		posvel[0] = std::atof(currentOrb->FirstChildElement("X")->GetText());
		posvel[1] = std::atof(currentOrb->FirstChildElement("Y")->GetText());
		posvel[2] = std::atof(currentOrb->FirstChildElement("Z")->GetText());
		posvel[3] = std::atof(currentOrb->FirstChildElement("VX")->GetText());
		posvel[4] = std::atof(currentOrb->FirstChildElement("VY")->GetText());
		posvel[5] = std::atof(currentOrb->FirstChildElement("VZ")->GetText());

		vector_posvel->push_back(posvel);
		vector_time->push_back(std::string(UTCTime));

		++count_loop;
	      }
	    int ind_first, ind_last;
	    selectOrbits(vector_time, outputMD, ind_first, ind_last);

	    // Check coherency between the number of orbits
	    if (count != count_loop)
	      {
		itkExceptionMacro(<< "Problem of coherency into EOF file");
	      }


	    int ret = createOrbitsAndUpdateMD(vector_posvel, vector_time, ind_first,
					       ind_last, outputMD);

	    if (ret == 1)
	      {
		itkExceptionMacro(<< "Problem during orbits update");
	      }

	    // Free Memory
	    for (unsigned int itab = 0; itab < vector_posvel->size(); itab++)
	      {
		delete vector_posvel->at(itab);
		vector_posvel->at(itab) = 0;
	      }
	    vector_posvel->clear();
	    delete vector_posvel;
	    vector_posvel = 0;

	    vector_time->clear();
	    delete vector_time;
	    vector_time = 0;
	  }
      }

    /////// Write output Metadata with correct gcp (correction on heigth) ///////
    if (IsParameterEnabled("outmd") && HasValue("outmd"))
      {
        //WriteImageMetadataToGeomFileCorrected(outputMD, GetParameterString("outmd"));
        WriteImageMetadataToGeomFile(outputMD, GetParameterString("outmd"));
        using WriterType = otb::ImageFileWriter<ComplexFloatImageType>;
        WriterType::Pointer writer = WriterType::New();
        SARPtr->SetImageMetadata(outputMD);
        writer->SetFileName(GetParameterString("outmd"));
        writer->SetInput(SARPtr);
        try
        {
          writer->Update();
        }
        catch (itk::ExceptionObject& err)
        {
          std::cerr << "ExceptionObject caught !" << std::endl;
          itkExceptionMacro(<< err);
        }
      }
  }


  // Vector for filters
  std::vector<itk::ProcessObject::Pointer> m_Ref;

};

}

}
OTB_APPLICATION_EXPORT(otb::Wrapper::SARMetadataCorrection)
