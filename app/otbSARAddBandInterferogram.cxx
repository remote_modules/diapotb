/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"


#include "otbConcatenateVectorImageFilter.h"
#include "otbRealAndImaginaryImageToComplexImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"

#include "otbSARGroupedByMLImageFilter.h"

#include "otbSARCompensatedComplexFromInterferogramImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SARAddBandInterferogram : public Application
    {
    public:
      typedef SARAddBandInterferogram Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SARAddBandInterferogram, otb::Wrapper::Application);

      // Filters
     typedef otb::RealAndImaginaryImageToComplexImageFilter<FloatImageType, FloatImageType, ComplexFloatImageType> RealImagToComplexFilterType;
      
     typedef otb::MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType, FloatImageType::PixelType> ExtractROIFilterType;


     typedef otb::ConcatenateVectorImageFilter<FloatVectorImageType, FloatVectorImageType, 
					       FloatVectorImageType> ConcatenateFilterType;

     typedef otb::SARCompensatedComplexFromInterferogramImageFilter<FloatVectorImageType, 
							       FloatVectorImageType> CompensatedComplexFromInterferogramFilterType;
      
     typedef otb::SARGroupedByMLImageFilter<ComplexFloatImageType, FloatVectorImageType> GroupedByMLFilterType;

    private:
      void DoInit() override
      {
	SetName("SARAddBandInterferogram");
	SetDescription("Replace an amplitude for input interferogram.");

	SetDocLongDescription("Replace an amplitude for input interferogram and merge it to rebuild three bands (amp, pha and coh).");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "ininterfamp", "Input Interferogram with correct amplitude");
	SetParameterDescription("ininterfamp", "Input Interferogram with correct amplitude.");
	
	AddParameter(ParameterType_InputImage,  "incomplexamp",   "Input Compensated complex image");
	SetParameterDescription("incomplexamp", "Input Compensated complex image.");
	
	AddParameter(ParameterType_InputImage,  "ininterf",   "Input Interferogram");
	SetParameterDescription("ininterf", "Input Interferogram.");
	

	AddParameter(ParameterType_Int,  "mlran", "Averaging on distance for output interferogram");
	SetParameterDescription("mlran", "Averaging on distance for output interferogram.");
	SetDefaultParameterInt("mlran", 1);
	SetMinimumParameterIntValue("mlran", 1);
	MandatoryOff("mlran");

	AddParameter(ParameterType_Int,  "mlazi", "Averaging on azimut for output interferogram");
	SetParameterDescription("mlazi", "Averaging on azimut for output interferogram.");
	SetDefaultParameterInt("mlazi", 1);
	SetMinimumParameterIntValue("mlazi", 1);
	MandatoryOff("mlazi");

	AddParameter(ParameterType_Float, "gain", "Gain to apply on output amplitude");
	SetParameterDescription("gain","Gain to apply on output amplitude");
	SetDefaultParameterFloat("gain", 0.1);
	SetMinimumParameterFloatValue("gain", 0);
	MandatoryOff("gain");

	AddParameter(ParameterType_OutputImage, "out", "Output interferogram (after filtering)");
	SetParameterDescription("out","Output Image : Interferogram after filtering with 3 corrected bands (amp, pha, coh).");
	
	AddRAMParameter();

	SetDocExampleParameterValue("ininterfamp","interferogram_amp.tiff");
	SetDocExampleParameterValue("ininterf","interferogram_phacoh.tiff");
	SetDocExampleParameterValue("out","outInterferogram.tiff");
      }

      void DoUpdateParameters() override
      {
	// Handle the dependency between incomplexamp and ininterfamp (two kinds of pipelines)
	if (GetParameterByKey("incomplexamp")->HasValue())
	  {
	    DisableParameter("ininterfamp");
	    MandatoryOff("ininterfamp");

	    otbAppLogINFO(<<"Complex input");
	  }
	else
	  {
	    EnableParameter("ininterfamp");
	    MandatoryOn("ininterfamp");

	    MandatoryOff("incomplexamp");

	    otbAppLogINFO(<<"Interferogram input");
	  }
      }

      void DoExecute() override
      { 

	// Get numeric parameters 
	int factor_azi = GetParameterInt("mlazi");
	int factor_ran = GetParameterInt("mlran");
	float gain = GetParameterFloat("gain");

	FloatVectorImageType::Pointer inInterfPtr = GetParameterFloatVectorImage("ininterf");
		
	otbAppLogINFO(<<"Averaging Factor on azimut for final interferogram :"<<factor_azi);
	otbAppLogINFO(<<"Averaging Factor on range for final interferogram : "<<factor_ran);
	otbAppLogINFO(<<"Gain to apply on output amplitude : "<<gain);

	// Complex Ptr
	ComplexFloatImageType::Pointer inComplexAmpPtr;

	// Two kinds of Pipeline : one with complex image and one with interferogram
	if (GetParameterByKey("incomplexamp")->HasValue())
	  {
	    inComplexAmpPtr = GetParameterComplexFloatImage("incomplexamp");
	  }
	else
	  {
	    FloatVectorImageType::Pointer inInterfAmpPtr = GetParameterFloatVectorImage("ininterfamp");
	
	    // Check ML factors (must be 1x1) thanks to the Metadata
	    otb::ImageMetadata MD = inInterfAmpPtr->GetImageMetadata();
	    int mlran_interf = 1;
	    int mlazi_interf = 1;
	    if (MD.Has("ml_ran"))
	      {
		mlran_interf = std::atoi(MD["ml_ran"].c_str());
	      }
	    if (MD.Has("ml_azi"))
	      {
		mlazi_interf = std::atoi(MD["ml_azi"].c_str());
	      }
	    
	    if (mlran_interf != 1 || mlazi_interf != 1)
	      {
		// Send an exception
		itkExceptionMacro(<<"Input interferogram must be into master geometry (ML : 1x1)");
	      }
	    

	    // Estimate ComplexCompensated image (VectorImage with : real, imag and mod as bands)
	    CompensatedComplexFromInterferogramFilterType::Pointer compensatedComplexFromInterf = 
	      CompensatedComplexFromInterferogramFilterType::New();
	    m_Ref.push_back(compensatedComplexFromInterf.GetPointer());
	    compensatedComplexFromInterf->SetInput(inInterfAmpPtr);

	    // Transform VectorImage To Complex Type
	    // Extract the first and second bands
	    ExtractROIFilterType::Pointer extractRealFilter = ExtractROIFilterType::New();
	    m_Ref.push_back(extractRealFilter.GetPointer());
	    ExtractROIFilterType::Pointer extractImagFilter = ExtractROIFilterType::New();
	    m_Ref.push_back(extractImagFilter.GetPointer());
	    
	    // Real part
	    extractRealFilter->SetInput(compensatedComplexFromInterf->GetOutput());
	    extractRealFilter->SetChannel(1);
	    extractRealFilter->GetOutput()->UpdateOutputInformation();
	    // Imag part
	    extractImagFilter->SetInput(compensatedComplexFromInterf->GetOutput());
	    extractImagFilter->SetChannel(2);
	    extractImagFilter->GetOutput()->UpdateOutputInformation();

	    // RealAndImagToComplex filter
	    RealImagToComplexFilterType::Pointer realImagToComplexFilter = 
	      RealImagToComplexFilterType::New();
	    m_Ref.push_back(realImagToComplexFilter.GetPointer());
	
	    realImagToComplexFilter->SetInputRealPart(extractRealFilter->GetOutput());
	    realImagToComplexFilter->SetInputImaginaryPart(extractImagFilter->GetOutput());
	    
	    
	    inComplexAmpPtr = realImagToComplexFilter->GetOutput();
	    inComplexAmpPtr->UpdateOutputInformation();
	  }

	
	// Create or Re-create the interferogram (with selected components : only amp for now).
	GroupedByMLFilterType::Pointer filterGroupedBy = GroupedByMLFilterType::New();
	m_Ref.push_back(filterGroupedBy.GetPointer());
	
	// Only for phase and coherence
	std::vector<std::string> bands;
	bands.push_back("amp");
	filterGroupedBy->bandSelection(bands);

	filterGroupedBy->SetInput(inComplexAmpPtr);
	filterGroupedBy->SetMLRan(factor_ran);
	filterGroupedBy->SetMLAzi(factor_azi);
	filterGroupedBy->SetGain(gain);
	filterGroupedBy->SetMarginRan(1);
	filterGroupedBy->SetMarginAzi(1);


	ConcatenateFilterType::Pointer concatVectorImageFilter = ConcatenateFilterType::New();
	m_Ref.push_back(concatVectorImageFilter.GetPointer());
	concatVectorImageFilter->SetInput1(filterGroupedBy->GetOutput());
	concatVectorImageFilter->SetInput2(inInterfPtr);
	
	// Output
	SetParameterOutputImage("out", concatVectorImageFilter->GetOutput());
	//RegisterPipeline();
	
      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;
    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SARAddBandInterferogram)
