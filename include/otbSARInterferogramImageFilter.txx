/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARInterferogramImageFilter_txx
#define otbSARInterferogramImageFilter_txx

#include "otbSARInterferogramImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "itkContinuousIndex.h"

#include <cmath>
#include <algorithm>


namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut> 
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::SARInterferogramImageFilter()
    :  m_MLRan(1), m_MLAzi(1), m_Gain(1), m_nbLinesSAR(0), m_nbColSAR(0), 
       m_MarginRan(1), m_MarginAzi(1), m_nbLinesDEM(0), m_nbColDEM(0)
  {
    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(2);
    this->SetNumberOfIndexedInputs(3);
  }
    
  /** 
   * Destructor
   */
  template <class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut> 
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::~SARInterferogramImageFilter()
  {
  }

  /**
   * Print
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);

    os << indent << "ML factors : " << m_MLRan << ", " << m_MLAzi << std::endl;
    os << indent << "Gain for amplitude estimation : " << m_Gain << std::endl;
  }

  /**
   * Set Master Image
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
 SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::SetMasterInput(const ImageSARType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageSARType *>(image));
  }

  /**
   * Set Coregistrated Slave Image
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
 SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::SetSlaveInput(const ImageSARType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<ImageSARType *>(image));
  }

  /**
   * Set Topographic phase
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::SetTopographicPhaseInput(const ImagePhaseType* phaseTopoImage)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(2, const_cast<ImagePhaseType *>(phaseTopoImage));

    // Adapt the tolerance for inputs
    if (m_MLRan != 1 || m_MLAzi != 1)
      {
	int MLFact = m_MLRan;
	if (m_MLRan < m_MLAzi)
	  {
	    MLFact = m_MLAzi;
	  }
	
	this->SetCoordinateTolerance(MLFact);
	this->SetDirectionTolerance(MLFact);
      }
  }

  /**
   * Get Master Image
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  const typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImageSARType *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetMasterInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageSARType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Coregistrated Slave Image
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  const typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImageSARType *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetSlaveInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const ImageSARType *>(this->itk::ProcessObject::GetInput(1));
  }

  /**
   * Get Phase Topographic
   */ 
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  const typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImagePhaseType *
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GetTopographicPhaseInput() const
  {
    if (this->GetNumberOfInputs()<3)
      {
	return 0;
      }
    return static_cast<const ImagePhaseType *>(this->itk::ProcessObject::GetInput(2));
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the inputs and output
    ImageSARConstPointer masterPtr = this->GetMasterInput();
    ImageSARConstPointer slavePtr = this->GetSlaveInput();
    ImageOutPointer outputPtr = this->GetOutput();

    // Metadata
    ImageMetadata masterMD = masterPtr->GetImageMetadata();

    // Master SAR Dimensions
    m_nbLinesSAR = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1];
    m_nbColSAR = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0];
  
    //////////////////////////// Main Output : Interferogram into ML Geo ///////////////////////////
    // Vector Image  :
    // At Least 4 Components :  
    //                _ Amplitude
    //                _ Phase
    //                _ Coherence
    //                _ IsData
    outputPtr->SetNumberOfComponentsPerPixel(4);

    // The output is defined with the Master SAR Image and by ML factors 
    // Origin, Spacing and Size (SAR master geometry) in function of ML factors
    ImageOutSizeType outputSize;

    outputSize[0] = std::max<ImageOutSizeValueType>(static_cast<ImageOutSizeValueType>(std::floor( (double) m_nbColSAR / (double) m_MLRan)),1);
    outputSize[1] = std::max<ImageOutSizeValueType>(static_cast<ImageOutSizeValueType>(std::floor( (double) m_nbLinesSAR / (double) m_MLAzi)),1);

    ImageOutPointType outOrigin;
    outOrigin = masterPtr->GetOrigin();
    ImageOutSpacingType outSP;
    outSP = masterPtr->GetSpacing();
    outSP[0] *= m_MLRan;
    outSP[1] *= m_MLAzi;

    // Define Output Largest Region
    ImageOutRegionType outputLargestPossibleRegion = masterPtr->GetLargestPossibleRegion();
    outputLargestPossibleRegion.SetSize(outputSize);
    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSpacing(outSP);

    // Add ML factors and bands meaning into Metadata
    ImageMetadata outputMD = masterMD;
    outputMD.Add("band.Amplitude", std::to_string(0));
    outputMD.Add("band.Phase", std::to_string(1));
    outputMD.Add("band.Coherency", std::to_string(2));
    outputMD.Add("band.isData", std::to_string(3));

    outputMD.Add("ml_ran", std::to_string(m_MLRan));
    outputMD.Add("ml_azi", std::to_string(m_MLAzi));

    // Set new keyword list to output image
    outputPtr->SetImageMetadata(outputMD);    

    ///////// Checks (with input topographic phase metadata) /////////////
    // Check ML Factors
     if (this->GetTopographicPhaseInput() != nullptr)
       {
	 // Get ImagMetadata
	 ImageMetadata topoMD = this->GetTopographicPhaseInput()->GetImageMetadata();
	 
	 if (topoMD.Has("ml_ran") && topoMD.Has("ml_azi"))
	   {
	     // Get Master ML Factors
	     unsigned int topo_MLRan = atoi(topoMD["ml_ran"].c_str());
	     unsigned int topo_MLAzi = atoi(topoMD["ml_azi"].c_str());
	
	     if ((topo_MLRan != m_MLRan) || (topo_MLAzi != m_MLAzi))
	       {
		 itkExceptionMacro(<<"ML Factor betwwen topographic phase and inputs of this application are different.");
	       }
	   }
       }

     ///////// Checks margin with ML factors (for averaging) /////////////
     if (m_MLRan != 0 || m_MLAzi != 0)
       {
	 //if (m_MLRan < m_MarginRan || m_MLAzi < m_MarginAzi)
	 // {
	 //   itkExceptionMacro(<<"Margin for averaging can't be superior to ML Factors.");   
	 // } 
       }
  }

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImageSARRegionType 
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::OutputRegionToInputRegion(const ImageOutRegionType& outputRegion, bool withMargin) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();

    // Add a margin for averaging
    int marge_MLRan = m_MarginRan;
    int marge_MLAzi = m_MarginAzi;

    if (!withMargin)
      {
	marge_MLRan = 0;
	marge_MLAzi = 0;
      }

    // Multiply to output region by ML factor to get input SAR region (After coregistration Master and Slave have
    // the same geometry)
    ImageSARIndexType inputRequestedRegionIndex; 
    inputRequestedRegionIndex[0] = static_cast<ImageSARIndexValueType>(outputRequestedRegionIndex[0] * m_MLRan
								       - marge_MLRan);
    inputRequestedRegionIndex[1] = static_cast<ImageSARIndexValueType>(outputRequestedRegionIndex[1] * m_MLAzi 
								       - marge_MLAzi);
    ImageSARSizeType inputRequestedRegionSize;
    inputRequestedRegionSize[0] = static_cast<ImageSARSizeValueType>(outputRequestedRegionSize[0] * m_MLRan + 
								     2*marge_MLRan);
    inputRequestedRegionSize[1] = static_cast<ImageSARSizeValueType>(outputRequestedRegionSize[1] * m_MLAzi + 
								     2*marge_MLAzi);  
   
    // Check Index and Size
    if (inputRequestedRegionIndex[0] < this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[0])
      {
	inputRequestedRegionIndex[0] = this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[0];
      }
    if (inputRequestedRegionIndex[1] < this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[1])
      {
	inputRequestedRegionIndex[1] = this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[1];
      }
    if ((inputRequestedRegionSize[0] + inputRequestedRegionIndex[0]) > 
	this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0])
      {
	inputRequestedRegionSize[0] = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0] - 
	  inputRequestedRegionIndex[0];
      }
    if ((inputRequestedRegionSize[1] + inputRequestedRegionIndex[1]) > 
	this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1])
      {
	inputRequestedRegionSize[1] = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1] - 
	  inputRequestedRegionIndex[1];
      }

    // Transform into a region
    ImageSARRegionType inputRequestedRegion = outputRegion;
    inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
    inputRequestedRegion.SetSize(inputRequestedRegionSize);
    
    
    return inputRequestedRegion;  
  }

/** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  typename SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >::ImagePhaseRegionType 
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::OutputRegionToInputPhaseRegion(const ImageOutRegionType& outputRegion, 
				   bool & sameGeoAsMainOutput, bool withMargin) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input grid requested region
    // that will provide the proper range
    const ImageOutSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();

    // Get Pointer
    ImagePhaseConstPointer topoPhase = this->GetTopographicPhaseInput();
            
    // Get Key into Metadata to identify the geometry of Topographic Phase image 
    // Two kinds of geometry : ML (with the same factors m_MLRan and m_MLAzi) or SAR
    ImageMetadata topoPhaseMD = topoPhase->GetImageMetadata();

    sameGeoAsMainOutput = false;

    std::string key_MLRan = "ml_ran";
    std::string key_MLAzi = "ml_azi";

    ImagePhaseRegionType inputPhaseRequestedRegion;

    if (!topoPhaseMD.Has(key_MLRan) || !topoPhaseMD.Has(key_MLAzi))
      {
	// Add a margin for averaging
	int marge_MLRan = m_MarginRan;
	int marge_MLAzi = m_MarginAzi;
	
	if (!withMargin)
	  {
	    marge_MLRan = 0;
	    marge_MLAzi = 0;
	  }

	// SAR Geometry
	// Multiply to output region by ML factor to get input SAR region 
	ImagePhaseIndexType inputPhaseRequestedRegionIndex; 
	inputPhaseRequestedRegionIndex[0] = static_cast<ImagePhaseIndexValueType>(outputRequestedRegionIndex[0] 
										  * m_MLRan - marge_MLRan);
	inputPhaseRequestedRegionIndex[1] = static_cast<ImagePhaseIndexValueType>(outputRequestedRegionIndex[1] 
										  * m_MLAzi - marge_MLAzi);
	ImagePhaseSizeType inputPhaseRequestedRegionSize;
	inputPhaseRequestedRegionSize[0] = static_cast<ImagePhaseSizeValueType>(outputRequestedRegionSize[0] 
										* m_MLRan + 2*marge_MLRan);
	inputPhaseRequestedRegionSize[1] = static_cast<ImagePhaseSizeValueType>(outputRequestedRegionSize[1] 
										* m_MLAzi + 2*marge_MLAzi);  
   
	// Check Index and Size
	if (inputPhaseRequestedRegionIndex[0] < this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[0])
	  {
	    inputPhaseRequestedRegionIndex[0] = this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[0];
	  }
	if (inputPhaseRequestedRegionIndex[1] < this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[1])
	  {
	    inputPhaseRequestedRegionIndex[1] = this->GetMasterInput()->GetLargestPossibleRegion().GetIndex()[1];
	  }
	if ((inputPhaseRequestedRegionSize[0] + inputPhaseRequestedRegionIndex[0]) > 
	    this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0])
	  {
	    inputPhaseRequestedRegionSize[0] = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0] - 
	      inputPhaseRequestedRegionIndex[0];
	  }
	if ((inputPhaseRequestedRegionSize[1] + inputPhaseRequestedRegionIndex[1]) > 
	    this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1])
	  {
	    inputPhaseRequestedRegionSize[1] = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1] - 
	      inputPhaseRequestedRegionIndex[1];
	  }

	// Transform into a region
	inputPhaseRequestedRegion = outputRegion;
	inputPhaseRequestedRegion.SetIndex(inputPhaseRequestedRegionIndex);
	inputPhaseRequestedRegion.SetSize(inputPhaseRequestedRegionSize);
      }
    else
      {
	// Get ML factors
	unsigned int ml_Ran_Phase = atoi(topoPhaseMD[key_MLRan].c_str());
	unsigned int ml_Azi_Phase = atoi(topoPhaseMD[key_MLAzi].c_str());
	
	// Check ML factors
	if (ml_Ran_Phase != m_MLRan || ml_Azi_Phase != m_MLAzi)
	  {
	    itkExceptionMacro(<<"ML factors for Topographic phase is not equal to ML factors for interferogram.");
	    return inputPhaseRequestedRegion;
	  }

	// Same region than output
	inputPhaseRequestedRegion = outputRegion;
	sameGeoAsMainOutput = true;
      }

    // Return phase region
    return inputPhaseRequestedRegion; 
  }

  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::GenerateInputRequestedRegion()
  {
    // call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
    
    // Get Output requested region
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

    ///////////// Find the region into SAR input images (same geometry for SAR after coregistration ////////////
    ImageSARRegionType inputSARRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion, true);

    ImageSARPointer  masterPtr = const_cast< ImageSARType * >( this->GetMasterInput() );
    ImageSARPointer  slavePtr = const_cast< ImageSARType * >( this->GetSlaveInput() );
    
    masterPtr->SetRequestedRegion(inputSARRequestedRegion);
    slavePtr->SetRequestedRegion(inputSARRequestedRegion);

    ///////////// Find the region into topographic phase image (if needed) /////////////
    if (this->GetTopographicPhaseInput() != nullptr)
      {
	bool sameGeoAsMainOutput;
	ImagePhaseRegionType phaseRequestedRegion = OutputRegionToInputPhaseRegion(outputRequestedRegion,
										   sameGeoAsMainOutput, true);
	ImagePhasePointer  phasePtr = const_cast< ImagePhaseType * >( this->GetTopographicPhaseInput() );
	phasePtr->SetRequestedRegion(phaseRequestedRegion);
      }
  }
  

  /**
   * Method ThreadedGenerateData
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::ThreadedGenerateData(const ImageOutRegionType & outputRegionForThread,
			 itk::ThreadIdType threadId)
  {
    if (this->GetTopographicPhaseInput() == nullptr)
      {
	// Process for line
	this->ThreadedGenerateDataWithoutTopographicPhase(outputRegionForThread,threadId);
      } 
    else
      {
	// Process for column
	this->ThreadedGenerateDataWithTopographicPhase(outputRegionForThread,threadId);
      } 
  }

  /**
   * Method ThreadedGenerateDataWithoutTopographicPhase
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::ThreadedGenerateDataWithoutTopographicPhase(const ImageOutRegionType & outputRegionForThread,
						itk::ThreadIdType threadId)
  {
    // Compute corresponding input region for SAR images
    ImageSARRegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread, true);
  
    //  Define/declare an iterator that will walk the input regions for this
    // thread. 
    InputSARIterator  inMasterIt(this->GetMasterInput(), inputRegionForThread);
    InputSARIterator  inSlaveIt(this->GetSlaveInput(), inputRegionForThread);

    // Define/declare an iterator that will walk the output region for this
    // thread.
    OutputIterator outIt(this->GetOutput(), outputRegionForThread);

    // Support progress methods/callbacks
    itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

    inMasterIt.GoToBegin();
    inSlaveIt.GoToBegin();
    outIt.GoToBegin();

    // Get the number of column of the region
    const ImageOutSizeType &   outputRegionForThreadSize = outputRegionForThread.GetSize();
    unsigned int nbCol = static_cast<unsigned int>(outputRegionForThreadSize[0]); 
  
    // Allocate Arrays for accumulations (size = nbCol)
    double *complexMulConjTab_Re = new double[nbCol];
    double *complexMulConjTab_Im = new double[nbCol];
    double *complexMulConjTab_Mod = new double[nbCol];
    
    // Output Pixel (VectorImage Pixel)
    ImageOutPixelType outPixel;
    outPixel.Reserve(4);

    //// Initialize the indLastL and indFirstL (for spending our averaging with a margin) ////
    int indFirstL = 0;
    int indLastL = m_MLAzi+ m_MarginAzi;
    int marginAzi = (int) m_MarginAzi;
    
    if (inMasterIt.GetIndex()[1] >= marginAzi && marginAzi != 0)
      {
	indFirstL = -marginAzi;
      }
	
    if ((inMasterIt.GetIndex()[1] + indLastL - indFirstL) > (m_nbLinesSAR-1))
      {
	indLastL = (m_nbLinesSAR-1) - inMasterIt.GetIndex()[1] + indFirstL;
      }

    bool backTopreviousLines = false;
    bool backTopreviousColunms = false;
    

    // For each Line
    while ( !inMasterIt.IsAtEnd() && !inSlaveIt.IsAtEnd() && !outIt.IsAtEnd())
      {
	// reinitialize
	for (unsigned int j = 0; j < nbCol; j++) 
	  {
	    complexMulConjTab_Re[j] = 0;
	    complexMulConjTab_Im[j] = 0;
	    complexMulConjTab_Mod[j] = 0;
	  }

	if (backTopreviousLines)
	  {
	    //// Previous index in azimut (for averaging) ////
	    if (inMasterIt.GetIndex()[1] >= marginAzi && marginAzi != 0)
	      {
		indFirstL = -marginAzi;
	      }

	    ImageSARIndexType indSARL;
	    indSARL[0] = inMasterIt.GetIndex()[0];
	    indSARL[1] = inMasterIt.GetIndex()[1] - (2*marginAzi);
	
	    if ((indSARL[1] + indLastL - indFirstL) > (m_nbLinesSAR-1))
	      {
		indLastL = (m_nbLinesSAR - 1) - indSARL[1] + indFirstL;
	      }

	    // Check previous index
	    if (indSARL[1] >= 0 && marginAzi != 0)
	      {
		// Previous index inputs
		inMasterIt.SetIndex(indSARL);
		inSlaveIt.SetIndex(indSARL);
	
		indFirstL = -marginAzi;
	      }
	  }

	backTopreviousLines = true;


	// GenerateInputRequestedRegion function requires an input region with In_nbLine = m_Averaging * Out_nbLine
	for (int i = indFirstL ; i < indLastL; i++) 
	  {
	    inMasterIt.GoToBeginOfLine();
	    inSlaveIt.GoToBeginOfLine();
	    outIt.GoToBeginOfLine();
	  
	    int colCounter = 0;
	    backTopreviousColunms = false;

	    //// Initialize the indLastC and indFirstC (for spending average with a margin) ////
	    int indFirstC = 0;
	    int indLastC = m_MLRan + m_MarginRan;
	    int marginRan = (int) m_MarginRan;
    
	    if (inMasterIt.GetIndex()[0] >= marginRan  && marginRan != 0)
	      {
		indFirstC = -marginRan;
	      }
	
	    if ((inMasterIt.GetIndex()[0] + indLastC - indFirstC) > (m_nbColSAR-1))
	      {
		indLastC = (m_nbColSAR-1) - inMasterIt.GetIndex()[0] + indFirstC;
	      }


	    // For each column
	    while (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine() && !outIt.IsAtEndOfLine())
	      {
		if (backTopreviousColunms)
		  {
		    //// Previous index in range (for averaging) ////
		    if (inMasterIt.GetIndex()[0] >= marginRan  && marginRan != 0)
		      {
			indFirstC = -marginRan;
		      }

		    ImageSARIndexType indSARC;
		    indSARC[0] = inMasterIt.GetIndex()[0] - (2*marginRan);
		    indSARC[1] = inMasterIt.GetIndex()[1];
		
		    if ((indSARC[0] + indLastC - indFirstC) > (m_nbColSAR-1))
		      {
			indLastC = (m_nbColSAR-1) - indSARC[0] + indFirstC;
		      }

		    // Check previous index
		    if (indSARC[0] >= 0 && marginRan != 0)
		      {
			// Previous index inputs
			inMasterIt.SetIndex(indSARC);
			inSlaveIt.SetIndex(indSARC);
		
			indFirstC = -marginRan;
		      }
	
		  }

		backTopreviousColunms = true;

		for (int k = indFirstC ; k < indLastC; k++) 
		  {
		    if (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine())
		      {
			// Complex interferogram (master * conj(slave))
			double real_interfero = (inMasterIt.Get().real()*inSlaveIt.Get().real() + 
						 inMasterIt.Get().imag()*inSlaveIt.Get().imag());

			double imag_interfero = (inMasterIt.Get().imag()*inSlaveIt.Get().real() - 
						 inMasterIt.Get().real()*inSlaveIt.Get().imag());

			///////////// Accumulations ///////////////
			complexMulConjTab_Re[colCounter] += real_interfero;

			
			complexMulConjTab_Im[colCounter] += imag_interfero;
			
			
			complexMulConjTab_Mod[colCounter] += sqrt((real_interfero*real_interfero) + 
								  (imag_interfero*imag_interfero));

			// Next colunm inputs
			++inMasterIt;
			++inSlaveIt;
		      }
		  }
	      	      
		// Estiamte and Assigne outIt
		if (i == (indLastL-1))
		  {
		    ///////////// Estimations of amplitude, phase and coherency ///////////////
		    double mod_Acc = sqrt(complexMulConjTab_Re[colCounter]*complexMulConjTab_Re[colCounter] + 
					  complexMulConjTab_Im[colCounter]*complexMulConjTab_Im[colCounter]);

		    int countPixel = (m_MLRan + 2*m_MarginRan) * (m_MLAzi + 2*m_MarginAzi);

		    // Amplitude
		    outPixel[0] = m_Gain * sqrt((complexMulConjTab_Mod[colCounter]/(countPixel)));
		    
		    // Phase
		    outPixel[1] = std::atan2(complexMulConjTab_Im[colCounter], 
					     complexMulConjTab_Re[colCounter]);

		    // Mod 2*Pi
		    outPixel[1] =  outPixel[1]-(2*M_PI)*floor(outPixel[1]/(2*M_PI));
		    
		    // Coherency
		    outPixel[2] = mod_Acc / complexMulConjTab_Mod[colCounter] ;
	  
		    // IsData always set to 1
		    outPixel[3] = 1;
		    
		    if (outPixel[0] == 0 && outPixel[1] == 0 && outPixel[2] == 0)
		      {
			outPixel[3] = 0;
		      } 
		    
		    // Assigne Main output (ML geometry) 
		    outIt.Set(outPixel);
		    progress.CompletedPixel();		  
		  }

		// Next colunm output
		++outIt;
		colCounter++;
	      }
	  
	    // Next line intputs
	    inMasterIt.NextLine();
	    inSlaveIt.NextLine();
	  }
      
	// Next line output
	outIt.NextLine();
   
      }
  delete [] complexMulConjTab_Re;
  delete [] complexMulConjTab_Im;
  delete [] complexMulConjTab_Mod;
  }

  /**
   * Method ThreadedGenerateDataWithTopographicPhase
   */
  template<class TImageSAR, class TImageDEM, class TImagePhase, class TImageOut>
  void
  SARInterferogramImageFilter< TImageSAR, TImageDEM, TImagePhase, TImageOut >
  ::ThreadedGenerateDataWithTopographicPhase(const ImageOutRegionType & outputRegionForThread,
					     itk::ThreadIdType threadId)
  {
    // Support progress methods/callbacks
    itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

    // Output Pixel (VectorImage Pixel)
    ImageOutPixelType outPixel;
    outPixel.Reserve(4);

    // Compute corresponding input region for SAR images
    ImageSARRegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread, true);
  
    // Compute corresponding input region for Topographic phase image
    bool sameGeoAsMainOutput; // If true => Geo TopoPhase == main output geo else Geo TopoPhase = SAR Geo    
    ImagePhaseRegionType inputPhaseRegionForThread = OutputRegionToInputPhaseRegion(outputRegionForThread,
										    sameGeoAsMainOutput, true);
    //  Define/declare an iterator that will walk the input regions for this
    // thread. 
    InputSARIterator  inMasterIt(this->GetMasterInput(), inputRegionForThread);
    InputSARIterator  inSlaveIt(this->GetSlaveInput(), inputRegionForThread);

    InputPhaseIterator inTopoPhaseIt(this->GetTopographicPhaseInput(), inputPhaseRegionForThread);

    // Define/declare an iterator that will walk the output region for this
    // thread.
    OutputIterator outIt(this->GetOutput(), outputRegionForThread);
    
    inMasterIt.GoToBegin();
    inSlaveIt.GoToBegin();
    inTopoPhaseIt.GoToBegin();
    outIt.GoToBegin();

    // Get the number of column of the region
    const ImageOutSizeType &   outputRegionForThreadSize = outputRegionForThread.GetSize();
    unsigned int nbCol = static_cast<unsigned int>(outputRegionForThreadSize[0]); 
  
    // Allocate Arrays for accumulations (size = nbCol)
    double *complexMulConjTab_Re = new double[nbCol];
    double *complexMulConjTab_Im = new double[nbCol];
    double *complexMulConjTab_Mod = new double[nbCol];
    int *isDataCounter = new int[nbCol];

    Point2DType groundPoint;
    itk::ContinuousIndex<double, 2> groundContinousIndex;

    //// Initialize the indLastL and indFirstL (for spending our averaging with a margin) ////
    int indFirstL = 0;
    int indLastL = m_MLAzi+ m_MarginAzi;
    int marginAzi = (int) m_MarginAzi;
    
    if (inMasterIt.GetIndex()[1] >= marginAzi && marginAzi != 0)
      {
	indFirstL = -marginAzi;
      }
      
    if ((inMasterIt.GetIndex()[1] + (indLastL - indFirstL)) > (m_nbLinesSAR-1))
      {
	indLastL = (m_nbLinesSAR-1) + indFirstL - inMasterIt.GetIndex()[1];
      }
    

    bool backTopreviousLines = false;
    bool backTopreviousColunms = false;
    
    //////// For each Line /////////
    while ( !inMasterIt.IsAtEnd() && !inSlaveIt.IsAtEnd() && !inTopoPhaseIt.IsAtEnd() && !outIt.IsAtEnd())
      {
	// reinitialize
	for (unsigned int j = 0; j < nbCol; j++) 
	  {
	    complexMulConjTab_Re[j] = 0;
	    complexMulConjTab_Im[j] = 0;
	    complexMulConjTab_Mod[j] = 0;
	    isDataCounter[j] = 0;
	  }

	if (backTopreviousLines)
	  {
	    //// Previous index in azimut (for averaging) ////
	    if (inMasterIt.GetIndex()[1] >= marginAzi && marginAzi != 0)
	      {
		indFirstL = -marginAzi;
	      }

	    ImageSARIndexType indSARL;
	    indSARL[0] = inMasterIt.GetIndex()[0];
	    indSARL[1] = inMasterIt.GetIndex()[1] - (2*marginAzi);

	    ImagePhaseIndexType indPhaL;
	    indPhaL[0] = inTopoPhaseIt.GetIndex()[0];
	    indPhaL[1] = inTopoPhaseIt.GetIndex()[1] - (2*marginAzi);
	
	
	    if ((indSARL[1] + indLastL - indFirstL) > (m_nbLinesSAR-1))
	      {
		indLastL = (m_nbLinesSAR - 1) - indSARL[1] + indFirstL;
	      }

	    // Check previous index
	    if (indSARL[1] >= 0 && indPhaL[1] >= 0 && marginAzi != 0)
	      {
		// Previous index inputs
		inMasterIt.SetIndex(indSARL);
		inSlaveIt.SetIndex(indSARL);
		// If topographic phase has SAR geo
		if (!sameGeoAsMainOutput)
		  {
		    inTopoPhaseIt.SetIndex(indPhaL);
		  }

		indFirstL = -marginAzi;
	      }
	  }

	backTopreviousLines = true;


	// GenerateInputRequestedRegion function requires an input region with 
	// In_nbLine = m_Averaging * Out_nbLine (Averaing in function of ML Factors and Margins)
	for (int i = indFirstL ; i < indLastL; i++) 
	  {
	    inMasterIt.GoToBeginOfLine();
	    inSlaveIt.GoToBeginOfLine();
	    inTopoPhaseIt.GoToBeginOfLine();
	    outIt.GoToBeginOfLine();
	  
	    int colCounter = 0;
	    backTopreviousColunms = false;

	    //// Initialize the indLastC and indFirstC (for spending average with a margin) ////
	    int indFirstC = 0;
	    int indLastC = m_MLRan + m_MarginRan;
	    int marginRan = (int) m_MarginRan;

	    if (inMasterIt.GetIndex()[0] >= marginRan  && marginRan != 0)
	      {
		indFirstC = -marginRan;
	      }

	    if ((inMasterIt.GetIndex()[0] + (indLastC - indFirstC)) > (m_nbColSAR-1))
	      {
		indLastC = (m_nbColSAR-1) + indFirstC - inMasterIt.GetIndex()[0];
	      }


	    ///////// For each column /////////
	    while (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine() && !inTopoPhaseIt.IsAtEndOfLine() 
		   && !outIt.IsAtEndOfLine())
	      {

		if (backTopreviousColunms)
		  {
		    //// Previous index in range (for averaging) ////
		    if (inMasterIt.GetIndex()[0] >= marginRan  && marginRan != 0)
		      {
			indFirstC = -marginRan;
		      }

		    ImageSARIndexType indSARC;
		    indSARC[0] = inMasterIt.GetIndex()[0] - (2*marginRan);
		    indSARC[1] = inMasterIt.GetIndex()[1];

		    ImagePhaseIndexType indPhaC;
		    indPhaC[0] = inTopoPhaseIt.GetIndex()[0] - (2*marginRan);
		    indPhaC[1] = inTopoPhaseIt.GetIndex()[1]; 
	
		
		    if ((indSARC[0] + indLastC - indFirstC) > (m_nbColSAR-1))
		      {
			indLastC = (m_nbColSAR-1) - indSARC[0] + indFirstC;
		      }

		    // Check previous index
		    if (indSARC[0] >= 0 && indPhaC[0] >= 0 && marginRan != 0)
		      {
			// Previous index inputs
			inMasterIt.SetIndex(indSARC);
			inSlaveIt.SetIndex(indSARC);
			// If topographic phase has SAR geo
			if (!sameGeoAsMainOutput)
			  {
			    inTopoPhaseIt.SetIndex(indPhaC);
			  }

			indFirstC = -marginRan;
		      }
	
		  }

		backTopreviousColunms = true;
	 

		for (int k = indFirstC ; k < indLastC; k++) 
		  {
		    if (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine())
		      {
			// Check isData (grom topographic phase)
			if (inTopoPhaseIt.Get()[1] != 0)
			  {			
			    // Complex Raw interferogram (master * conj(slave))
			    double real_RawInterfero = (inMasterIt.Get().real()*inSlaveIt.Get().real() + 
							inMasterIt.Get().imag()*inSlaveIt.Get().imag());

			    double imag_RawInterfero = (inMasterIt.Get().imag()*inSlaveIt.Get().real() - 
							inMasterIt.Get().real()*inSlaveIt.Get().imag());

			    ///////////// Topographoc phase as complex number ////////////////
			    double topoPhase = inTopoPhaseIt.Get()[0];
				
			    double complexTopoPhase_Re = std::cos(topoPhase);
			    double complexTopoPhase_Im = std::sin(topoPhase);

	
			    // Multiply the conj(complexTopoPhase) with complex raw interferogram
			    double real_interfero = (real_RawInterfero * complexTopoPhase_Re + 
						     imag_RawInterfero * complexTopoPhase_Im);
			    double imag_interfero = (imag_RawInterfero * complexTopoPhase_Re - 
						     real_RawInterfero * complexTopoPhase_Im);

			    ///////////// Accumulations ///////////////
			    complexMulConjTab_Re[colCounter] += real_interfero;

			
			    complexMulConjTab_Im[colCounter] += imag_interfero;
			
			
			    complexMulConjTab_Mod[colCounter] += sqrt((real_interfero*real_interfero) + 
								      (imag_interfero*imag_interfero));

			    isDataCounter[colCounter] += 1; 
			  }

			// Next colunm inputs
			++inMasterIt;
			++inSlaveIt;
			// If topographic phase has SAR geo
			if (!sameGeoAsMainOutput)
			  {
			    ++inTopoPhaseIt;
			  }
		      }
		  } // End for k (indFirstC to indLastC)
	      	      
		    // Estiamte and Assigne outIt
		    //if (i == (m_MLAzi-1))
		if (i == (indLastL-1))
		  {
		    // Check if Data
		    if (isDataCounter[colCounter] >= 1)
		      {
			///////////// Estimations of amplitude, phase and coherency ///////////////
			double mod_Acc = sqrt(complexMulConjTab_Re[colCounter]*complexMulConjTab_Re[colCounter]
					      + complexMulConjTab_Im[colCounter]*
					      complexMulConjTab_Im[colCounter]);

			int countPixel = (m_MLRan + 2*m_MarginRan) * (m_MLAzi + 2*m_MarginAzi);

			// Amplitude
			outPixel[0] = m_Gain * sqrt((complexMulConjTab_Mod[colCounter]/(countPixel)));
		    
			// Phase
			outPixel[1] = std::atan2(complexMulConjTab_Im[colCounter], 
						 complexMulConjTab_Re[colCounter]);

			// Mod 2*Pi
			outPixel[1] =  outPixel[1]-(2*M_PI)*floor(outPixel[1]/(2*M_PI));

			// Coherency
			if (complexMulConjTab_Mod[colCounter] != 0)
			  {
			    outPixel[2] = mod_Acc / complexMulConjTab_Mod[colCounter];
			  }
			else
			  {
			    outPixel[2] = 0;
			  }

			// isData set to 1 
			outPixel[3] = 1;

			if (outPixel[0] == 0 && outPixel[1] == 0 && outPixel[2] == 0)
			  {
			    outPixel[3] = 0;
			  } 
		      }
		    else
		      {
			outPixel[0] = 0;
			outPixel[1] = 0;
			outPixel[2] = 0;
			outPixel[3] = 0;
		      }
		    
		    outIt.Set(outPixel);
		    progress.CompletedPixel();		  
		  }

		// Next colunm output
		++outIt;
		// If topographic phase has Main output geo
		if (sameGeoAsMainOutput)
		  {
		    ++inTopoPhaseIt;
		  }
		colCounter++;
	      }
	  
	    // Next line intputs
	    inMasterIt.NextLine();
	    inSlaveIt.NextLine();
	    // If topographic phase has SAR geo
	    if (!sameGeoAsMainOutput)
	      {
		inTopoPhaseIt.NextLine();
	      }
	    
	  } // End for i (firstL to lastL)

	    // Next line output
	outIt.NextLine();
	// If topographic phase has Main output geo
	if (sameGeoAsMainOutput)
	  {
	    inTopoPhaseIt.NextLine();
	  }
   
      }
    delete [] complexMulConjTab_Re;
    delete [] complexMulConjTab_Im;
    delete [] complexMulConjTab_Mod;
    delete [] isDataCounter;
      

  }

  } /*namespace otb*/

#endif
