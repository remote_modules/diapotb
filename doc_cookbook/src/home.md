<div align="justify">

# Welcome to DiapOTB remote module! ![image](./Art/DIAPOTB_logo.jpg)

This is a module implements a legacy processing chain for SAR interferometry called Diapason as an OTB remote module.

The differential SAR interferometry (DInSAR) technique relies on the processing of two SAR images of the same portion of the Earth’s surface taken at different time. The aim is to analyze potential events (earthquake, destruction …) by highlighting differences between SAR images. DInSAR involves a set of tools such as creation of deformation grids , coregistration or building of interferograms. DiapOTB contains all necessary steps and allows to launch a complete DInSAR chain. The DiapOTB module were used with Sentinel-1, TSX/PAZ/TDX and Cosmo data with satisfactory results.

# Table of Contents


* [All Applications](Applications)


* [Processing Chains](ProcessingChains)
