/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDerampS1IWImageFilter_txx
#define otbSARDerampS1IWImageFilter_txx

#include "otbSARDerampS1IWImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include "otbSarSensorModel.h"

#include <cmath>
#include <algorithm>

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImage> 
  SARDerampS1IWImageFilter< TImage >::SARDerampS1IWImageFilter()
    :  m_FM_C0(0), m_FM_C1(0), m_FM_C2(0), m_FM_Tau0(0), m_DCF_C0(0), m_DCF_C1(0), m_DCF_C2(0), m_DCF_Tau0(0)
  {
    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(1);
    this->SetNumberOfIndexedInputs(2);
  }
    
  /** 
   * Destructor
   */
  template <class TImage> 
  SARDerampS1IWImageFilter< TImage >::~SARDerampS1IWImageFilter()
  {
   
  }


  template<class TImage>
  void
  SARDerampS1IWImageFilter< TImage >
  ::getAllCoefs(ImageMetadata  const& kwl, std::vector<FMRateRecordType> & FMRateRecords)
  {
    char fmRatePrefix_[1024];
    std::size_t nbLists(0);
    
    nbLists = boost::any_cast<const otb::SARParam&>(kwl[otb::MDGeom::SAR]).azimuthFmRates.size();

    std::string FM_PREFIX = "azimuthFmRate.azimuthFmRatePolynomial";
    
    for (std::size_t listId=0; listId!= nbLists ; ++listId) 
      {
	const int pos = sprintf(fmRatePrefix_, "%s%zu.", FM_PREFIX.c_str(), (listId+1));
	assert(pos > 0 && (unsigned long) pos < sizeof(fmRatePrefix_));
	const std::string FMPrefix(fmRatePrefix_, pos);

	FMRateRecordType fmRateRecord;
	fmRateRecord.azimuthFMRateTime = otb::MetaData::ReadFormattedDate(kwl[FMPrefix+
										      "azi_fm_rate_coef_time"]);
	fmRateRecord.coef0FMRate = std::stod(kwl[FMPrefix + "1.azi_fm_rate_coef"]);
	fmRateRecord.coef1FMRate = std::stod(kwl[FMPrefix + "2.azi_fm_rate_coef"]);
	fmRateRecord.coef2FMRate = std::stod(kwl[FMPrefix + "3.azi_fm_rate_coef"]);
	fmRateRecord.tau0FMRate = std::stod(kwl[FMPrefix + "slant_range_time"]);

	FMRateRecords.push_back(fmRateRecord);
      }
  }

  template<class TImage>
  void
  SARDerampS1IWImageFilter< TImage >
  ::getAllCoefs(ImageMetadata  const& kwl, std::vector<DCFRecordType> & DCFRecords)
  {
     char dcfPrefix_[1024];
    std::size_t nbLists(0);
    
    nbLists = std::stoi(kwl["dopplerCentroid.dop_coef_nb_list"]);

    std::string DCF_PREFIX = "dopplerCentroid.dop_coef_list";
    
    for (std::size_t listId=0; listId!= nbLists ; ++listId) 
      {
	const int pos = sprintf(dcfPrefix_, "%s%zu.", DCF_PREFIX.c_str(), (listId+1));
	assert(pos > 0 && (unsigned long) pos < sizeof(dcfPrefix_));
	const std::string DCFPrefix(dcfPrefix_, pos);

	DCFRecordType dcfRecord;
	dcfRecord.azimuthDCFTime = otb::MetaData::ReadFormattedDate(kwl[DCFPrefix +
									      "dop_coef_time"]);
	dcfRecord.coef0DCF = std::stod(kwl[DCFPrefix + "1.dop_coef"]);
	dcfRecord.coef1DCF = std::stod(kwl[DCFPrefix + "2.dop_coef"]);
	dcfRecord.coef2DCF = std::stod(kwl[DCFPrefix + "3.dop_coef"]);
	dcfRecord.tau0DCF = std::stod(kwl[DCFPrefix + "slant_range_time"]);

	 DCFRecords.push_back(dcfRecord);
      }
  }

  /**
   * Method selectFMRateCoef
   */
  template<class TImage>
  bool 
  SARDerampS1IWImageFilter< TImage >
  ::selectFMRateCoef()
  {
    // Get input
    ImagePointer inputPtr = const_cast< ImageType * >( this->GetImageInput() );

     // Retrieve all polynomials
    std::vector<otb::AzimuthFmRate> FMRateRecords;

    ImageMetadata inputKWL;
    if (this->m_ShiftMode)
	  {
	    if (this->GetGridInput() != nullptr)
	      {
		inputKWL = this->m_SarKWL;
	      }
	    else
	      {
		itkExceptionMacro(<<"Missing Image Metadata for shift mode");
	      }
	  }
    else
      {
	inputKWL = inputPtr->GetImageMetadata();
      }
    
    //this->getAllCoefs(inputKWL, FMRateRecords);
    FMRateRecords = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).azimuthFmRates;

    DurationType diffAziTimeMin;
    unsigned int polySelected = 0;
    
    // Select one polynomial (with m_MidAziTime)
    for (unsigned int i = 0; i < FMRateRecords.size(); i++)
      {
	// Difference between midAziTime and aziTime of the current polynomial
	DurationType diffAziTime;
	if (this->m_MidAziTime > FMRateRecords[i].azimuthTime)
	  {
	    diffAziTime = DurationType(this->m_MidAziTime - FMRateRecords[i].azimuthTime);
	  }
	else
	  {
	    diffAziTime = DurationType(FMRateRecords[i].azimuthTime - this->m_MidAziTime);
	  }
	
	if (diffAziTime < diffAziTimeMin || i == 0)
	  {
	    diffAziTimeMin = diffAziTime;
	    polySelected = i;
	  }
      }

    // Assign m_FM_C0, m_FM_C1, m_FM_C2, m_FM_Tau0
    m_FM_C0 = FMRateRecords[polySelected].azimuthFmRatePolynomial[0];
    m_FM_C1 = FMRateRecords[polySelected].azimuthFmRatePolynomial[1];
    m_FM_C2 = FMRateRecords[polySelected].azimuthFmRatePolynomial[2];
    m_FM_Tau0 = FMRateRecords[polySelected].t0;
      
    return true;
  }

 /**
   * Method selectDCFCoef
   */
  template<class TImage>
  bool 
  SARDerampS1IWImageFilter< TImage >
  ::selectDCFCoef()
  {
     // Get input
    ImagePointer inputPtr = const_cast< ImageType * >( this->GetImageInput() );

     // Retrieve all polynomials
    std::vector<otb::DopplerCentroid> DCFRecords;
    
    ImageMetadata inputKWL;
    if (this->m_ShiftMode)
	  {
	    if (this->GetGridInput() != nullptr)
	      {
		inputKWL = this->m_SarKWL;
	      }
	    else
	      {
		itkExceptionMacro(<<"Missing Image Metadata for shift mode");
	      }
	  }
    else
      {
	inputKWL = inputPtr->GetImageMetadata();
      }
    
    // this->getAllCoefs(inputKWL, DCFRecords);
    DCFRecords = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).dopplerCentroids;

    DurationType diffAziTimeMin;
    unsigned int polySelected = 0;
    
    // Select one polynomial (with m_MidAziTime)
    for (unsigned int i = 0; i < DCFRecords.size(); i++)
      {
	// Difference between midAziTime and aziTime of the current polynomial
	DurationType diffAziTime;
	if (this->m_MidAziTime > DCFRecords[i].azimuthTime)
	  {
	    diffAziTime = DurationType(this->m_MidAziTime - DCFRecords[i].azimuthTime);
	  }
	else
	  {
	    diffAziTime = DurationType(DCFRecords[i].azimuthTime - this->m_MidAziTime);
	  }
	
	if (diffAziTime < diffAziTimeMin || i == 0)
	  {
	    diffAziTimeMin = diffAziTime;
	    polySelected = i;
	  }
      }

    // Assign m_FM_C0, m_FM_C1, m_FM_C2, m_FM_Tau0
    m_DCF_C0 = DCFRecords[polySelected].dopCoef[0];
    m_DCF_C1 = DCFRecords[polySelected].dopCoef[1];
    m_DCF_C2 = DCFRecords[polySelected].dopCoef[2];
    m_DCF_Tau0 = DCFRecords[polySelected].t0;

    return true;
  }

  /**
   * Apply FM Rate coefficients Method
   */
  template<class TImage>
  long double
  SARDerampS1IWImageFilter<TImage>
  ::applyFMRateCoefs(double index_sample)
  {
    double slant_range_time = this->m_FirstRangeTime + (index_sample / this->m_RangeSamplingRate);
    
    return (m_FM_C0 + m_FM_C1*(slant_range_time - m_FM_Tau0) + m_FM_C2*(slant_range_time - m_FM_Tau0)*
	    (slant_range_time - m_FM_Tau0));
  }

  /**
   * Apply doppler centroid Frequency coefficients Method
   */
  template<class TImage>
  long double
  SARDerampS1IWImageFilter<TImage>
  ::applyDCFCoefs(double index_sample)
  {
    double slant_range_time = this->m_FirstRangeTime + (index_sample / this->m_RangeSamplingRate);
    
    return (m_DCF_C0 + m_DCF_C1*(slant_range_time - m_DCF_Tau0) + m_DCF_C2*(slant_range_time - m_DCF_Tau0)*
	    (slant_range_time - m_DCF_Tau0));
  }

  /**
   * Get Phi
   */
  template<class TImage>
  double
  SARDerampS1IWImageFilter<TImage>
  ::getPhi(double indL, double indC)
  {
    // Zero Doppler azimuth Time
    double aziTime = this->m_AziTimeInt*(indL - this->m_LineAtMidBurst); // zero-Doppler azimuth time centered in the middle of the burst

    // Reference Time
    double refDur = - (this->applyDCFCoefs(indC) / this->applyFMRateCoefs(indC)) - this->m_RefTime0;

    // Kt
    double Kt = (this->applyFMRateCoefs(indC) * this->m_Ks) / (this->applyFMRateCoefs(indC) - this->m_Ks);
    double diffTime = aziTime - refDur;
    
    // Phi
    double Phi = - M_PI * Kt * (diffTime)*(diffTime);

    return Phi;

  }


  /**
  * Method Initialize attributes for BeforeGenerateData()
   */
  template<class TImage>
  void 
  SARDerampS1IWImageFilter< TImage >
  ::ThreadInit(ImageMetadata inputKWL)
  {
    // Get some metadata
	double aziSteeringRate = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).azimuthSteeringRate;
	// Conversion to radians per seconds
	aziSteeringRate *= (M_PI/180); 

	this->m_FirstAziTime = inputKWL[MDTime::AcquisitionStartTime];
	this->m_FirstRangeTime = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).nearRangeTime;

	this->m_AziTimeInt = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).azimuthTimeInterval.TotalSeconds();
	this->m_RangeSamplingRate = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).rangeSamplingRate;

	double radarFrequency = inputKWL[MDNum::RadarFrequency];
	
	int nbLineBurst = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).numberOfLinesPerBurst;

	int nbSampleBurst = boost::any_cast<const otb::SARParam&>(inputKWL[otb::MDGeom::SAR]).numberOfSamplesPerBurst;
	// Estimation m_Ks
	this->m_LineAtMidBurst = nbLineBurst/2.;
	this->m_MidAziTime = this->m_FirstAziTime + DurationType::Seconds(this->m_AziTimeInt * this->m_LineAtMidBurst);
	
	// Try to create a SarSensorModelAdapter
	SarSensorModel* sarSensorModel = new SarSensorModel(inputKWL);

	Point3DType satpos, satvel;
 
	bool lineToSatPosAndVelOk = sarSensorModel->LineToSatPositionAndVelocity(this->m_LineAtMidBurst, satpos, satvel);

	if (!lineToSatPosAndVelOk)
	  itkExceptionMacro(<<"Failed to estimate satellite position and velocity.");
  
	this->m_VSatAtMidAziTime = std::sqrt(satvel[0]*satvel[0] + satvel[1]*satvel[1] + satvel[2]*satvel[2]);

	this->m_Ks = (2*this->m_VSatAtMidAziTime/C) * radarFrequency * aziSteeringRate;
	
	DurationType diffTime = this->m_MidAziTime - this->m_FirstAziTime;

	this->m_MidRanTime = this->m_FirstRangeTime + (nbSampleBurst / (2*this->m_RangeSamplingRate));

	// Polynomial selection (FM Rate and Doppler Centroid Frequency)
	this->selectFMRateCoef();
	this->selectDCFCoef();

	// Estimate Reference time at first sample
	this->m_RefTime0 = - (this->applyDCFCoefs(0) / this->applyFMRateCoefs(0));

	this->m_RefTimeMid =  - (this->applyDCFCoefs(nbSampleBurst / 2) / this->applyFMRateCoefs(nbSampleBurst / 2));

  }
  

} /*namespace otb*/

#endif
