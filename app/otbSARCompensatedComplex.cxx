/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARCompensatedComplexImageFilter.h"

#include "otbWrapperOutputImageParameter.h"
#include "otbWrapperTypes.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SARCompensatedComplex : public Application
    {
    public:
      typedef SARCompensatedComplex Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SARCompensatedComplex, otb::Wrapper::Application);

      // Filters
      typedef otb::SARCompensatedComplexImageFilter<ComplexFloatImageType, FloatVectorImageType, FloatVectorImageType> CompensatedComplexFilterType;

    private:
      void DoInit() override
      {
	SetName("SARCompensatedComplex");
	SetDescription("Compensated complex into master geometry between two SAR images.");

	SetDocLongDescription("This application estimates the compensated complex into ground geometry between two"
        " SAR images.");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "insarslave",   "Input SAR Slave image (Coregistrated image)");
	SetParameterDescription("insarslave", "Input SAR Slave image (Coregistrated image).");

	AddParameter(ParameterType_InputImage,  "insarmaster",   "Input SAR Master image");
	SetParameterDescription("insarmaster", "Input SAR Master image.");

	AddParameter(ParameterType_InputImage,  "topographicphase",   "Input Topographic Phase (estimation with DEM projection)");
	SetParameterDescription("topographicphase", "Input Topographic Phase (estimation with DEM projection).");
	MandatoryOff("topographicphase");
	
	AddParameter(ParameterType_OutputImage, "out", "Compensated complex image in master geometry");
	SetParameterDescription("out","Output Image : Compensated complex image in master geometry.");

	AddRAMParameter();

	SetDocExampleParameterValue("insarslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002.tiff");
	SetDocExampleParameterValue("insarmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
        SetDocExampleParameterValue("topographicphase","TopographicPhase.tiff");
        SetDocExampleParameterValue("out","s1b-s1a-s4-CompensatedComplex.tiff");
      }

      void DoUpdateParameters() override
      {
	// Nothing to do here : all parameters are independent
      }

      void DoExecute() override
      { 

	/////////////////////////////////// Compensated Complex Filter ////////////////////////////////////////
	// Instanciate the first filter
	CompensatedComplexFilterType::Pointer filterCompensatedComplex = CompensatedComplexFilterType::New();
	m_Ref.push_back(filterCompensatedComplex.GetPointer());
	
	// Execute the Pipeline
	ComplexFloatImageType::Pointer SARMasterPtr;
	SARMasterPtr = GetParameterComplexFloatImage("insarmaster");

	ComplexFloatImageType::Pointer SARSlavePtr;
	SARSlavePtr = GetParameterComplexFloatImage("insarslave");

	// Two Main inputs
	filterCompensatedComplex->SetMasterInput(SARMasterPtr);
	filterCompensatedComplex->SetSlaveInput(SARSlavePtr);

	// One optionnal input
	if (GetParameterByKey("topographicphase")->HasValue())
	  {
	    filterCompensatedComplex->SetTopographicPhaseInput(GetParameterImage("topographicphase"));
	  }
	

	// Main Output
	SetParameterOutputImage("out", filterCompensatedComplex->GetOutput());
      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;

    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SARCompensatedComplex)
